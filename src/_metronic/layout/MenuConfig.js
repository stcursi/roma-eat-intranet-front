export default {
  header: {
    self: {},
    items: [

    ]
  },
  aside: {
    self: {},
    items: [
      {
        title: "Summary",
        root: true,
        icon: "flaticon2-architecture-and-city",
        page: "summaryAdmin",
        translate: "MENU.SUMMARY",
        bullet: "dot"
      },
      {
        title: "Calendar",
        root: true,
        icon: "flaticon-event-calendar-symbol",
        page: "tours/calendar",
        translate: "MENU.CALENDAR",
        bullet: "dot"
      },
      {
        title: "Revisioni",
        root: true,
        icon: "flaticon-book",
        page: "tours/past",
        translate: "MENU.REVISIONI",
        bullet: "dot"
      },
      { section: "Management" },
      {
        title: "Assenze",
        root: true,
        icon: "flaticon-users-1",
        page: "assenze",
        translate: "MENU.ASSENZE",
        bullet: "dot"
      },
      {
        title: "Tours",
        root: true,
        icon: "flaticon-map-location",
        alignment: "left",
        toggle: "click",
        submenu: [
          {
            title: "New",
            root: true,
            bullet: "dot",
            page: "tour/new",
            translate: "MENU.NEWTOUR",
            icon: "flaticon-clock-2"
          },
          {
            title: "All",
            root: true,
            bullet: "dot",
            page: "tours/all",
            translate: "MENU.TOURS",
            icon: "flaticon2-setup"
          }
        ]
      },
      {
        title: "Guides",
        root: true,
        icon: "flaticon-users-1",
        page: "guides",
        translate: "MENU.GUIDES",
        bullet: "dot"
      },
      { section: "Settings" },
      {
        title: "Experiences",
        icon: "flaticon-book",
        root: true,
        alignment: "left",
        toggle: "click",
        submenu: [
          {
            title: "Create",
            root: true,
            bullet: "dot",
            page: "experience/new",
            translate: "MENU.NEWEXPERIENCE",
            icon: "flaticon2-plus-1"
          },
          {
            title: "All",
            root: true,
            bullet: "dot",
            page: "experiences",
            translate: "MENU.EXPERIENCES",
            icon: "flaticon2-setup"
          }
        ]
      },
      {
        title: "Tappe",
        root: true,
        alignment: "left",
        icon: "flaticon-placeholder-2",
        toggle: "click",
        submenu: [
          {
            title: "Create",
            root: true,
            bullet: "dot",
            page: "tappa/new",
            translate: "MENU.NEWTAPPA",
            icon: "flaticon2-plus-1"
          },
          {
            title: "All",
            root: true,
            bullet: "dot",
            page: "tappe",
            translate: "MENU.TAPPE",
            icon: "flaticon2-setup"
          }
        ]
      },
      {
        title: "Email automatiche",
        root: true,
        icon: "flaticon-paper-plane",
        translate: "MENU.NEWEMAIL",
        page: "email/new",
        bullet: "dot"
      },
      {
        title: "Piattaforme prenotazione",
        root: true,
        icon: "flaticon-time-3",
        translate: "MENU.NEWEMAIL",
        page: "fonti/new",
        bullet: "dot"
      },
      {

        title: "Metodi pagamento",
        root: true,
        icon: "flaticon-piggy-bank",
        translate: "MENU.NEWEMAIL",
        page: "payment-method/new",
        bullet: "dot"
      }
    ]
  }
};