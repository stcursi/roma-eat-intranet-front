export default {
      header: {
        self: {},
        items: [

        ]
      },
      aside: {
        self: {},
        items: [
          {
            title: "Summary",
            root: true,
            icon: "flaticon2-architecture-and-city",
            page: "summary",
            translate: "MENU.SUMMARY",
            bullet: "dot"
          },
          {
            title: "Calendar",
            root: true,
            icon: "flaticon-event-calendar-symbol",
            page: "tours/calendar",
            translate: "MENU.CALENDAR",
            bullet: "dot"
          },
          {
            title: "Revisioni",
            root: true,
            icon: "flaticon-book",
            page: "tours/past",
            translate: "MENU.REVISIONI",
            bullet: "dot"
          }
        ]
      }
    };