/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React, { useState } from "react";
import { Nav, Tab, Collapse } from "react-bootstrap";
import { toAbsoluteUrl } from "../utils/utils";
import { formatDate } from "../utils/date";
import clsx from "clsx";
import moment from 'moment';
import { useEffect } from "react";


const getOtherTourConfirmed = (tour, userId) => {
  const tourGuideId = tour.guide?.id;
  if (tourGuideId !== userId) {
    return tour.confermatoSecondaGuida;
  } else {
    return tour.confermato;
  }
}

export default function TourReminderTable({ tours, user, onSelectTour, onConfirmTour, onRefuseTour, onSelectToConfirm, onSelectedPartecipant, onSelectIter, paddingRight, className }) {
  const [key, setKey] = useState("All");
  const [selected, setSelected] = useState(null);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(null);

  useEffect(() => {
    setConfirmLoading(false);
    setCurrentIndex(null);
  }, [tours])

  const amountToReceive = (nextTour) => {

    let amount = 0;

    for (let partecipant of nextTour.partecipants) {

      if (partecipant.cash) {
        amount += partecipant.price ? Number(partecipant.price.replace(',', '.')) : 0;
      }
    }

    return amount;

  }

  const getTourPartecipantsFiltered = (t) => {
    const arrayFiltered = t.partecipants && t.partecipants.length ? t.partecipants.filter((p) => !p.annullato) : [];
    return arrayFiltered.length;
  }

  return (
    <div className={`card card-custom ${className}`}>
      {/* Head */}
      <div className="card-header d-flex justify-content-between border-0 pt-5">
        <h4 className="card-title align-items-start flex-column">
          <span className="card-label font-weight-bolder text-dark">
            I tuoi Tour !
          </span>
          <p className="small text-muted mt-3 font-weight-bold font-size-sm">
            Ricordati di confermare quelli in atttesa.
          </p>
        </h4>
        <div className="card-toolbar">
          <Tab.Container defaultActiveKey={key}>
            <Nav
              as="ul"
              onSelect={(_key) => {
                setKey(_key);
                onSelectToConfirm(_key === 'All' ? null : true)
              }}
              className="nav nav-pills nav-pills-sm nav-dark-75"
            >
              <Nav.Item className="nav-item" as="li">
                <Nav.Link
                  eventKey="All"
                  className={`nav-link py-2 px-4 ${key === "All" ? "active" : ""
                    }`}
                >
                  Tutti
                </Nav.Link>
              </Nav.Item>
              <Nav.Item className="nav-item" as="li">
                <Nav.Link
                  eventKey="ToConfirm"
                  className={`nav-link py-2 px-4 ${key === "ToConfirm" ? "active" : ""
                    }`}
                >
                  Da confermare
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </Tab.Container>
        </div>
      </div>
      {/* Body */}
      <div className="card-body pt-3 pb-0">
        <div className="table-responsive">
          <table className="table table-borderless table-vertical-center">
            <tbody>
              {tours && tours.map((tour, index) => {
                tour.partecipants = tour.partecipants && tour.partecipants.length ? tour.partecipants.filter((p) => !p.annullato) : [];
                return <>
                  <tr aria-expanded={false} aria-controls={"idRowTour_" + index}
                    onClick={() => {
                      let toSelected = selected === index ? null : index;
                      setSelected(toSelected);
                      onSelectTour(tour)
                    }}
                    className={"align-items-start rounded" + (index !== (tours.length - 1) ? " border-bottom" : "")}>
                    <td className="pl-0">
                      <img style={{ width: "50px", height: "50px" }}
                        src={toAbsoluteUrl("/media/icons/png/" + tour.experience.picture)}
                      ></img>
                    </td>
                    <td className="pl-0">
                      <h6
                        href="#"
                        className="text-dark-75 font-weight-bolder kt-font-brand font-size-lg"
                      >
                        {tour.experience && tour.experience.name ? tour.experience.name : "No value"}
                      </h6>
                      <div className="d-flex">
                        <span className="font-weight-bolder">MT:</span>
                        <p
                          className="text-muted mx-1 text-hover-primary"
                          href="#"
                        >
                          {tour.meetingPoint}
                        </p>
                      </div>
                      {tour.guide && tour.guide.id !== user.id ?
                        <div className="d-flex">
                          <p className="small text-muted mr-2">Seconda guida</p>
                          <div className="kt-notification__item-icon">
                            <i className="flaticon-users-1 kt-font-warning fa-lg" />
                          </div>
                        </div> : <></>}
                    </td>
                    <td className="text-left pt-2">
                      <span className="text-muted font-weight-bold">Giorno</span>
                      <span className="text-dark-75 font-weight-bolder d-block font-size-lg">
                        {formatDate(moment(tour.date).format('DD-MM-yyyy'))}
                      </span>
                      <span className="font-weight-bolder d-flex">Inizia:  <span className="text-muted mx-1 font-weight-500">
                        {moment(tour.startTime).format('HH:mm')}
                      </span></span>
                      <span className="font-weight-bolder d-flex">Finisce: <span className="text-muted mx-1 font-weight-500">
                        {moment(tour.endTime).format('HH:mm')}
                      </span></span>
                    </td>
                    <td className="text-center">
                      <span onClick={(e) => onSelectIter(e, tour)} className="fas fa-map-marked-alt fa-2x kt-font-instagram" />
                      <p className="small text-muted">Itinerario</p>
                    </td>
                    <td className="text-center mt-3">
                      {getOtherTourConfirmed(tour, user.id) ? <div><button className={`border-0 badge badge-lg badge-success badge-inline ${clsx(
                        {
                          "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": (confirmLoading && index === currentIndex)
                        }
                      )}`}
                        style={{ paddingRight }}
                        onClick={(e) => {
                          setConfirmLoading(true);
                          setCurrentIndex(index);
                          return onConfirmTour(e, tour, false, false)
                        }}>
                        Confermato
                      </button><p className="small text-muted">Click per annullare</p></div> : <div>
                        <button className={`border-0 badge badge-lg badge-info badge-inline ${clsx(
                          {
                            "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": (confirmLoading && index === currentIndex)
                          }
                        )}`}
                          style={{ paddingRight }}
                          onClick={(e) => {
                            setConfirmLoading(true);
                            setCurrentIndex(index);
                            return onConfirmTour(e, tour, true, false)
                          }}>
                          In attesa
                        </button><p className="small text-muted">Click per confermare</p>
                      </div>}
                    </td>
                    <td className="text-right pt-2">
                      <button className=" border-0 badge badge-lg badge-danger badge-inline mt-1"
                        onClick={(e) => onRefuseTour(e, tour)}>
                        Rifiuta
                      </button>
                    </td>
                    <td className="text-center mt-0">
                      <div>
                        <span className="h4 font-weight-bold kt-font-google">{tour.partecipants ? getTourPartecipantsFiltered(tour) : ""}</span>
                        <span onClick={(e) => onSelectedPartecipant(e, tour)} className="fas fa-users fa-2x kt-font-google mx-2" />
                      </div>
                      <p className="small text-muted">Dettaglio partecipanti</p>
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="6">
                      <Collapse in={(selected === index)}>
                        <div id={"idRowTour_" + index} className="row bg-light rounded">
                          <div className="col-lg-4 p-2">
                            <p className="h6 kt-font-facebook">NumeroPartecipanti: <span className="text-info mx-1">{tour.partecipants ? tour.partecipants.length : ""}</span></p>
                            <p className="h6 kt-font-facebook">Totale da incassare: <span className="text-info mx-1">{amountToReceive(tour) ? amountToReceive(tour) + " €" : "0 €"}</span></p>
                          </div>
                          <div className="col-g-8 p-1">
                            <span className="h6 kt-font-facebook">Note ed eventuali: </span>
                            <span>{tour.noteGuida}</span>
                          </div>
                        </div>
                      </Collapse>
                    </td>
                  </tr>
                </>
              })}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
