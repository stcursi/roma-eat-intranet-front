/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import PerfectScrollbar from "react-perfect-scrollbar";
import { toAbsoluteUrl } from "../utils/utils";
import PortletHeaderDropdown from "../partials/content/CustomDropdowns/PortletHeaderDropdown";
import moment from 'moment';

const perfectScrollbarOptions = {
  wheelSpeed: 2,
  wheelPropagation: false
};

let arrayOfTappe;
let lastTime = null;

const setTappaTime = (tappe, orderTappe, tour) => {
  arrayOfTappe = [];
  const startTime = moment(tour.startTime);

  for (let id of orderTappe.split(',')) {
    for (let tappa of tappe) {
      if (parseInt(id) === tappa.id) {
        let tappaObj = {
          time: startTime.format('HH:mm'),
          name: tappa.name,
          type: tappa.type,
          address: tappa.address,
          note: tappa.note
        }
        arrayOfTappe.push(tappaObj);
        startTime.add(parseInt(tappa.duration), 'minutes');
        lastTime = startTime;
        break;
      }
    }
  }


}

const getTappaColor = (type, index) => {
  if (index === 0) return "success"
  switch (type) {
    case "archeologica":
      return "warning";
    case "enogastronomica":
      return "brand";
  }

  return "primary";
}

export default function TourIter({ tappe, orderTappe, tour }) {

  setTappaTime(tappe, orderTappe, tour);

  return (
    <>
      <div className="kt-portlet">
        <div className="kt-portlet__head">
          <div className="kt-portlet__head-label">
            <h3 className="kt-portlet__head-title">Itinerario del Tour</h3>
          </div>
          {/* <PortletHeaderDropdown /> */}
        </div>
        <div className="kt-portlet__body">
          {/* style="max-height: 50vh;" */}
          <PerfectScrollbar
            options={perfectScrollbarOptions}
            style={{ maxHeight: "50vh", position: "relative" }}
          >
            <div className="kt-timeline-v2 ps ps--active-y">
              <div className="kt-timeline-v2__items kt-padding-top-25 kt-padding-bottom-30">
                {tappe && arrayOfTappe.map((tappa, index) =>
                  <div className="kt-timeline-v2__item">
                    <span className="kt-timeline-v2__item-time">{tappa.time}</span>
                    <div className="kt-timeline-v2__item-cricle">
                      <i className={"fa fa-genderless kt-font-" + getTappaColor(tappa.type, index)} />
                    </div>
                    <div className="kt-timeline-v2__item-text kt-padding-top-5">
                      <div className="d-flex flex-column">
                        <p className="h6">{tappa.name}</p>
                        <span className="kt-font-google">{tappa.address}</span>
                        <span className="small text-muted">{tappa.note}</span>
                      </div>
                    </div>
                    <div className="kt-list-pics kt-list-pics--sm kt-padding-l-20" />
                  </div>)}
                <div className="kt-timeline-v2__item last-child">
                  <span className="kt-timeline-v2__item-time">{lastTime.format('HH:mm')}</span>
                  <div className="kt-timeline-v2__item-cricle">
                    <i className="fa fa-genderless kt-font-info" />
                  </div>
                  <div className="kt-timeline-v2__item-text kt-padding-top-5">
                    <p className="h6">Fine Tour</p>
                  </div>
                  <div className="kt-list-pics kt-list-pics--sm kt-padding-l-20" />
                </div>
              </div>
            </div>
          </PerfectScrollbar>
        </div>
      </div>
    </>
  );
}
