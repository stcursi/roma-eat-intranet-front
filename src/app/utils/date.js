import moment from "moment";

export function getFormatX(ts) {
    
}

export function formatDate(date, italian) {
    let toReturn = "Invalid Date";
    if (date) {
        let dateSplitted = date.split('T');
        if (dateSplitted && dateSplitted.length > 0) toReturn = dateSplitted[0];
    }

    if (italian) {
        let values = toReturn.split('-');
        if (values && values.length === 3) {
            toReturn = values[2] + "-" + values[1] + "-" + values[0]
        }
    }

    return toReturn;
}

export function formatPickerDate(stringDate) {
    let arraySplit = stringDate ? stringDate.split(':') : null;
    if (arraySplit && arraySplit.length > 1) {
        return moment().hour(arraySplit[0]).minute(arraySplit[1]);
    } else {
        return moment();
    }
}