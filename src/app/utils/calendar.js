export function getLegenda(color) {
    switch (color) {
        case 'VERDE':
            return 'Tutte le guide indicate hanno confermato e ci sono partecipanti';
        case 'ROSSO':
            return 'Evento annullato o tutti i partecipanti hanno annullato';
        case 'VIOLETTO':
            return 'La guida o una di quelle indicate deve confermare, ma non ci sono ancora partecipanti';
        case 'AZZURRO':
            return 'Tutte le guide indicate hanno confermato ma non ci sono partecipanti';
        case 'GIALLO':
            return 'Ci sono dei partecipanti ma non sono state indicate le guide';
        case 'ARANCIONE':
            return 'Ci sono i partecipanti ma la guida non ha confermato';
        case 'ROSA':
            return 'Ci sono i partecipanti ma una delle guide indicate non ha confermato';
        case 'MARRONE':
            return 'Ci sono i partecipanti ma entrambe le guide non hanno confermato';
        case 'GRIGIO':
            return 'Data aperta ma non ci sono nè i partecipanti nè le guide';
        case 'PICCIONE':
            return 'Assenza';
        default:
            return 'Caso non gestito dai colori'
    }
}

export const checkIfEveryPartecipantsRefused = (partecipants) => {

    if (!partecipants || partecipants.length === 0) {
        return false;
    }
    let countPresenti = 0;
    if (partecipants && partecipants.length) {
        partecipants.forEach((p) => {
            if (!p.annullato) {
                countPresenti++;
            }
        })
    }

    return countPresenti === 0;
}

export function selectColor(event) {

    let backgroundColor = '#C0C0C0'; // Grigio chiaro
    let borderColor = '#808080'; // Grigio chiaro
    let legenda = 'Data aperta ma non ci sono nè i partecipanti nè le guide';
    let textColor;
    const partencipantsLength = event.partecipants?.length || 0;


    if (event.isAssenza) {
        return {
            backgroundColor: '#606060', // Grigio piccione
            borderColor: '#202020', // Grigio piccione
            color: "#FFFFFF",
            legenda: 'Assenza'
        }
    }

    if (event.annullato || checkIfEveryPartecipantsRefused(event.partecipants)) {
        backgroundColor = "#FF3333"; // rosso
        borderColor = "#990000";
        legenda = 'Evento annullato o tutti i partecipanti hanno annullato';
        textColor = "#FFFFFF";

        return {
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            legenda: legenda,
            color: textColor || null
        };
    }

    if (partencipantsLength && !event.guide && !event.secondGuide) {

        backgroundColor = "#F8F32B"; // Giallo semaforo
        borderColor = "#F6DD39";
        legenda = 'Ci sono dei partecipanti ma non sono state indicate le guide';
        textColor = "black";

        return {
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            legenda: legenda,
            color: textColor || null
        };
    }

    if (event.guide && !event.secondGuide) {

        if (event.confermato && partencipantsLength) {
            backgroundColor = "#33FF33"; // Verde
            borderColor = "#009900";
            legenda = 'Tutte le guide indicate hanno confermato e ci sono partecipanti';
            textColor = "black";
        } else if (!event.confermato && !partencipantsLength) {
            backgroundColor = "#F578F9"; // Violetto
            borderColor = "#009900";
            legenda = 'La guida o una di quelle indicate deve confermare, ma non ci sono ancora partecipanti';
            textColor = "#FFFFFF";
        } else if (!event.confermato && partencipantsLength) {
            backgroundColor = "#FF7514"; // Arancione
            borderColor = "#009900";
            legenda = 'Ci sono i partecipanti ma la guida non ha confermato';
            textColor = "black";
        } else if (event.confermato && !partencipantsLength) {
            backgroundColor = "#99FFFF"; // Azzurrino
            borderColor = "#0066CC";
            legenda = 'Tutte le guide indicate hanno confermato ma non ci sono partecipanti';
            textColor = "blue";
        }

        return {
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            legenda: legenda,
            color: textColor || null
        };
    }

    if (event.secondGuide && !event.guide) {

        if (event.confermatoSecondaGuida && partencipantsLength) {
            backgroundColor = "#33FF33"; // Verde
            borderColor = "#009900";
            legenda = 'Tutte le guide indicate hanno confermato e ci sono partecipanti';
            textColor = "black";
        } else if (!event.confermatoSecondaGuida && !partencipantsLength) {
            backgroundColor = "#F578F9"; // Violetto
            borderColor = "#009900";
            legenda = 'La guida o una di quelle indicate deve confermare, ma non ci sono ancora partecipanti';
            textColor = "#FFFFFF";
        } else if (!event.confermatoSecondaGuida && partencipantsLength) {
            backgroundColor = "#FF7514"; // Arancione
            borderColor = "#009900";
            legenda = 'Ci sono i partecipanti ma la guida non ha confermato';
            textColor = "black";
        } else if (event.confermatoSecondaGuida && !partencipantsLength) {
            backgroundColor = "#99FFFF"; // Azzurrino
            borderColor = "#0066CC";
            legenda = 'Tutte le guide indicate hanno confermato ma non ci sono partecipanti';
            textColor = "blue";
        }

        return {
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            legenda: legenda,
            color: textColor || null
        };

    }


    if (event.secondGuide && event.guide) {

        if (event.confermatoSecondaGuida && event.confermato && partencipantsLength) {
            backgroundColor = "#33FF33"; // Verde
            borderColor = "#009900";
            legenda = 'Tutte le guide indicate hanno confermato e ci sono partecipanti';
            textColor = "black";
        } else if ((event.confermatoSecondaGuida && event.confermato) && !partencipantsLength) {
            backgroundColor = "#99FFFF"; // Azzurrino
            borderColor = "#0066CC";
            legenda = 'Tutte le guide indicate hanno confermato ma non ci sono partecipanti';
            textColor = "blue";
        } else if ((!event.confermatoSecondaGuida || !event.confermato) && !partencipantsLength) {
            backgroundColor = "#F578F9"; // Violetto
            borderColor = "#009900";
            legenda = 'La guida o una di quelle indicate deve confermare, ma non ci sono ancora partecipanti';
            textColor = "#FFFFFF";
        } else if ((!event.confermatoSecondaGuida && !event.confermato) && partencipantsLength) {
            backgroundColor = "#CC6600"; // marrone
            borderColor = "#CC6600";
            legenda = 'Ci sono i partecipanti ma entrambe le guide non hanno confermato';
            textColor = "#FFFFFF";
        } else if ((!event.confermatoSecondaGuida || !event.confermato) && partencipantsLength) {
            backgroundColor = "#FFC0CB"; // Rosa
            borderColor = "#009900";
            legenda = 'Ci sono i partecipanti ma una delle guide indicate non ha confermato';
            textColor = "black";
        }


        return {
            backgroundColor: backgroundColor,
            borderColor: borderColor,
            legenda: legenda,
            color: textColor || null
        };
    }

    // it means that is without any partecipants and guide - PICCIONE
    return {
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        legenda: legenda,
        color: textColor || null
    };
}