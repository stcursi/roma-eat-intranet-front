export function initAxios(axios, store) {

  axios.interceptors.request.use(
    config => {
      const {
        auth: { authToken }
      } = store.getState();

      if (authToken) {
        config.headers.Authorization = `Bearer ${authToken}`;
      }

      let baseURL;
      
      baseURL = "https://romaeatbackend.herokuapp.com";
      // baseURL = "http://localhost:9000";

      console.log('set base url');
      config.baseURL = baseURL;

      return config;
    },
    err => {
      console.error(err)
      Promise.reject(err)
    }
  );

  axios.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    console.error(error);
    if (error && error.response && error.response.status === 401 && error.response.data === "expired") {
      window.location = '/logout';
    }
    return Promise.reject(error);
  });

}


