import React, { Component } from "react";
import { connect } from "react-redux";
import CustomRangePicker from "../../partials/content/CustomRangePicker";
import { injectIntl } from "react-intl";
import { getAllByRole } from "../../crud/user.crud";
import { Button, Modal } from "react-bootstrap";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import { formatDate } from "../../utils/date";
import Select from 'react-select';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './Assenze.css';

import moment from 'moment';
import { deleteBulkAssenze, getAllAssenze } from "../../crud/assenza.crud";

const reactSelectStyles = {
    container: (base, state) => ({
        ...base,
        opacity: state.isDisabled ? ".5" : "1",
        backgroundColor: "transparent",
        margin: "0 10px 10px 0",
        width: "40%",
        zIndex: "999",
    })
};

class AssenzePage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            assenze: [],
            selectedEvent: null,
            allGuides: [],
            filters: {},
            selectedGuide: null,
            rowSelected: [],
            showRangeCalendarModal: false,
            from: null,
            to: null,
            showSuccessAlert: false,
            showFailAlert: false
        };
    }

    componentDidMount() {


        const dataFrom = moment().startOf('day').toISOString();
        const dataTo = moment().add(3, 'months').startOf('day').toISOString();

        const params = { from: dataFrom, to: dataTo };


        this.getAssenzeByFilter(params);

        getAllByRole('GUIDE')
            .then((res) => {
                this.setState({ allGuides: res.data });
            })
            .catch((error) => {
                console.error('[getAllByRole-GUIDE] - ', error);
            })

    }

    getAssenzeByFilter(params) {

        getAllAssenze(params)
            .then((res) => {
                if (res && res.data) this.setState({ assenze: res.data });
            })
            .catch((error) => {
                console.error('[Calendar] - getAllAssenze: ', error);
            })

    }

    componentWillUnmount() {


    }

   
    handleDeleteAssenze = () => {

        const { rowSelected, filters } = this.state;

        let assenze = [];

        if (rowSelected) rowSelected.map(assenzaId => assenze.push(assenzaId));

        if (assenze.length) {
            deleteBulkAssenze(assenze)
                .then(() => this.getAssenzeByFilter(filters))
                .catch((e) => console.log('error: ', e));
        }

    }

    prepareGuideFilter(allGuides, selectedFilters) {
        let toReturn = [];
        let toAssignSelected = false;
        for (let guide of allGuides) {
            if (selectedFilters) {
                let arrayOfId = selectedFilters.split(',');
                for (let selected of arrayOfId) {
                    if (parseInt(selected) === guide.id) {
                        toReturn.push({ value: guide.id, label: guide.fullname });
                    }
                    if (selected === "toAssign") toAssignSelected = true;
                }
            } else {
                toReturn.push({ value: guide.id, label: guide.fullname });
            }
        }

        if (selectedFilters) {
            if (toAssignSelected) toReturn.push({ value: "toAssign", label: "Da assegnare" });
            return toReturn;
        }

        toReturn.push({ value: "toAssign", label: "Da assegnare" });
        toReturn.push({ value: null, label: "Tutte" });

        return toReturn;
    }


    render() {

        return (
            <div className="container-fluid">
                <main id="content" role="main">
                    {/* <!-- Upload Form Section --> */}
                    <div className="container space-2">
                        <div className="text-center">
                            <h1 className="h2 kt-font-brand font-weight-medium">Assenze</h1>
                            <p>Seleziona le assenze da eliminare</p>
                        </div>
                        <div className="my-5">
                            {this.renderTable()}
                            {this.renderRangeCalendarModal()}
                            {this.renderSuccessModal()}
                            {this.renderFailModal()}
                        </div>
                    </div>
                </main>
            </div>)

    }

    renderTable() {
        const { assenze, allGuides, filters, rowSelected } = this.state;

        if (!allGuides.length) return "";

        let pageOptions = { pageCurrent: null, paginationSize: null };

        // const columns = this.renderColumns(this.props.discovery);
        pageOptions.paginationSize = 50;

        let selectedGuideFilter = filters.guide ? this.prepareGuideFilter(allGuides, filters.guide) : null;

        const GuideOptions = this.prepareGuideFilter(allGuides);

        const assenzeParsed = this.renderRows(assenze);

        const sizePerPageList = [{
            text: '5', value: 5
        }, {
            text: '10', value: 10
        }, {
            text: '25', value: 25
        }, {
            text: '50', value: 50
        }, {
            text: '100', value: 100
        }, {
            text: '250', value: 250
        }, {
            text: '500', value: 500
        }];

        const sizePerPageOptionRenderer = ({
            text,
            page,
            onSizePerPageChange
        }) => (
            <li
                key={text}
                role="presentation"
                className="dropdown-item"
            >
                <a
                    href="#"
                    tabIndex="-1"
                    key={text}
                    role="menuitem"
                    data-page={page}
                    onMouseDown={(e) => {
                        onSizePerPageChange(page);
                        e.preventDefault();
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    {text}
                </a>
            </li>
        );



        const onPaginationChange = (sizePerPage, page) => {
            console.log('size per page: ', sizePerPage);
            console.log('page: ', page);
        }

        const rowEvents = {
            // todo
        };

        const optionsPagination = {
            sizePerPageOptionRenderer: sizePerPageOptionRenderer,
            sizePerPage: pageOptions.paginationSize,
            sizePerPageList: sizePerPageList,
            onSizePerPageChange: onPaginationChange,
            page: pageOptions.pageCurrent || 1
        };

        const rowClasses = 'table-small link-cursor-pointer';

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: false,
            onSelect: (row, isSelect, rowIndex, e) => {
                const { rowSelected } = this.state;
                if (isSelect) {
                    rowSelected.push(row.id);
                } else {
                    let idx = rowSelected.indexOf(row.id);
                    if (idx !== -1) {
                        rowSelected.splice(idx, 1);
                    }
                }

                this.setState({ rowSelected: rowSelected });
            },
            onSelectAll: (isSelect, rows, e) => {
                let { rowSelected } = this.state;
                if (isSelect) {
                    rows.map((row) => rowSelected.push(row.id));
                } else {
                    rowSelected = [];
                }

                this.setState({ rowSelected: rowSelected });
            }
        };

        const columns = [{
            dataField: 'startTime',
            text: 'Inizio',
            sort: true,
        }, {
            dataField: 'endTime',
            text: 'Fine',
            sort: true,
        }, {
            dataField: 'guide',
            text: 'Guida',
            sort: true,
        }, {
            dataField: 'allDay',
            text: 'Tutto il giorno'
        }, {
            dataField: 'multiDays',
            text: 'Più giorni'
        }
        ];


        const defaultSorted = [{
            dataField: 'startTime', // if dataField is not match to any column you defined, it will be ignored.
            order: 'asc' // desc or asc
        }];

        return <ToolkitProvider
            keyField="assenzeTable"
            // trClassName={"text-secondary no-min-height"}
            data={assenzeParsed}
            columns={columns}
            search={{
                searchFormatted: true
            }}
        >
            {
                props => (
                    <div>
                        <div className="d-flex flex-row justify-content-between align-items-start mb-2">
                            <div className="flex-grow-0">
                                <button className="btn btn-sm btn-icon btn-facebook mt-1 mx-3"
                                    onClick={() => this.setState({ showRangeCalendarModal: true })}>
                                    <i className="flaticon-event-calendar-symbol" />
                                </button>
                            </div>
                            <div className="d-flex justify-content-end mt-1">
                                <button className="btn btn-sm btn-warning ml-1 mr-2 d-block"
                                    disabled={rowSelected.length === 0}
                                    onClick={this.handleDeleteAssenze}>
                                    Elimina
                                </button>
                            </div>
                            <div className="d-flex flex-grow-1 justify-content-end mb-2">
                                <Select
                                    className="btn-xs no-min-height basic-multi-select"
                                    key="selectFilter"
                                    styles={reactSelectStyles}
                                    isMulti
                                    placeholder="Filtra per guida"
                                    value={selectedGuideFilter}
                                    onChange={this.handleChangeGuideFilterSubmit}
                                    options={GuideOptions}
                                />
                            </div>
                        </div>
                        <BootstrapTable {...props.baseProps} striped bordered keyField='id'
                            rowClasses={rowClasses}
                            rowEvents={rowEvents}
                            defaultSorted={defaultSorted}
                            selectRow={selectRow}
                            pagination={paginationFactory(optionsPagination)}
                        />
                    </div>
                )
            }
        </ToolkitProvider>

    }

    renderRows(assenze) {

        let toReturn = [];

        for (let assenza of assenze) {
            let parseAssenza = {
                id: assenza.id,
                startTime: moment(assenza.startTime).format('DD-MM-yyyy HH:mm'),
                endTime: moment(assenza.endTime).format('DD-MM-yyyy HH:mm'),
                allDay: assenza.allDay,
                multiDays: assenza.multiDays,
                guide: assenza.guide ? assenza.guide.fullname : "Da assegnare"
            }
            toReturn.push(parseAssenza);
        }

        return toReturn;
    }

    renderRangeCalendarModal() {

        const { showRangeCalendarModal, from, to } = this.state;

        const handleClose = () => {
            const { from, to, filters } = this.state;
            this.setState({ showRangeCalendarModal: false })
            if (from && to) {
                filters.from = from;
                filters.to = to;
                this.getAssenzeByFilter(filters);
            }
        };

        const updateFromTo = (range) => {
            this.setState({ from: range.from, to: range.to })
        }


        return (<Modal show={showRangeCalendarModal} size="md" className="w-100" onHide={handleClose} >

            <Modal.Header closeButton>
                <Modal.Title className="h6">Scegliere il periodo di tempo</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <CustomRangePicker className="w-100" updateFromTo={updateFromTo} from={from} to={to} />
            </Modal.Body>
            <Modal.Footer>
                <Button className="btn btn-facebook" onClick={handleClose}>
                    Applica
                </Button>
            </Modal.Footer>
        </Modal>)
    }

    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState({ showSuccessAlert: false })
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }

}

export default injectIntl(
    connect(
    )(AssenzePage)
);