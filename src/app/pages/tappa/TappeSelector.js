import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';


class TappeSelector extends Component {

    constructor(props) {
        super(props);

        this.state = {
            orderTappe: this.props.orderTappe,
            tappe: this.props.tappe,
            selectedTappe: this.props.selectedTappe
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {


    }

    render() {

        let { tappe } = this.state;


        const renderOrderTappaLine = (tappa, idx) => {

            if (!tappa) return "";

            const onChange = (e) => {

                e.preventDefault();

            //     const { selectedTappe } = this.state;

            //     if (e && e.currentTarget && e.currentTarget.form) {
            //         let currentForm = e.currentTarget.form;

            //         for (let seltappa of selectedTappe) {

            //             if (currentForm["tappa_" + seltappa]) {

            //                 this.props.orderTappe[currentForm["tappa_" + seltappa].value] = seltappa;
            //             }
            //         }

            //     }

            }

            const findOrder = (currentTappa) => {
                const { orderTappe } = this.state;

                for (let idx of Object.keys(orderTappe)) {
                    if (currentTappa && orderTappe[idx] && orderTappe[idx].toString() === currentTappa.toString()) {
                        return idx;
                    }
                }

                return "";
            }

            const name = giveTappaName(tappa);

            return <div className="row" key={"order_" + idx}>
                <div className="col-lg-8">
                    <span>{name}</span>
                </div>
                <div className="col-lg-4">
                    <input id={"tappa_" + tappa} type="number" className="form-control" defaultValue={findOrder(tappa)} onChange={(onChange)} aria-describedby="order" placeholder="Inserisci l'ordine" data-error="Inserire solo un numero" required />
                </div>
            </div>
        }

        const giveTappaName = (currentTappa) => {

            for (let t of tappe) {
                if (t.id.toString() === currentTappa.toString()) return t.name;
            }
        }

        const checkSelected = (currentTappa) => {

            let idx = this.props.selectedTappe.indexOf(currentTappa.id);

            return (idx !== -1);
        }

        const ITEM_HEIGHT = 48;
        const ITEM_PADDING_TOP = 8;

        const MenuProps = {
            PaperProps: {
                style: {
                    maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                    width: 250,
                },
            },
        };

        return (
            <div className="row">
                <div className="col-lg-6 mb-3">

                    <InputLabel className="mb-4" id="tappeLabel">Tappe del tour</InputLabel>

                    <FormControl className="w-100">
                        <Select
                            labelId="tappeLabel"
                            id="TappeMultipleCheckbox"
                            multiple
                            value={this.props.selectedTappe}
                            onChange={this.props.updateTappe}
                            input={<Input />}
                            renderValue={(selected) => selected.map((t) => giveTappaName(t)).join(", ")}
                            MenuProps={MenuProps}
                        >
                            {tappe.map((tappa, idx) => (
                                <MenuItem key={tappa.name + "_" + idx} value={tappa.id}>
                                    <Checkbox checked={checkSelected(tappa)} />
                                    <ListItemText primary={tappa.name} />
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    {/* <!-- End Input --> */}
                </div>
                <div className="col-lg-6 mb-3">
                    {/* <!-- Input --> */}
                    <div className="form-group mb-5">
                        <div className="js-focus-state">
                            <label className="form-label" htmlFor="ordineTappe">
                                <span className="fas fa-map-list mr-2"></span>Ordine Tappe</label>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                </div>
                                <div className="container" name="ordineTappe" id="ordineTappe">
                                    {this.props.selectedTappe.map((tappa, idx) => renderOrderTappaLine(tappa, idx))}
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <!-- End Input --> */}
                </div>
            </div>
        );

    }

}


export default injectIntl(
    connect(
    )(TappeSelector)
);