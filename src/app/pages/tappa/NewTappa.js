import React, { Component } from "react";
import { toAbsoluteUrl } from "../../utils/utils";
import { getAllEmail } from "../../crud/email.crud";
import { getAllTappe } from "../../crud/tappa.crud";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { saveTappa } from "../../crud/tappa.crud";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";

class NewTappa extends Component {

  constructor(props) {
    super(props);

    this.myFormRef = React.createRef();

    this.state = {
      showSuccessAlert: false,
      showFailAlert: false
    };
  }

  componentDidMount() {

  }

  componentWillUnmount() {

  }

  initializeNewTappa(data) {
    let toReturn = {};

    toReturn.name = data.tappaName ? data.tappaName.value : null;
    toReturn.address = data.indirizzo ? data.indirizzo.value : null;
    toReturn.payment = data.payment ? data.payment.value : null;
    toReturn.type = data.type ? data.type.value : null;
    toReturn.note = data.note ? data.note.value : null;
    toReturn.duration = data.duration ? data.duration.value : null;

    return toReturn;
  }

  render() {

    const saveNewTappa = (event) => {
      event.preventDefault();

      if (event.target) {
        const newTappa = this.initializeNewTappa(event.target);

        saveTappa(newTappa)
          .then(() => {
            this.setState({ showSuccessAlert: true })
          })
          .catch(() => {
            this.setState({ showFailAlert: true })
          })
          .finally(() => {
            this.myFormRef.reset();
          })
      }

    }

    return (
      <div className="container-fluid">
        <main id="content" role="main">
          {/* <!-- Upload Form Section --> */}
          <div className="container space-2">
            <div className="w-lg-75 mx-lg-auto">
              {/* <!-- Title --> */}
              <div className="text-center mb-9">
                <h1 className="h2 kt-font-brand font-weight-medium">Crea una nuova Tappa</h1>
                <p>Inserisci i valori nei campi che seguono</p>
              </div>
              {/* <!-- End Title --> */}

              <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewTappa} ref={(el) => this.myFormRef = el}>
                {/* <!-- Listing Agent Information --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="tappaName">Nome</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="tappaNameLabel">
                                <span className="fas fa-file-signature"></span>
                              </span>
                            </div>
                            <input type="text" className="form-control" id="tappaName" placeholder="Nome nuova tappa (Tipo sergio nzucchero)" aria-label="Nome tappa" aria-describedby="tappaNameLabel" required
                              data-msg="Please enter a listing agent name."
                              data-error-class="u-has-error"
                              data-success-class="u-has-success" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="payment">Tipologia pagamento</label>
                          <div className="input-group d-flex align-items-center">
                            <select class="form-control" id="payment" required>
                              <option value="" hidden>Scegliere il pagamento</option>
                              <option value="anticipato">Anticipato</option>
                              <option value="fine mese">Fine mese</option>
                              <option value="guida">Guida</option>
                              <option value="nessun pagamento">Nessun pagamento</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="type">Categoria tappa</label>
                          <div className="input-group d-flex align-items-center">
                            <select class="form-control" id="type" required>
                              <option value="" hidden>Scegliere la categoria</option>
                              <option value="enogastronomica">Enogastronomica</option>
                              <option value="archeologica">Archeologica</option>
                              <option value="transitoria">Transitoria</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="duration">Durata tappa</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="tappaDurationLabel">
                                <span className="fas fa-clock"></span>
                              </span>
                            </div>
                            <input type="number" className="form-control" id="duration" placeholder="Durata della tappa comprensiva di spostamento" aria-label="Durata tappa" aria-describedby="tappaDurationLabel" required
                              data-msg="Inserire durata tappa"
                              data-error-class="u-has-error"
                              data-success-class="u-has-success" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>

                </div>
                {/* <!-- End Listing Agent Information --> */}


                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Indirizzo e note</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="js-form-message mb-3">
                        <label className="form-label">
                          Indirizzo
                    <span className="text-danger">*</span>
                        </label>

                        <div className="input-group">
                          <textarea className="form-control" rows="3" name="text" id="indirizzo" placeholder="Aggiungi indirizzo .. (Via/Piazza, numero civico, CAP)" aria-label="Indirizzo" required
                            data-msg="Please enter a property description."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success"></textarea>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="js-form-message mb-3">
                        <label className="form-label">
                          Note e varie
                    <span className="text-danger">*</span>
                        </label>

                        <div className="input-group">
                          <textarea className="form-control" rows="6" name="text" id="note" placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                            data-msg="Please enter a property description."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success"></textarea>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>
                </div>
                {/* <!-- End Upload Images --> */}

                <div className="row">
                  <div className="col-lg-3 col-md-2 col-sm-1">
                    <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          {/* <!-- End Upload Form Section --> */}
        </main>
        {this.renderSuccessModal()}
        {this.renderFailModal()}
      </div>
    )
  }

  renderSuccessModal() {
    const { showSuccessAlert } = this.state;

    const onCloseSuccessModal = () => {
      this.setState({ showSuccessAlert: false });
    };

    return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
  }

  renderFailModal() {
    const { showFailAlert } = this.state;

    const onCloseFailModal = () => {
      this.setState({ showFailAlert: false })
    };

    return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
  }



}

export default injectIntl(
  connect(
  )(NewTappa)
);