import React, { Component } from "react";
import { getAllTappe, editTappa, deleteTappa } from "../../crud/tappa.crud";
import { Modal } from "react-bootstrap";

import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";

import { connect } from "react-redux";
import { injectIntl } from "react-intl";


class Tappe extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tappe: null,
            selectedTappa: null,
            showEditModal: false
        };
    }

    componentDidMount() {
        this.getTappe();
    }

    getTappe() {
        getAllTappe()
            .then((res) => {
                if (res && res.data) this.setState({ tappe: res.data });
            })
            .catch((error) => {
                console.error("[NewExperience] - getAllTappe: ", error);
            })

    }

    componentWillUnmount() {

    }


    initializeNewTappa(data) {
        let toReturn = {};

        toReturn.name = data.tappaName ? data.tappaName.value : null;
        toReturn.address = data.indirizzo ? data.indirizzo.value : null;
        toReturn.payment = data.payment ? data.payment.value : null;
        toReturn.type = data.type ? data.type.value : null;
        toReturn.note = data.note ? data.note.value : null;
        toReturn.duration = data.duration ? data.duration.value : null;

        return toReturn;
    }

    handleDeleteTappa(data) {

        deleteTappa(data.id)
            .then(() => this.getTappe());
    }


    render() {

        const { tappe } = this.state;

        if (!tappe) return "";

        return <div className="container-fluid">
            <main id="content" role="main">
                {/* <!-- Upload Form Section --> */}
                <div className="row">
                    <div className="col-lg-12">
                        {/* <!-- Title --> */}
                        <div className="row align-items-center mb-5">
                            <div className="col-lg-12">
                                <h3>Tutte le tappe: {tappe.length}</h3>
                            </div>

                        </div>
                        {/* <!-- End title --> */}

                        {/* <!-- Tappe --> */}
                        <div className="row mx-n2 mb-5">
                            {tappe.map(tappa => <div key={"tappa_" + tappa.id} className="col-sm-8 col-md-6 col-lg-4 px-2 px-sm-3 mb-3 mb-sm-5">
                                <div className="card border shadow-none text-center">

                                    <div className="mt-3">
                                        {/* <img className="card-img-top" src="../../assets/img/300x180/img3.jpg" alt="Image Description" /> */}
                                        <div className="d-flex justify-content-between align-items-center">
                                            <h5 className="mx-auto mt-2 pl-5">{tappa.name}</h5>
                                            <button type="button" className="btn btn-pill btn-icon btn-light transition-3d-hover"
                                                onClick={() => this.setState({ showEditModal: true, selectedTappa: tappa })}>
                                                <span className="p-0 far fa-edit kt-font-brand btn-icon__inner"></span>
                                            </button>
                                            <button type="button" className="btn btn-pill btn-icon btn-light transition-3d-hover"
                                                onClick={() => this.handleDeleteTappa(tappa)}>
                                                <span className="p-0 fas fa-trash-alt kt-font-danger btn-icon__inner"></span>
                                            </button>
                                        </div>
                                        <div className="text-center my-2">
                                            {tappa.type === "enogastronomica" ?
                                                <span className="badge badge-success badge-pill">{tappa.type}</span> : tappa.type === "archeologica" ?
                                                    <span className="badge badge-secondary badge-pill">{tappa.type}</span> :
                                                    <span className="badge badge-warning badge-pill">{tappa.type}</span>
                                            }
                                        </div>
                                    </div>

                                    <div className="card-body">

                                        <div className="mb-2">
                                            <span className="d-inline-block font-weight-bold mb-1">{tappa.address}</span>
                                            <p className="small">Durata: {tappa.duration}'</p>
                                            <p className="small">Pagament: <span className="text-dark">{tappa.payment}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>)}
                        </div>
                        {/* <!-- End Products --> */}

                        {/* <!-- Divider --> */}
                        < div className="d-lg-none" >
                            <hr className="my-7 my-sm-11" />
                        </div>
                        {/* <!-- End Divider --> */}
                    </div>

                </div>
            </main>
            {this.renderEditModal()}
            {this.renderSuccessModal()}
            {this.renderFailModal()}
        </div>
    }

    renderEditModal() {

        const { selectedTappa } = this.state;

        if (!selectedTappa) return "";

        const handleSubmit = (event) => {
            event.preventDefault();

            if (event.target) {
                const params = this.initializeNewTappa(event.target);

                for (let key of Object.keys(params)) {
                    selectedTappa[key] = params[key];
                }

                editTappa(selectedTappa.id, params)
                    .then(() => {
                        this.setState({ showSuccessAlert: true, showEditModal: false, selectedTappa: selectedTappa })
                    })
                    .catch(() => {
                        this.setState({ showFailAlert: true, showEditModal: false, selectedTappa: null })
                    })
            }

        }

        const handleClose = () => {
            this.setState({ showEditModal: false, selectedTappa: null })

        }


        return <Modal show={this.state.showEditModal} size="lg" onHide={handleClose} >
            <Modal.Header closeButton>
                <Modal.Title className="h6">Modifica tappa</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <div className="container space-2">
                    <form id="uploadForm" className="js-validate svg-preloader" onSubmit={handleSubmit}>
                        {/* <!-- Listing Agent Information --> */}
                        <div className="mb-7">

                            <div className="row">
                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="tappaName">Nome</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="tappaNameLabel">
                                                        <span className="fas fa-file-signature"></span>
                                                    </span>
                                                </div>
                                                <input type="text" className="form-control" id="tappaName"
                                                    placeholder="Nome nuova tappa (Tipo sergio nzucchero)"
                                                    aria-label="Nome tappa" aria-describedby="tappaNameLabel"
                                                    defaultValue={selectedTappa.name} required
                                                    data-msg="Please enter a listing agent name."
                                                    data-error-className="u-has-error"
                                                    data-success-className="u-has-success" />
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>

                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="payment">Tipologia pagamento</label>
                                            <div className="input-group d-flex align-items-center">
                                                <select className="form-control" id="payment" defaultValue={selectedTappa.payment} required>
                                                    <option value="" hidden>Scegliere il pagamento</option>
                                                    <option value="anticipato">Anticipato</option>
                                                    <option value="fine mese">Fine mese</option>
                                                    <option value="guida">Guida</option>
                                                    <option value="nessun pagamento">Nessun pagamento</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="type">Categoria tappa</label>
                                            <div className="input-group d-flex align-items-center">
                                                <select className="form-control" id="type" defaultValue={selectedTappa.type} required>
                                                    <option value="" hidden>Scegliere la categoria</option>
                                                    <option value="enogastronomica">Enogastronomica</option>
                                                    <option value="archeologica">Archeologica</option>
                                                    <option value="transitoria">Transitoria</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="duration">Durata tappa</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="tappaDurationLabel">
                                                        <span className="fas fa-clock"></span>
                                                    </span>
                                                </div>
                                                <input type="number" className="form-control" id="duration"
                                                    placeholder="Durata della tappa comprensiva di spostamento"
                                                    aria-label="Durata tappa" aria-describedby="tappaDurationLabel"
                                                    defaultValue={selectedTappa.duration}
                                                    required
                                                    data-msg="Inserire durata tappa"
                                                    data-error-className="u-has-error"
                                                    data-success-className="u-has-success" />
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>

                        </div>
                        {/* <!-- End Listing Agent Information --> */}


                        {/* <!-- Upload Images --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Indirizzo e note</h2>
                            </div>
                            {/* <!-- End Title --> */}

                            <div className="row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <label className="form-label">
                                            Indirizzo
                                            <span className="text-danger">*</span>
                                        </label>

                                        <div className="input-group">
                                            <textarea className="form-control" rows="3" name="text" id="indirizzo"
                                                placeholder="Aggiungi indirizzo .. (Via/Piazza, numero civico, CAP)"
                                                defaultValue={selectedTappa.address}
                                                aria-label="Indirizzo" required
                                                data-msg="Please enter a property description."
                                                data-error-className="u-has-error"
                                                data-success-className="u-has-success"></textarea>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <label className="form-label">
                                            Note e varie
                                            <span className="text-danger">*</span>
                                        </label>

                                        <div className="input-group">
                                            <textarea className="form-control" rows="6" name="text" id="note"
                                                placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                                defaultValue={selectedTappa.note}
                                                data-msg="Please enter a property description."
                                                data-error-className="u-has-error"
                                                data-success-className="u-has-success"></textarea>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>
                        </div>
                        {/* <!-- End Upload Images --> */}

                        <div className="row">
                            <div className="col-lg-3 col-md-2 col-sm-1">
                                <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Salva</button>
                            </div>
                        </div>
                    </form>
                </div>
            </Modal.Body>
        </Modal>
    }



    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState({ showSuccessAlert: false });
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }


}

export default injectIntl(
    connect(
    )(Tappe)
);