/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { Button, Modal } from "react-bootstrap";
import { toAbsoluteUrl } from "../../utils/utils";
import * as auth from "../../store/ducks/auth.duck";
import { Formik } from "formik";
import { TextField } from "@material-ui/core";
import { updateUser } from "../../crud/user.crud";
import { changePassword } from "../../crud/auth.crud";

class UserProfile extends React.Component {


  constructor(props) {
    super(props);

    this.state = {
      showEditInfoModal: false,
      showChangePasswordModal: false
    };
  }

  componentDidMount() {
    console.log('REACt USER PROFILE');
  }

  componentWillUnmount() {

  }

  render() {
    return <div className="container fluid">
      {this.renderPage()}
      {this.renderEditModal()}
      {this.renderChangePasswordModal()}
    </div>
  }


  renderPage() {
    const { user } = this.props;

    if (user) {
      return (
        <main className="px-4">
          <div className="row">
            <div className="col-sm-4 col-md-4">
              <div className="card">
                <div className="card-header">
                  <div className="kt-widget4__pic kt-widget4__pic--pic ">
                    <img alt="img profile" className="rounded-circle" style={{ width: '80px', height: '80px' }} src={toAbsoluteUrl(user.profilePicture || "/media/users/100_1.jpg")} />
                  </div>
                </div>
                <div className="card-body">
                  <h5 className="card-title">{user.fullname}</h5>
                  <p className="card-text"></p>
                </div>
                <ul className="list-group list-group-flush">
                  <li className="list-group-item"><strong>username: </strong>{user.username}</li>
                  <li className="list-group-item"><strong>email: </strong>{user.email}</li>
                  <li className="list-group-item"><strong>role: </strong>{user.role ? user.role.toLowerCase() : "no role"}</li>
                </ul>
                <div className="card-body">
                  <div className="media">
                    <a onClick={(e) => {
                      e.preventDefault();
                      this.setState({ showEditInfoModal: true })
                    }} className="card-link kt-font-brand">Edit info</a>
                    <a onClick={(e) => {
                      e.preventDefault();
                      this.setState({ showChangePasswordModal: true })
                    }} className="card-link text-primary">Change Password</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>
      );
    }
    return "";
  }

  renderEditModal() {
    const handleClose = () => {
      this.setState({ showEditInfoModal: false });
    };

    const { intl, user } = this.props;

    return (

      <Modal show={this.state.showEditInfoModal} onHide={handleClose} >
        <Modal.Header closeButton>
          <Modal.Title className="h6">Edit {user.fullname}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-light">
          <Formik
            initialValues={{
              email: user.email,
              fullname: user.fullname,
              username: user.username,
            }}
            validate={values => {
              const errors = {};

              if (!values.email) {
                errors.email = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                });
              } else if (
                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              ) {
                errors.email = intl.formatMessage({
                  id: "AUTH.VALIDATION.INVALID_FIELD"
                });
              }

              if (!values.fullname) {
                errors.fullname = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                });
              }

              if (!values.username) {
                errors.username = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                });
              }

              return errors;
            }}
            onSubmit={(values, { setStatus, setSubmitting }) => {
              updateUser(
                user.id,
                values.email,
                values.fullname,
                values.username,
                values.role
              )
                .then((res) => {
                  const user = res.data;
                  this.props.editUser(user);
                  this.setState({ showEditInfoModal: false });
                })
                .catch(() => {
                  setSubmitting(false);
                  setStatus(
                    intl.formatMessage({
                      id: "AUTH.VALIDATION.INVALID_LOGIN"
                    })
                  );
                });
            }}
          >
            {({
              values,
              status,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting
            }) => (
                <form onSubmit={handleSubmit} noValidate autoComplete="off">
                  {status && (
                    <div role="alert" className="alert alert-danger">
                      <div className="alert-text">{status}</div>
                    </div>
                  )}

                  <div className="form-group mb-0">
                    <TextField
                      margin="normal"
                      label="Fullname"
                      className="kt-width-full"
                      name="fullname"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.fullname}
                      helperText={touched.fullname && errors.fullname}
                      error={Boolean(touched.fullname && errors.fullname)}
                    />
                  </div>

                  <div className="form-group mb-0">
                    <TextField
                      label="Email"
                      margin="normal"
                      className="kt-width-full"
                      name="email"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.email}
                      helperText={touched.email && errors.email}
                      error={Boolean(touched.email && errors.email)}
                    />
                  </div>

                  <div className="form-group mb-0">
                    <TextField
                      margin="normal"
                      label="Username"
                      className="kt-width-full"
                      name="username"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.username}
                      helperText={touched.username && errors.username}
                      error={Boolean(touched.username && errors.username)}
                    />
                  </div>

                  <div className="kt-login__actions mt-5">

                    <button
                      disabled={isSubmitting}
                      className="btn btn-primary btn-elevate kt-login__btn-brand"
                    >
                      Submit
                                     </button>
                  </div>
                </form>
              )}
          </Formik>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="link" onClick={handleClose}>
            Close
                    </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  renderChangePasswordModal() {
    const handleClose = () => {
      this.setState({ showChangePasswordModal: false });
    };

    const { intl, user } = this.props;

    return (

      <Modal show={this.state.showChangePasswordModal} onHide={handleClose} >
        <Modal.Header closeButton>
          <Modal.Title className="h6">Edit {user.fullname}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="bg-light">
          <Formik
            initialValues={{

            }}
            validate={values => {
              const errors = {};

              if (!values.password) {
                errors.password = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                });
              }

              if (!values.confirmPassword) {
                errors.confirmPassword = intl.formatMessage({
                  id: "AUTH.VALIDATION.REQUIRED_FIELD"
                });
              } else if (values.password !== values.confirmPassword) {
                errors.confirmPassword =
                  "Password and Confirm Password didn't match.";
              }

              return errors;
            }}
            onSubmit={(values, { setStatus, setSubmitting }) => {
              changePassword(
                values.oldPassword,
                values.password
              )
                .then((res) => {
                  this.setState({ showChangePasswordModal: false });
                })
                .catch(() => {
                  setSubmitting(false);
                  setStatus(
                    intl.formatMessage({
                      id: "AUTH.VALIDATION.INVALID_LOGIN"
                    })
                  );
                });
            }}
          >
            {({
              values,
              status,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              isSubmitting
            }) => (
                <form onSubmit={handleSubmit} noValidate autoComplete="off">
                  {status && (
                    <div role="alert" className="alert alert-danger">
                      <div className="alert-text">{status}</div>
                    </div>
                  )}

                  <div className="form-group mb-0">
                    <TextField
                      type="password"
                      margin="normal"
                      label="Old Password"
                      className="kt-width-full"
                      name="oldPassword"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.oldPassword}
                      helperText={touched.oldPassword && errors.oldPassword}
                      error={Boolean(touched.oldPassword && errors.oldPassword)}
                    />
                  </div>

                  <div className="form-group mb-0">
                  <TextField
                      type="password"
                      margin="normal"
                      label="Password"
                      className="kt-width-full"
                      name="password"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.password}
                      helperText={touched.password && errors.password}
                      error={Boolean(touched.password && errors.password)}
                    />
                  </div>

                  <div className="form-group mb-0">
                    <TextField
                      type="password"
                      margin="normal"
                      label="Confirm Password"
                      className="kt-width-full"
                      name="confirmPassword"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      value={values.confirmPassword}
                      helperText={touched.confirmPassword && errors.confirmPassword}
                      error={Boolean(
                        touched.confirmPassword && errors.confirmPassword
                      )}
                    />
                  </div>

                  <div className="kt-login__actions mt-5">

                    <button
                      disabled={isSubmitting}
                      className="btn btn-primary btn-elevate kt-login__btn-brand"
                    >
                      Submit
                                     </button>
                  </div>
                </form>
              )}
          </Formik>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="link" onClick={handleClose}>
            Close
                    </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

const mapStateToProps = ({ auth: { user } }) => ({
  user
});
export default injectIntl(connect(mapStateToProps, auth.actions)(UserProfile));