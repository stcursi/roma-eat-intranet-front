import React, { Component } from "react";
import { toAbsoluteUrl } from "../../utils/utils";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { savePaymentMethod, editPaymentMethod, deletePaymentMethod } from "../../crud/paymentMethod.crud";
import { getAllPaymentMethods } from "../../crud/paymentMethod.crud";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

class NewPaymentMethod extends Component {

  constructor(props) {
    super(props);

    this.myFormRef = React.createRef();

    this.state = {
      paymentMethods: []
    };
  }

  componentDidMount() {
    this.getAllPayments()
  }

  async getAllPayments() {
    await getAllPaymentMethods()
      .then((res) => {
        if (res && res.data) this.setState({ paymentMethods: res.data });
      })
      .catch((error) => {
        console.error("[NewPaymentMethod] - getAllPayments: ", error);
      });
  }

  componentWillUnmount() {

  }

  initializeNewPaymentMethod(data) {
    let toReturn = {};

    toReturn.type = data.paymentName ? data.paymentName.value : null;
    toReturn.note = data.note ? data.note.value : null;

    return toReturn;
  }

  render() {

    const saveNewPaymentMethod = (event) => {
      event.preventDefault();

      if (event.target) {
        const newPaymentMethod = this.initializeNewPaymentMethod(event.target);
        savePaymentMethod(newPaymentMethod)
          .then(() => {
            this.setState({ showSuccessAlert: true })
          })
          .catch(() => {
            this.setState({ showFailAlert: true })
          })
          .finally(() => {
            this.myFormRef.reset();
          })
      }

    }

    return (
      <div className="container-fluid">
        <div className="container space-2">
          <div className="w-lg-75 mx-lg-auto">
            {/* <!-- Title --> */}
            <div className="text-center mb-9">
              <h1 className="h2 kt-font-brand font-weight-medium">Crea una nuova metodo di pagamento</h1>
              <p>Inserisci i valori nei campi che seguono</p>
            </div>
            {/* <!-- End Title --> */}

            <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewPaymentMethod} ref={(el) => this.myFormRef = el}>
              {/* <!-- Listing Agent Information --> */}
              <div className="mb-7">
                {/* <!-- Title --> */}
                <div className="border-bottom pb-3 mb-5">
                  <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                </div>
                {/* <!-- End Title --> */}

                <div className="row">
                  <div className="col-md-6 mb-3">
                    {/* <!-- Input --> */}
                    <div className="form-group">
                      <div className="js-form-message js-focus-state">
                        <label className="form-label" htmlFor="paymentName">Nome Metodo di pagamento</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text" id="paymentNameLabel">
                              <span className="fas fa-file-signature"></span>
                            </span>
                          </div>
                          <input type="text" className="form-control" id="paymentName" placeholder="Nome nuovo metodo di pagamento (Pay pal es.)" aria-label="Nome metodo pagamento" aria-describedby="paymentNameLabel" required
                            data-msg="Please enter a listing agent name."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success" />
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Input --> */}
                  </div>

                </div>
                {/* <!-- End Listing Agent Information --> */}


                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Note</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">

                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="js-form-message mb-3">
                        <label className="form-label">
                          Note
                    <span className="text-danger">*</span>
                        </label>

                        <div className="input-group">
                          <textarea className="form-control" rows="6" name="note" id="note" placeholder="Note e varie" aria-label="Note e varie"
                            data-msg="Please enter a property description."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success"></textarea>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- End Upload Images --> */}

              <div className="row">
                <div className="col-lg-3 col-md-2 col-sm-1">
                  <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Submit</button>
                </div>
              </div>
            </form>
          </div>
          {this.renderTable()}
        </div>
        {/* <!-- End Upload Form Section --> */}
        {this.renderSuccessModal()}
        {this.renderFailModal()}
      </div>
    )
  }

  renderTable() {

    const { paymentMethods } = this.state;

    const columns = [{
      dataField: 'type',
      editable: true,
      text: 'Nome',
      sort: true,
    }, {
      dataField: 'note',
      editable: true,
      text: 'Note',
      sort: true,
    }, {
      dataField: 'delete',
      editable: false,
      text: 'Emilina',
    }
    ];

    const handleDeletePayment = (id) => {

      deletePaymentMethod(id)
        .then(() => {
          this.getAllPayments()
            .then(() => this.setState({ showSuccessAlert: true }));

        })
        .catch((error) => {
          console.log('Error in delete payment method: ', error);
          this.setState({ showFailAlert: true });
        });
    }

    const handleEditPayment = (id, param, value) => {

      let params = {};
      params[param] = value;

      editPaymentMethod(id, params)
        .then(() => {
          this.getAllPayments()
            .then(() => this.setState({ showSuccessAlert: true }));

        })
        .catch((error) => {
          console.log('Error in edit payment method: ', error);
          this.setState({ showFailAlert: true });
        });

    }

    const cellEdit = cellEditFactory({
      mode: 'click',
      afterSaveCell: (oldValue, newValue, row, column) => {

        handleEditPayment(row.id, column.dataField, newValue)

      },
    });

    const renderRows = (paymentMethods) => {

      let toReturn = [];

      for (let paymentMethod of paymentMethods) {
        let newPaymentMethod = Object.assign({}, paymentMethod);
        newPaymentMethod.type = paymentMethod.type ? paymentMethod.type : "Sconosciuto";
        newPaymentMethod.note = paymentMethod.note ? paymentMethod.note : "Sconosciuto";
        newPaymentMethod.delete = <div className="text-center">
          <button className="btn btn-xs btn-danger" onClick={() => handleDeletePayment(paymentMethod.id)}>Elimina</button>
        </div>

        toReturn.push(newPaymentMethod);
      }

      return toReturn;
    }


    return (
      <div className="row my-5">
        <div className="col-lg-12">
          <h5>Metodi di pagamento </h5>
          <BootstrapTable striped
            keyField="id"
            data={renderRows(paymentMethods)}
            columns={columns}
            cellEdit={cellEdit}
          />
        </div>
      </div>
    )
  }


  renderSuccessModal() {
    const { showSuccessAlert } = this.state;

    const onCloseSuccessModal = () => {
      this.setState({ showSuccessAlert: false });
    };

    return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
  }

  renderFailModal() {
    const { showFailAlert } = this.state;

    const onCloseFailModal = () => {
      this.setState({ showFailAlert: false })
    };

    return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
  }


}

export default injectIntl(
  connect(
  )(NewPaymentMethod)
);