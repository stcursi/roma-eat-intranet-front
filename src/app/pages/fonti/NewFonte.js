import React, { Component } from "react";
import { toAbsoluteUrl } from "../../utils/utils";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { saveFonte, getAllFonti, deleteFonte, editFonte } from "../../crud/fonte.crud";

import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

class NewFonte extends Component {

  constructor(props) {
    super(props);

    this.myFormRef = React.createRef();

    this.state = {
      fonti: [],
      showSuccessAlert: false,
      showFailAlert: false
    };
  }

  componentDidMount() {
    this.getFonti()
  }

  async getFonti() {
    await getAllFonti()
      .then((res) => {
        if (res && res.data) this.setState({ fonti: res.data });
      })
      .catch((error) => {
        console.error("[NewFonte] - getAllFonti: ", error);
      });
  }

  componentWillUnmount() {

  }

  initializeNewFonte(data) {
    let toReturn = {};

    toReturn.name = data.fonteName ? data.fonteName.value : null;
    toReturn.note = data.note ? data.note.value : null;

    return toReturn;
  }

  render() {

    const saveNewFonte = (event) => {
      event.preventDefault();

      if (event.target) {
        const newEmail = this.initializeNewFonte(event.target);
        saveFonte(newEmail)
          .then(() => {
            this.setState({ showSuccessAlert: true })
          })
          .catch(() => {
            this.setState({ showFailAlert: true })
          })
          .finally(() => {
            this.myFormRef.reset();
          })
      }

    }

    return (
      <div className="container-fluid">
        <div className="container space-2">
          <div className="w-lg-75 mx-lg-auto">
            {/* <!-- Title --> */}
            <div className="text-center mb-9">
              <h1 className="h2 kt-font-brand font-weight-medium">Crea una nuova piattaforma di prenotazione</h1>
              <p>Inserisci i valori nei campi che seguono</p>
            </div>
            {/* <!-- End Title --> */}

            <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewFonte} ref={(el) => this.myFormRef = el}>
              {/* <!-- Listing Agent Information --> */}
              <div className="mb-7">
                {/* <!-- Title --> */}
                <div className="border-bottom pb-3 mb-5">
                  <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                </div>
                {/* <!-- End Title --> */}

                <div className="row">
                  <div className="col-md-6 mb-3">
                    {/* <!-- Input --> */}
                    <div className="form-group">
                      <div className="js-form-message js-focus-state">
                        <label className="form-label" htmlFor="fonteName">Nome Piattaforma</label>
                        <div className="input-group">
                          <div className="input-group-prepend">
                            <span className="input-group-text" id="fonteNameLabel">
                              <span className="fas fa-file-signature"></span>
                            </span>
                          </div>
                          <input type="text" className="form-control" id="fonteName"
                            placeholder="Nome nuova piattaforma (Airbnb per es.)" aria-label="Nome piattaforma"
                            aria-describedby="fonteNameLabel" required
                            data-msg="Please enter a listing agent name."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success" />
                        </div>
                      </div>
                    </div>
                    {/* <!-- End Input --> */}
                  </div>

                </div>
                {/* <!-- End Listing Agent Information --> */}


                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Note</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">

                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="js-form-message mb-3">
                        <label className="form-label">
                          Note
                    <span className="text-danger">*</span>
                        </label>

                        <div className="input-group">
                          <textarea className="form-control" rows="6" name="note" id="note" placeholder="Note e varie" aria-label="Note e varie"
                            data-msg="Please enter a property description."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success"></textarea>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>
                </div>
              </div>
              {/* <!-- End Upload Images --> */}

              <div className="row">
                <div className="col-lg-3 col-md-2 col-sm-1">
                  <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Submit</button>
                </div>
              </div>
            </form>
          </div>
          {this.renderTable()}
        </div>
        {this.renderSuccessModal()}
        {this.renderFailModal()}
      </div>
    )
  }

  renderTable() {

    const { fonti } = this.state;

    const columns = [{
      dataField: 'type',
      editable: true,
      text: 'Nome',
      sort: true,
    }, {
      dataField: 'note',
      editable: true,
      text: 'Note',
      sort: true,
    }, {
      dataField: 'delete',
      editable: false,
      text: 'Emilina',
    }
    ];

    const handleDeleteFonte = (id) => {

      deleteFonte(id)
        .then(() => {
          this.getFonti()
            .then(() => this.setState({ showSuccessAlert: true }));
        })
        .catch((error) => {
          console.log('Error in delete fonte: ', error);
          this.setState({ showFailAlert: true });
        });
    }

    const handleEditFonte = (id, param, value) => {

      let params = {};
      params[param] = value;

      editFonte(id, params)
        .then(() => {
          this.getFonti()
            .then(() => this.setState({ showSuccessAlert: true }));
        });

    }

    const cellEdit = cellEditFactory({
      mode: 'click',
      afterSaveCell: (oldValue, newValue, row, column) => {

        handleEditFonte(row.id, column.dataField, newValue)

      },
    });

    const renderRows = (fonti) => {

      let toReturn = [];

      for (let fonte of fonti) {
        let newfonte = Object.assign({}, fonte);
        newfonte.type = fonte.type ? fonte.type : "Sconosciuto";
        newfonte.note = fonte.note ? fonte.note : "Sconosciuto";
        newfonte.delete = <div className="text-center">
          <button className="btn btn-xs btn-danger" onClick={() => handleDeleteFonte(fonte.id)}>Elimina</button>
        </div>

        toReturn.push(newfonte);
      }

      return toReturn;
    }


    return (
      <div className="row my-5">
        <div className="col-lg-12">
          <h5>Piattaforme di prenotazione </h5>
          <BootstrapTable striped
            keyField="id"
            data={renderRows(fonti)}
            columns={columns}
            cellEdit={cellEdit}
          />
        </div>
      </div>
    )
  }



  renderSuccessModal() {
    const { showSuccessAlert } = this.state;

    const onCloseSuccessModal = () => {
      this.setState({ showSuccessAlert: false });
    };

    return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
  }

  renderFailModal() {
    const { showFailAlert } = this.state;

    const onCloseFailModal = () => {
      this.setState({ showFailAlert: false })
    };

    return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
  }


}

export default injectIntl(
  connect(
  )(NewFonte)
);