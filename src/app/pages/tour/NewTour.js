import React, { Component } from "react";
import { getAllExperiences, getExperience } from "../../crud/experience.crud";
import { getAllFonti } from "../../crud/fonte.crud";
import { getAllByRole } from "../../crud/user.crud";
import { getAllTappe } from "../../crud/tappa.crud";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { saveTour } from "../../crud/tour.crud";
import { formatPickerDate } from "../../utils/date";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {
    Grid,
} from "@material-ui/core";
import TappeSelector from "../tappa/TappeSelector";
import DateFnsUtils from "@date-io/date-fns";
import "moment/locale/it";
import 'react-widgets/dist/css/react-widgets.css';
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";

var moment = require('moment');

const initialState = {
    experiences: [],
    allGuides: [],
    allAllergies: [],
    openDay: moment().format(),
    startDay: null,
    fonti: [],
    tappe: [],
    selectedTappe: [],
    orderTappe: {},
    selectedExperience: null,
    startTime: null,
    endTime: null,
    showAddPartecipant: false,
    immediateOpen: true,
    tourPrivato: false,
    tabValue: 1,
    fromDay: moment().format(),
    tillDay: moment().format(),
    showSuccessAlert: false,
    showFailAlert: false
}

class NewTour extends Component {

    constructor(props) {
        super(props);

        this.myFormRef = React.createRef();

        this.state = initialState;
    }

    componentDidMount() {

        // WITH HISTORY PUSH
        const dateTourFromCalendar = this.props.location && this.props.location.state || null;

        if (dateTourFromCalendar) {
            this.setState({ startDay: dateTourFromCalendar })
        }

        getAllExperiences()
            .then((res) => {
                if (res && res.data) {
                    this.setState({
                        experiences: res.data
                    });
                }
            })
            .catch((error) => {
                console.error("[NewTour] - getAllExperiences: ", error);
            })

        getAllByRole('GUIDE')
            .then((res) => {
                this.setState({ allGuides: res.data });
            })
            .catch((error) => {
                console.error('[getAllByRole-GUIDE] - ', error);
            })

        getAllFonti()
            .then((res) => {
                if (res && res.data) this.setState({ fonti: res.data });
            })
            .catch((error) => {
                console.error("[NewExperience] - getAllFonti: ", error);
            })

        getAllTappe()
            .then((res) => {
                if (res && res.data) this.setState({ tappe: res.data });
            })
            .catch((error) => {
                console.error("[NewExperience] - getAllTappe: ", error);
            })

    }

    componentWillUnmount() {

    }

    setWeekday(weekdays, data) {
        let i = 1;
        while (i <= 7) {
            if (data["weekday_" + i] && data["weekday_" + i].checked) {
                weekdays.push(i);
            }
            i++;
        }
    }

    initializeNewTour(data) {
        let toReturn = {};

        const { startTime, endTime, openDay, startDay, immediateOpen, tourPrivato, fromDay, tillDay, selectedTappe } = this.state;

        toReturn.experience = data.experience ? data.experience.value : null;
        toReturn.meetingPoint = data.meetingPoint ? data.meetingPoint.value : null;
        let tappeToJoin = [];

        let orderTappeToSave = {};

        if (selectedTappe) {
            for (let selTappa of selectedTappe) {
                if (data["tappa_" + selTappa] && data["tappa_" + selTappa].value) {
                    orderTappeToSave[data["tappa_" + selTappa].value] = selTappa;
                }
            }

            let i = 1;

            while (i <= selectedTappe.length) {
                tappeToJoin.push(orderTappeToSave[i])
                i++;
            }
        }

        toReturn.orderTappe = tappeToJoin.join(',');

        toReturn.fonti = [];

        if (data.fonti && data.fonti.selectedOptions) {
            for (let opt of data.fonti.selectedOptions) {
                toReturn.fonti.push(opt.value);
            }
        }

        toReturn.guide = data.guide && data.guide.value !== "noguide" ? data.guide.value : null;
        toReturn.secondGuide = data.secondGuide && data.secondGuide.value !== "noguide" ? data.secondGuide.value : null;
        toReturn.note = data.note ? data.note.value : null;

        toReturn.open = immediateOpen;
        toReturn.privato = tourPrivato;
        if (toReturn.open) {
            toReturn.openDay = moment().startOf('day');
        } else {
            toReturn.openDay = openDay ? moment(openDay).startOf('day') : null;
        }

        toReturn.weekdays = [];
        this.setWeekday(toReturn.weekdays, data);

        if (toReturn.weekdays.length === 0) {
            toReturn.date = startDay ? moment(startDay).format() : null;
            let tourDayMomentForStart = startDay ? moment(startDay) : null;
            let tourDayMomentForEnd = startDay ? moment(startDay) : null;
            let startTimeMoment = startTime ? moment(startTime, 'HH:mm') : null;
            let endTimeMoment = endTime ? moment(endTime, 'HH:mm') : null;

            if (startTimeMoment && tourDayMomentForStart) {
                toReturn.startTime = tourDayMomentForStart.set({
                    hour: startTimeMoment.get('hour'),
                    minute: startTimeMoment.get('minute')
                })
            }

            if (endTimeMoment && tourDayMomentForEnd) {
                toReturn.endTime = tourDayMomentForEnd.set({
                    hour: endTimeMoment.get('hour'),
                    minute: endTimeMoment.get('minute')
                })
            }

        } else {
            toReturn.fromDay = fromDay ? moment(fromDay).format() : null;
            toReturn.tillDay = tillDay ? moment(tillDay).format() : null;
            toReturn.startTime = startTime ? moment(startTime, 'HH:mm') : null;
            toReturn.endTime = endTime ? moment(endTime, 'HH:mm') : null;
        }


        return toReturn;
    }

    render() {

        let { experiences, savedTour } = this.state;

        // savedTour = 117;
        if (savedTour) {
            return <div className="container-fluid">
                <main id="content" role="main">
                    {/* <!-- Upload Form Section --> */}
                    <div className="container space-2">
                        <div className="w-lg-75 mx-lg-auto">
                            {/* <!-- Title --> */}
                            <h3 className="h2 text-center kt-font-brand font-weight-medium mb-3">Tour salvato! Vuoi aggiungere partecipanti?</h3>
                            <div className="row justify-content-center">
                                <a className="btn btn-brand btn-elevate mr-1" href={"/partecipant/add/" + savedTour.id}>Aggiungi partecipanti</a>
                                <button className="btn btn-sm btn-info ml-1" onClick={() => this.setState({ savedTour: null })}>Crea nuovo tour</button>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        }

        const saveNewTour = (event) => {
            event.preventDefault();

            if (event.target) {

                const newTour = this.initializeNewTour(event.target);
                saveTour(newTour)
                    .then((res) => {
                        this.myFormRef.reset();
                        this.setState({ showSuccessAlert: true, selectedTappe: [], orderTappe: {}, savedTour: res.data || null })
                    })
                    .catch((error) => {
                        this.setState({ showFailAlert: true })
                    })
            }

        }

        const retrieveExpInfo = (event) => {
            event.preventDefault();

            this.setState({ selectedTappe: [], orderTappe: {} });

            if (event.target && event.target.value) {
                getExperience(event.target.value)
                    .then((res) => {
                        if (res && res.data) {
                            let preparedTappe = [];
                            let preparedOrderTappe = {};
                            if (res.data.tappe) {
                                for (let t of res.data.tappe) {
                                    preparedTappe.push(t.id)
                                }
                            }

                            if (res.data.orderTappe && res.data.orderTappe.indexOf(',') !== -1) {
                                let splitted = res.data.orderTappe.split(',');
                                for (let i = 0; i <= splitted.length - 1; i++) {
                                    preparedOrderTappe[i + 1] = splitted[i];
                                }
                            }

                            this.setState({
                                selectedExperience: res.data,
                                selectedTappe: preparedTappe,
                                orderTappe: preparedOrderTappe,
                                startTime: formatPickerDate(res.data.startTime),
                                endTime: formatPickerDate(res.data.endTime)
                            });
                        } else {
                            console.error('error no data in response')
                        }
                    })
                    .catch((error) => console.error(error));
            }
        }

        return (
            <div className="container-fluid">
                <main id="content" role="main">
                    {/* <!-- Upload Form Section --> */}
                    <div className="container space-2">
                        <div className="w-lg-75 mx-lg-auto">
                            {/* <!-- Title --> */}
                            <div className="text-center mb-9">
                                <h1 className="h2 kt-font-brand font-weight-medium">Apri un nuovo tour</h1>
                                <p>Recupera i valori chiave dal tipo d'Experience</p>
                            </div>
                            {/* <!-- End Title --> */}

                            <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewTour} ref={(el) => this.myFormRef = el}>
                                {/* <!-- Listing Agent Information --> */}
                                <div className="mb-7">
                                    {/* <!-- Title --> */}
                                    <div className="border-bottom pb-3 mb-5">
                                        <h2 className="h6 kt-font-brand mb-0">Categoria</h2>
                                    </div>
                                    {/* <!-- End Title --> */}

                                    <div className="row">
                                        <div className="col-lg-6 mb-3">
                                            {/* <!-- Input --> */}
                                            <div className="form-group">
                                                <div className="js-focus-state">
                                                    <label className="form-label" htmlFor="experience">
                                                        <span className="fas fa-map-marker-alt mr-2"></span>Experiences</label>
                                                    <div className="input-group">
                                                        <select className="custom-select" onChange={retrieveExpInfo} id="experience" placeholder="Scegliere la tipologia di Experience" aria-describedby="experienceLabel">
                                                            <option hidden>Scegliere la tipologia di Experience</option>
                                                            {experiences ? experiences.map(exp => <option key={exp.id} value={exp.id}>{exp.name}</option>) : <option>caricamento experiences</option>}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            {/* <!-- End Input --> */}
                                        </div>

                                    </div>

                                </div>
                                {/* <!-- End Listing Agent Information --> */}
                                {this.renderDataLoaded()}
                                {this.renderSuccessModal()}
                                {this.renderFailModal()}
                            </form>
                        </div>
                    </div>
                    {/* <!-- End Upload Form Section --> */}
                </main>
            </div>
        )
    }

    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState(initialState);
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }


    renderDataLoaded() {

        const {
            selectedExperience,
            allGuides,
            tourPrivato,
            tappe,
            fonti,
            selectedTappe,
            orderTappe
        } = this.state;

        if (!selectedExperience) return "";

        let firstTappaId = selectedExperience.orderTappe && selectedExperience.orderTappe.split(',')[0];
        let firstTappa = null;

        if (selectedExperience.tappe) {
            selectedExperience.tappe.forEach((t) => {
                if (t.id.toString() === firstTappaId) {
                    firstTappa = t;
                }
            })
        }

        const updateTappe = (event) => {
            this.setState({ selectedTappe: event.target.value })
        }

        return (
            <div>
                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                        <h2 className="h6 kt-font-brand mb-0">Informazioni Experience: {selectedExperience.name}</h2>
                    </div>
                    {/* <!-- End Title --> */}

                    {this.renderTimePickers()}
                </div>

                <div className="row">
                    <div className="col-md-6 mb-3">
                        {/* <!-- Input --> */}
                        <div className="form-group">
                            <div className="js-form-message js-focus-state">
                                <label className="form-label" htmlFor="meetingPoint">Meeting Point</label>
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text" id="meetingPointLabel">
                                            <span className="fas fa-file-signature"></span>
                                        </span>
                                    </div>
                                    <input type="text" className="form-control" name="meetingPoint" id="meetingPoint"
                                        defaultValue={firstTappa ? firstTappa.name : ""}
                                        placeholder="Meeting Point"
                                        aria-label="Meeting Point"
                                        aria-describedby="meetingPointLabel" required
                                        data-msg="Please enter a Meeting point."
                                        data-error-class="u-has-error"
                                        data-success-class="u-has-success" />
                                </div>
                            </div>
                        </div>
                        {/* <!-- End Input --> */}
                    </div>
                </div>

                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                        <h2 className="h6 kt-font-brand mb-0">Data di apertura e Guida</h2>
                    </div>
                    {/* <!-- End Title --> */}
                    <div className="row">
                        <div className="col-lg-6 mb-3">
                            {this.renderOpenDayPicker()}
                        </div>

                        <div className="col-lg-6 mb-3">
                            {/* <!-- Input --> */}
                            <div className="form-group">
                                <div className="js-focus-state">
                                    <label className="form-label" htmlFor="guide">Guida</label>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="guideLabel">
                                                <span className="fas fa-envelope"></span>
                                            </span>
                                        </div>
                                        <select className="custom-select" id="guide" placeholder="Scegliere guida" aria-describedby="guideLabel">
                                            <option selected value="noguide">Hai già una guida da assegnare?</option>
                                            {allGuides ? allGuides.map(guide => <option key={"guida_" + guide.id} value={guide.id}>{guide.fullname}</option>) : <option>caricamento guide</option>}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="form-group">
                                <div className="js-focus-state">
                                    <label className="form-label" htmlFor="secondGuide">Seconda Guida</label>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="secondGuideLabel">
                                                <span className="fas fa-envelope"></span>
                                            </span>
                                        </div>
                                        <select className="custom-select" id="secondGuide" placeholder="Scegliere seconda guida" aria-describedby="secongGuideLabel">
                                            <option selected value="noguide">Hai una seconda guida da assegnare?</option>
                                            {allGuides ? allGuides.map(guide => <option key={"secguida_" + guide.id} value={guide.id}>{guide.fullname}</option>) : <option>caricamento guide</option>}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- End Input --> */}
                        </div>
                    </div>
                </div>

                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-2">
                        <h2 className="h6 kt-font-brand mb-0">Giorno del Tour</h2>
                    </div>
                    <div className="row">
                        {this.renderTabs()}
                    </div>
                </div>

                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                        <h2 className="h6 kt-font-brand mb-0">Partecipanti</h2>
                    </div>
                    {/* <!-- End Title --> */}

                    <div className="row">
                        <div className="col-lg-12">
                            <div className="form-group ml-3">
                                <div className="js-focus-state">
                                    <label className="form-label" htmlFor="privatTourCheck"><span className="fas fa-lock"></span> Tour privato </label>
                                    <div className="input-group">
                                        <div className="form-check">
                                            <input className="form-check-input" defaultChecked={tourPrivato} onChange={(e) => this.setState({ tourPrivato: e.target.checked })} type="checkbox" value="" id="privatTourCheck" />
                                            <label className="form-check-label text-muted" htmlFor="privatTourCheck">
                                                Indicare se il tuor è per composto da gruppi di conoscenti.
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div >

                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                        <h2 className="h6 kt-font-brand mb-0">Struttura</h2>
                    </div>
                    {/* <!-- End Title --> */}

                    {tappe.length ? selectedTappe.length && <TappeSelector tappe={tappe}
                        orderTappe={orderTappe}
                        selectedTappe={selectedTappe}
                        updateTappe={updateTappe} /> : ""}

                </div>

                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                        <h2 className="h6 kt-font-brand mb-0">Piattaforme di prenotazione</h2>
                    </div>
                    {/* <!-- End Title --> */}

                    <div className="row">
                        <div className="col-lg-6 mb-3">
                            {/* <!-- Input --> */}
                            <div className="form-group">
                                <div className="js-focus-state">
                                    <label className="form-label" htmlFor="fonti">
                                        <span className="fas fa-map-marker-alt mr-2"></span> Aprire su</label>
                                    <div className="input-group">

                                        <select className="custom-select" multiple searchable="Search here.." id="fonti" aria-describedby="fontiLabel">
                                            {fonti ? fonti.map(fonte => <option value={fonte.id}>{fonte.type}</option>) : <option>caricamento fonti</option>}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- End Input --> */}
                        </div>

                    </div>
                </div>
                {/* <!-- End Upload Images --> */}

                <div className="mb-7">

                    <div className="row">
                        <div className="col-lg-7 mt-3">
                            <div className="form-group">
                                {/* <!-- Input --> */}
                                <div className="js-form-message mb-3">
                                    <label className="form-label"> Note ed eventuali
                                <span className="text-danger">*</span>
                                    </label>

                                    <div className="input-group">
                                        <textarea className="form-control" rows="6" name="text" id="note" placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                            data-msg="Please enter a property description."
                                            data-error-class="u-has-error"
                                            data-success-class="u-has-success"></textarea>
                                    </div>
                                </div>
                            </div>
                            {/* <!-- End Input --> */}
                        </div>


                    </div>
                </div >

                <div className="row">
                    <div className="col-lg-3 col-md-2 col-sm-1">
                        <button type="submit" className="btn btn-brand btn-block transition-3d-hover">Submit</button>
                    </div>
                </div>
            </div >
        )
    }


    renderTabs() {

        let { tabValue } = this.state;

        let classes = {
            header1: "nav-link",
            header2: "nav-link",
            tab1: "tab-pane p-4 fade show bg-trasparent",
            tab2: "tab-pane p-4 fade show bg-trasparent"
        }

        classes["header" + tabValue] = classes["header" + tabValue] += " active";
        classes["tab" + tabValue] = classes["tab" + tabValue] += " active";

        const handleChange = (event) => {
            if (event && event.target) {
                this.setState({ tabValue: event.target.id })
            }
        }

        return (
            <div className="container mb-5">
                <ul className="nav nav-tabs bg-trasparent" id="myTab" role="tablist">
                    <li className="nav-item"><a className={classes.header1} id="1" onClick={handleChange}>Data singola</a></li>
                    <li className="nav-item"><a className={classes.header2} id="2" onClick={handleChange}>Data Multipla</a></li>
                </ul>
                <div className="tab-content" id="myTabContent">
                    <div className={classes.tab1} id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                        <div className="row">
                            <div className="col-lg-6 mb-3">
                                {this.renderStartTourDayPicker()}
                            </div>
                        </div>
                    </div>
                    <div className={classes.tab2} id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                        <div className="row">
                            <div className="btn-group col-lg-12" id="weekdayGroup" data-toggle="buttons">
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_1" id="weekday_1" value="Lunedì" /><span className="mx-1">Lunedì</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_2" id="weekday_2" value="Martedì" /><span className="mx-1">Martedì</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_3" id="weekday_3" value="Mercoledì" /><span className="mx-1">Mercoledì</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_4" id="weekday_4" value="Giovedì" /><span className="mx-1">Giovedì</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_5" id="weekday_5" value="Venerdì" /><span className="mx-1">Venerdì</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_6" id="weekday_6" value="Sabato" /><span className="mx-1">Sabato</span></div>
                                <div className="d-flex align-items-center"><input type="checkbox" name="weekday_7" id="weekday_7" value="Domenica" /><span className="mx-1">Domenica</span></div>
                            </div>
                        </div>
                        <div className="row my-3">
                            {this.renderFromTillPickers()}
                        </div>
                    </div>
                </div>
            </div>
        )


    }

    renderFromTillPickers() {
        const { fromDay, tillDay } = this.state;

        const handleFromDayChange = (day) => {
            this.setState({ fromDay: day, tillDay: day });
        };

        const handleTillDayChange = (day) => {
            this.setState({ tillDay: day });
        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" htmlFor="fromTimePicker"><span className="fas fa-clock"></span> Dal </label>
                        <div className="input-group">
                            <KeyboardDatePicker
                                disableToolbar
                                variant="dialog"
                                format="dd/MM/yyyy"
                                margin="normal"
                                id="from-picker-dialog"
                                label="Date picker"
                                value={fromDay}
                                onChange={handleFromDayChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" htmlFor="tillTimePicker"><span className="fas fa-clock"></span> Al </label>
                        <div className="input-group">
                            <KeyboardDatePicker
                                disableToolbar
                                variant="dialog"
                                format="dd/MM/yyyy"
                                margin="normal"
                                id="till-picker-dialog"
                                label="Date picker"
                                value={tillDay}
                                onChange={handleTillDayChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

    renderStartTourDayPicker() {
        const { startDay } = this.state;

        const handleStartDayChange = (day) => {
            console.log('DAY', day);
            this.setState({ startDay: day });
        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={10} item>
                    <label className="form-label" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Giorno del Tour </label>
                    <div className="input-group">
                        <KeyboardDatePicker
                            disableToolbar
                            variant="dialog"
                            format="dd/MM/yyyy"
                            margin="normal"
                            id="start-day-picker-dialog"
                            label="Date picker"
                            value={startDay}
                            onChange={handleStartDayChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

    renderOpenDayPicker() {
        const { openDay, immediateOpen } = this.state;

        const handleOpenDayChange = (day) => {
            this.setState({ openDay: day });
        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={10} item>
                    <label className="form-label mt-1" htmlFor="immediateCheck"><span className="fas fa-bolt"></span> Apertura Immediata </label>
                    <div className="input-group">
                        <div className="form-check">
                            <input className="form-check-input" defaultChecked={immediateOpen} onChange={(e) => this.setState({ immediateOpen: e.target.checked })} type="checkbox" value="" id="immediateCheck" />
                            <label className="form-check-label text-muted" htmlFor="immediateCheck">
                                Aprire subito dopo il salvataggio
                             </label>
                        </div>
                    </div>
                </Grid>
                <Grid lg={10} item>
                    <label className="form-label mt-5" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Giorno d'Apertura </label>
                    <div className="input-group">
                        <KeyboardDatePicker
                            disableToolbar
                            disabled={this.state.immediateOpen}
                            variant="dialog"
                            format="dd/MM/yyyy"
                            margin="normal"
                            id="date-picker-dialog"
                            label="Date picker"
                            value={openDay}
                            onChange={handleOpenDayChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </div>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

    renderTimePickers() {

        const { startTime, endTime } = this.state;

        const handleSTDateChange = (date) => {
            this.setState({ startTime: date });
        };

        const handleETDateChange = (date) => {
            this.setState({ endTime: date });
        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="startTimePicker"
                                ampm={false}
                                value={startTime}
                                onChange={handleSTDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>

                    </Grid>
                </Grid>
                <Grid lg={6}>
                    <Grid lg={10} className="ml-1" item>
                        <label className="form-label" htmlFor="endTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="endTimePicker"
                                ampm={false}
                                value={endTime}
                                onChange={handleETDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

}



export default injectIntl(
    connect(
    )(NewTour)
);