import React, { Component } from "react";
import { connect } from "react-redux";
import TourModal from "./TourModal";
import CustomRangePicker from "../../partials/content/CustomRangePicker";
import { injectIntl } from "react-intl";
import { getToursForActions, assignTours, deleteBulkTours, editBulkTours, editBulkTimeTours } from "../../crud/tour.crud";
import { getAllByRole } from "../../crud/user.crud";
import { getAllExperiences } from "../../crud/experience.crud";
import { Button, Modal } from "react-bootstrap";
import {
    Grid,
    Typography
} from "@material-ui/core";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker
} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";

import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import { formatDate } from "../../utils/date";
import Select from 'react-select';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import './Tours.css';

import moment from 'moment';


const { SearchBar, ClearSearchButton } = Search;

const reactSelectStyles = {
    container: (base, state) => ({
        ...base,
        opacity: state.isDisabled ? ".5" : "1",
        backgroundColor: "transparent",
        margin: "0 10px 10px 0",
        width: "40%",
        zIndex: "999",
    })
};

class ToursPage extends Component {

    constructor(props) {
        super(props);

        this.closeModal = this.closeModal.bind(this);
        this.updateTour = this.updateTour.bind(this);


        this.state = {
            tours: [],
            experiences: [],
            selectedEvent: null,
            showTourModal: null,
            allGuides: [],
            filters: {},
            selectedGuide: null,
            rowSelected: [],
            showRangeCalendarModal: false,
            from: null,
            to: null,
            editStartTime: moment().hour(9).minute(0),
            editEndTime: moment().hour(9).minute(0),
            showSuccessAlert: false,
            showFailAlert: false
        };
    }

    componentDidMount() {


        const dataFrom = moment().startOf('day').toISOString();
        const dataTo = moment().add(3, 'months').startOf('day').toISOString();

        const params = { from: dataFrom, to: dataTo };


        this.getToursByFilter(params);

        getAllExperiences()
            .then((res) => {
                if (res && res.data) {
                    this.setState({
                        experiences: res.data
                    });
                }
            })
            .catch((error) => {
                console.error("[NewTour] - getAllExperiences: ", error);
            })

        getAllByRole('GUIDE')
            .then((res) => {
                this.setState({ allGuides: res.data });
            })
            .catch((error) => {
                console.error('[getAllByRole-GUIDE] - ', error);
            })
    }

    getToursByFilter(params) {

        getToursForActions(params)
            .then((res) => {
                if (res && res.data) this.setState({ tours: res.data, filters: params || {} });
            })
            .catch((error) => {
                console.error("[Tours] - getToursForActions: ", error);
            })

    }

    componentWillUnmount() {


    }

    closeModal() {
        const { tours } = this.state;
        this.setState({ showTourModal: false, tours: tours, rowSelected: [] })
    }

    updateAllTour = () => {

        getToursForActions(this.state.filters)
            .then((res) => {
                if (res && res.data) {
                    for (let tour of res.data) {
                        if (tour.id === this.state.selectedEvent.id) {
                            this.setState({ tours: res.data, selectedEvent: tour });
                            break;
                        }
                    }
                };
            })
            .catch((error) => {
                console.error("[UpdateAllTour] - getToursForActions: ", error);
            })
    }


    updateTour = (data) => {
        let currentTour = this.state.selectedEvent;
        currentTour = data;

        let tours = [...this.state.tours];

        let idx = 0;

        for (let i = 0; i < tours.length - 1; i++) {
            if (tours[i].id.toString() === data.id.toString()) {

                idx = i;

                break;
            }
        }

        tours.splice(idx, 1);

        let newTour = Object.assign({}, data);

        tours.push(newTour);

        this.setState({ tours: tours, selectedEvent: currentTour })
    }

    prepareExperienceFilter(experiences, selectedFilters) {
        let toReturn = [];
        for (let exp of experiences) {
            if (selectedFilters) {
                let arrayOfId = selectedFilters.split(',');
                for (let selected of arrayOfId) {
                    if (parseInt(selected) === exp.id) {
                        toReturn.push({ value: exp.id, label: exp.name });
                    }
                }
            } else {
                toReturn.push({ value: exp.id, label: exp.name });
            }

        }

        if (selectedFilters) return toReturn;

        toReturn.push({ value: null, label: "Tutte" });

        return toReturn;
    }

    prepareGuideFilter(allGuides, selectedFilters) {
        let toReturn = [];
        let toAssignSelected = false;
        for (let guide of allGuides) {
            if (selectedFilters) {
                let arrayOfId = selectedFilters.split(',');
                for (let selected of arrayOfId) {
                    if (parseInt(selected) === guide.id) {
                        toReturn.push({ value: guide.id, label: guide.fullname });
                    }
                    if (selected === "toAssign") toAssignSelected = true;
                }
            } else {
                toReturn.push({ value: guide.id, label: guide.fullname });
            }
        }

        if (selectedFilters) {
            if (toAssignSelected) toReturn.push({ value: "toAssign", label: "Da assegnare" });
            return toReturn;
        }

        toReturn.push({ value: "toAssign", label: "Da assegnare" });
        toReturn.push({ value: null, label: "Tutte" });

        return toReturn;
    }

    handleChangeGuideFilterSubmit = selectedFilters => {
        console.log('SELECTED Filter: ', selectedFilters);
        let { filters } = this.state;
        if (selectedFilters && selectedFilters.length) {
            let arrayGuides = [];
            selectedFilters.map((guideObj) => arrayGuides.push(guideObj.value));
            filters.guide = arrayGuides.join(',');
        } else {
            filters.guide = '';
        }
        this.getToursByFilter(filters);
    };

    handleChangeExpFilterSubmit = selectedFilters => {
        console.log('SELECTED Filter: ', selectedFilters);
        let { filters } = this.state;
        if (selectedFilters && selectedFilters.length) {
            let arrayExperiences = [];
            selectedFilters.map((epxObj) => arrayExperiences.push(epxObj.value));
            filters.experience = arrayExperiences.join(',');
        } else {
            filters.experience = '';
        }
        this.getToursByFilter(filters);
    };

    handleDeleteTours = () => {

        const { rowSelected, filters } = this.state;

        let tours = [];

        if (rowSelected) rowSelected.map(tourId => tours.push(tourId));

        if (tours.length) {
            deleteBulkTours(tours)
                .then(() => this.getToursByFilter(filters))
                .catch((e) => console.log('error: ', e));
        }

    }

    handleEditTours = (params) => {

        const { rowSelected, filters } = this.state;

        let tours = [];

        if (rowSelected) rowSelected.map(tourId => tours.push(tourId));

        if (tours.length) {
            editBulkTours(tours, params)
                .then(() => this.getToursByFilter(filters))
                .catch((e) => console.log('error: ', e));
        }

    }

    render() {

        return (
            <div className="container-fluid">
                <main id="content" role="main">
                    {/* <!-- Upload Form Section --> */}
                    <div className="container space-2">
                        <div className="text-center">
                            <h1 className="h2 kt-font-brand font-weight-medium">Tour programmati</h1>
                            <p>Clicca sul tour che vuoi analizzare meglio</p>
                        </div>
                        <div className="my-5">
                            {this.renderTable()}
                            {this.renderTourModal()}
                            {this.renderRangeCalendarModal()}
                            {this.renderSuccessModal()}
                            {this.renderFailModal()}
                        </div>
                        <Typography variant="h5" className="mt-2 mb-5 pb-3 kt-font-brand">
                            Azioni Massive
                        </Typography>
                        <div className="mb-7">
                            <Typography variant="h6" className="my-2 border-bottom pb-3 kt-font-facebook">
                                Assegnazioni
                            </Typography>
                            <div className="row">
                                {this.renderMultipeAssign()}
                            </div>
                        </div>
                        <div className="mb-7">
                            <Typography variant="h6" className="my-2 border-bottom pb-3 kt-font-facebook">
                                Cambio orario
                            </Typography>
                            <div className="row">
                                {this.renderMultipleTimeEdit()}
                            </div>
                        </div>
                    </div>
                </main>
            </div>)

    }

    renderTable() {
        const { tours, experiences, allGuides, filters, rowSelected } = this.state;

        if (!experiences.length || !allGuides.length) return "";

        let pageOptions = { pageCurrent: null, paginationSize: null };

        // const columns = this.renderColumns(this.props.discovery);
        pageOptions.paginationSize = 10;

        let selectedGuideFilter = filters.guide ? this.prepareGuideFilter(allGuides, filters.guide) : null;
        let selectedExperienceFilter = filters.experience ? this.prepareExperienceFilter(experiences, filters.experience) : null;

        const GuideOptions = this.prepareGuideFilter(allGuides);
        const ExperienceOptions = this.prepareExperienceFilter(experiences);

        const toursParsed = this.renderRows(tours);

        const sizePerPageList = [{
            text: '5', value: 5
        }, {
            text: '10', value: 10
        }, {
            text: '25', value: 25
        }, {
            text: '50', value: 50
        }, {
            text: '100', value: 100
        }, {
            text: '250', value: 250
        }, {
            text: '500', value: 500
        }];

        const sizePerPageOptionRenderer = ({
            text,
            page,
            onSizePerPageChange
        }) => (
            <li
                key={text}
                role="presentation"
                className="dropdown-item"
            >
                <a
                    href="#"
                    tabIndex="-1"
                    key={text}
                    role="menuitem"
                    data-page={page}
                    onMouseDown={(e) => {
                        onSizePerPageChange(page);
                        e.preventDefault();
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    {text}
                </a>
            </li>
        );



        const onPaginationChange = (sizePerPage, page) => {
            console.log('size per page: ', sizePerPage);
            console.log('page: ', page);
        }

        const rowEvents = {
            onClick: (e, row, rowIndex) => {
                let tourToOpen = row.originalTour;
                if (tourToOpen) {
                    tourToOpen.startTime = new Date(tourToOpen.startTime)
                    tourToOpen.endTime = new Date(tourToOpen.endTime)
                    this.setState({ selectedEvent: tourToOpen, showTourModal: true });
                }
            }
        };

        const optionsPagination = {
            sizePerPageOptionRenderer: sizePerPageOptionRenderer,
            sizePerPage: pageOptions.paginationSize,
            sizePerPageList: sizePerPageList,
            onSizePerPageChange: onPaginationChange,
            page: pageOptions.pageCurrent || 1
        };

        const rowClasses = 'table-small link-cursor-pointer';

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: false,
            onSelect: (row, isSelect, rowIndex, e) => {
                const { rowSelected } = this.state;
                if (isSelect) {
                    rowSelected.push(row.id);
                } else {
                    let idx = rowSelected.indexOf(row.id);
                    if (idx !== -1) {
                        rowSelected.splice(idx, 1);
                    }
                }

                this.setState({ rowSelected: rowSelected });
            },
            onSelectAll: (isSelect, rows, e) => {
                let { rowSelected } = this.state;
                if (isSelect) {
                    rows.map((row) => rowSelected.push(row.id));
                } else {
                    rowSelected = [];
                }

                this.setState({ rowSelected: rowSelected });
            }
        };

        const columns = [{
            dataField: 'date',
            text: 'Data',
            sort: true
        }, {
            dataField: 'startTime',
            text: 'Inizio',
            sort: true,
        }, {
            dataField: 'endTime',
            text: 'Fine',
            sort: true,
        }, {
            dataField: 'experience',
            text: 'Experience',
            sort: true,
        }, {
            dataField: 'guide',
            text: 'Guida',
            sort: true,
        }, {
            dataField: 'privato',
            text: 'Privato',
        }, {
            dataField: 'open',
            text: 'Aperto',
        }, {
            dataField: 'originalDate',
            hidden: true
        }
        ];


        const defaultSorted = [{
            dataField: 'originalDate', // if dataField is not match to any column you defined, it will be ignored.
            order: 'asc' // desc or asc
        }];

        return <ToolkitProvider
            keyField="toursTable"
            // trClassName={"text-secondary no-min-height"}
            data={toursParsed}
            columns={columns}
            search={{
                searchFormatted: true
            }}
        >
            {
                props => (
                    <div>
                        <div className="d-flex flex-row justify-content-between align-items-start mb-2">
                            <div className="flex-grow-0">
                                <SearchBar className="form-control-xs" {...props.searchProps} />
                                <button className="btn btn-sm btn-icon btn-facebook mt-1 mx-3"
                                    onClick={() => this.setState({ showRangeCalendarModal: true })}>
                                    <i className="flaticon-event-calendar-symbol" />
                                </button>
                            </div>
                            <div className="d-flex justify-content-end mt-1">
                                <button className="btn btn-sm btn-success mx-1 d-block"
                                    disabled={rowSelected.length === 0}
                                    onClick={() => this.handleEditTours({ open: true })}>
                                    Apri
                                </button>
                                <button className="btn btn-sm btn-info mx-1 d-block"
                                    disabled={rowSelected.length === 0}
                                    onClick={() => this.handleEditTours({ open: false })}>
                                    Chiudi
                                </button>
                                <button className="btn btn-sm btn-warning ml-1 mr-2 d-block"
                                    disabled={rowSelected.length === 0}
                                    onClick={this.handleDeleteTours}>
                                    Elimina
                                </button>
                            </div>
                            <div className="d-flex flex-grow-1 justify-content-end mb-2">
                                <Select
                                    className="btn-xs no-min-height basic-multi-select"
                                    key="selectFilter"
                                    styles={reactSelectStyles}
                                    isMulti
                                    placeholder="Filtra per guida"
                                    value={selectedGuideFilter}
                                    onChange={this.handleChangeGuideFilterSubmit}
                                    options={GuideOptions}
                                />
                                <Select
                                    className="btn-xs no-min-height basic-multi-select"
                                    key="selectSort"
                                    isMulti
                                    styles={reactSelectStyles}
                                    placeholder="Filtra per experience"
                                    value={selectedExperienceFilter}
                                    onChange={this.handleChangeExpFilterSubmit}
                                    options={ExperienceOptions}
                                />
                            </div>
                        </div>
                        <BootstrapTable {...props.baseProps} striped bordered keyField='id'
                            rowClasses={rowClasses}
                            rowEvents={rowEvents}
                            defaultSorted={defaultSorted}
                            selectRow={selectRow}
                            pagination={paginationFactory(optionsPagination)}
                        />
                    </div>
                )
            }
        </ToolkitProvider>

    }

    renderRows(tours) {

        let toReturn = [];

        for (let tour of tours) {
            let parseTour = {
                id: tour.id,
                startTime: moment(tour.startTime).format('HH:mm'),
                endTime: moment(tour.endTime).format('HH:mm'),
                date: formatDate(moment(tour.date).format('DD-MM-yyyy')),
                originalDate: tour.date,
                guide: tour.guide ? tour.guide.fullname : "Da assegnare",
                experience: tour.experience && tour.experience.name ? tour.experience.name : "No value",
                privato: tour.privato ? "Si" : "No",
                open: tour.open ? "Si" : "No",
                originalTour: tour
            }
            toReturn.push(parseTour);
        }

        return toReturn;
    }

    renderMultipleTimeEdit() {

        const { editStartTime, editEndTime, rowSelected } = this.state;

        const handleSTDateChange = (date) => {

            this.setState({ editStartTime: date });

        };

        const handleETDateChange = (date) => {

            this.setState({ editEndTime: date });

        };

        const handleMultipleTimeChange = () => {
            const { rowSelected, editStartTime, editEndTime, filters } = this.state;

            let tours = [];

            if (rowSelected) rowSelected.map(tourId => tours.push(tourId));

            let params = {
                startTime: editStartTime,
                endTime: editEndTime
            }

            editBulkTimeTours(tours, params)
                .then(() => {
                    this.setState({ showSuccessAlert: true })
                    this.getToursByFilter(filters);
                })
                .catch(() => {
                    this.setState({ showFailAlert: true })
                })
        }


        return <Grid container className="ml-3 mb-5">
            <Grid lg={12} className="mb-3 mt-5">
                <Typography variant="p" className="secondary">
                    Scegli il nuovo orario dei tour selezionati
                </Typography>
            </Grid>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container className="ml-3 mb-5">
                    <Grid lg={6}>
                        <Grid lg={10} item>
                            <label className="form-label" forHtml="editStartTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
                            <div className="input-group">
                                <KeyboardTimePicker
                                    margin="normal"
                                    id="editStartTimePicker"
                                    ampm={false}
                                    value={editStartTime}
                                    onChange={handleSTDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </div>

                        </Grid>
                    </Grid>
                    <Grid lg={6}>
                        <Grid lg={10} className="ml-1" item>
                            <label className="form-label" forHtml="editEndTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
                            <div className="input-group">
                                <KeyboardTimePicker
                                    margin="normal"
                                    id="editEndTimePicker"
                                    ampm={false}
                                    value={editEndTime}
                                    onChange={handleETDateChange}
                                    KeyboardButtonProps={{
                                        'aria-label': 'change time',
                                    }}
                                />
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </MuiPickersUtilsProvider>
            <Grid lg={12}>
                <Grid lg={2} className="ml-auto" item>
                    <Button color="primary" disabled={rowSelected.length === 0} onClick={handleMultipleTimeChange}>Cambia orario</Button>
                </Grid>
            </Grid>
        </Grid>
    }

    renderMultipeAssign() {

        const { selectedGuide, allGuides, rowSelected } = this.state;

        const handleChangeGuideAssignSubmit = selectedFilter => {
            console.log('Guide to assign: ', selectedFilter);
            this.setState({ selectedGuide: selectedFilter });
        };

        const handleAssignSubmition = () => {
            const { rowSelected, selectedGuide, filters } = this.state;

            assignTours(rowSelected, selectedGuide.value)
                .then(() => {
                    this.setState({ showSuccessAlert: true })
                    this.getToursByFilter(filters);
                })
                .catch(() => {
                    this.setState({ showFailAlert: true })
                })

        }

        return <Grid container className="ml-3 mb-5">
            <Grid lg={12} className="mb-3 mt-5">
                <Typography variant="p" className="secondary">
                    Scegli una guida per assegnare i tuor selezionati
                </Typography>
            </Grid>
            <Grid lg={8}>
                <Grid lg={10} item>
                    <Select
                        className="w-75 btn-xs no-min-height"
                        key="selectFilter"
                        styles={reactSelectStyles}
                        placeholder="Guide disponibili"
                        value={selectedGuide}
                        onChange={handleChangeGuideAssignSubmit}
                        options={allGuides.map((guide) => { return { value: guide.id, label: guide.fullname } })}
                    />
                </Grid>
            </Grid>
            <Grid lg={4}>
                <Grid lg={10} className="ml-1" item>
                    <Button color="primary" disabled={rowSelected.length === 0 || !selectedGuide} onClick={handleAssignSubmition}>Assegna</Button>
                </Grid>
            </Grid>
        </Grid>
    }

    renderTourModal() {
        return <TourModal closeModal={() => this.closeModal()}
            tour={this.state.selectedEvent}
            allTours={this.state.tours}
            guides={this.state.allGuides}
            updateTour={this.updateTour}
            updateAllTour={this.updateAllTour}
            showModal={this.state.showTourModal} />
    }

    renderRangeCalendarModal() {

        const { showRangeCalendarModal, from, to } = this.state;

        const handleClose = () => {
            const { from, to, filters } = this.state;
            this.setState({ showRangeCalendarModal: false })
            if (from && to) {
                filters.from = from;
                filters.to = to;
                this.getToursByFilter(filters);
            }
        };

        const updateFromTo = (range) => {
            this.setState({ from: range.from, to: range.to })
        }


        return (<Modal show={showRangeCalendarModal} size="md" className="w-100" onHide={handleClose} >

            <Modal.Header closeButton>
                <Modal.Title className="h6">Scegliere il periodo di tempo</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <CustomRangePicker className="w-100" updateFromTo={updateFromTo} from={from} to={to} />
            </Modal.Body>
            <Modal.Footer>
                <Button className="btn btn-facebook" onClick={handleClose}>
                    Applica
                </Button>
            </Modal.Footer>
        </Modal>)
    }

    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState({ showSuccessAlert: false })
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }

}

export default injectIntl(
    connect(
    )(ToursPage)
);