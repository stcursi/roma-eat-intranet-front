import React, { Component, useRef } from "react";
import { Tabs, Tab, Button, Collapse, Modal } from "react-bootstrap";
import { connect } from "react-redux";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";
import { Link } from "react-router-dom";
import {
    Grid,
    Typography,
    Select as SelectUI,
} from "@material-ui/core";
import { formatDate } from "../../utils/date";
import TappeSelector from "../tappa/TappeSelector";
import { getAllTappe } from "../../crud/tappa.crud";
import { getTour, getAllTours, editTour } from "../../crud/tour.crud";
import { getAllFonti } from "../../crud/fonte.crud";
import { getAllPaymentMethods } from "../../crud/paymentMethod.crud";
import { editBulkPartecipations, deleteBulkPartecipations, editPartecipation } from "../../crud/partecipation.crud";
import { editCustomer } from "../../crud/customer.crud";
import { getReportByTour } from "../../crud/tourReport.crud";
import { getAllByRole } from "../../crud/user.crud";
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import { Type } from 'react-bootstrap-table2-editor';
import Select from 'react-select';
import cellEditFactory from 'react-bootstrap-table2-editor';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { selectColor } from "../../utils/calendar";
import "./Tours.css";
import { injectIntl } from "react-intl";
import moment from 'moment';

const reactSelectStyles = {
    container: (base, state) => ({
        ...base,
        opacity: state.isDisabled ? ".5" : "1",
        backgroundColor: "transparent",
        margin: "0 10px 10px 0",
        width: "40%",
        zIndex: "999",
    })
};

const { SearchBar, ClearSearchButton } = Search;

class ToursPage extends Component {

    constructor(props) {
        super(props);

        this.myFormRef = React.createRef();
        this.assignedGuide = React.createRef();
        this.assignedSecondGuide = React.createRef();

        this.state = {
            tappe: [],
            report: null,
            guides: null,
            selectedTappe: [],
            columnsToEdit: [],
            commColToEdit: [],
            fonti: [],
            paymentMethods: [],
            orderTappe: {},
            collapseOpen: false,
            collapseNewTour: false,
            secondaryInfomation: true,
            rowSelected: [],
            date: null,
            startTime: null,
            endTime: null,
            selectedNewTour: null,
            sendMailGuides: false,
            notifyGuides: false
        }
    }


    componentDidMount() {

        getAllTappe()
            .then((res) => {
                if (res && res.data) this.setState({ tappe: res.data });
            })
            .then(() => {


            })
            .catch((error) => {
                console.error("[TourModal] - getAllTappe: ", error);
            })


        getAllFonti()
            .then((res) => {
                if (res && res.data) this.setState({ fonti: res.data });
            })
            .catch((error) => {
                console.error("[TourModal] - getAllFonti: ", error);
            });


        getAllPaymentMethods()
            .then((res) => {
                if (res && res.data) this.setState({ paymentMethods: res.data });
            })
            .catch((error) => {
                console.error("[TourModal] - getAllPayments: ", error);
            });

    }

    prepareSetOfEntities(data) {
        const toReturn = [];
        for (let item of data) {
            toReturn.push({ value: item.id, label: item.type })
        }

        return toReturn;
    }

    initializeNewTour(data) {
        let toReturn = {};

        const { startTime, endTime, date, selectedTappe } = this.state;

        toReturn.meetingPoint = data.meetingPoint ? data.meetingPoint.value : null;
        let tappeToJoin = [];

        let orderTappeToSave = {}
        if (selectedTappe) {
            for (let selTappa of selectedTappe) {
                if (data["tappa_" + selTappa] && data["tappa_" + selTappa].value) {
                    orderTappeToSave[data["tappa_" + selTappa].value] = selTappa;
                }
            }

            let i = 1;

            while (i <= selectedTappe.length) {
                tappeToJoin.push(orderTappeToSave[i])
                i++;
            }
        }

        toReturn.orderTappe = tappeToJoin.join(',');

        toReturn.guide = this.assignedGuide.current && this.assignedGuide.current.value !== "noguide" ? this.assignedGuide.current.value : null;
        toReturn.secondGuide = this.assignedSecondGuide.current && this.assignedSecondGuide.current.value !== "noguide" ? this.assignedSecondGuide.current.value : null;
        toReturn.note = data.note ? data.note.value : "";
        toReturn.noteGuida = data.noteGuida ? data.noteGuida.value : "";

        toReturn.date = date ? moment(date).format() : null;
        let tourDayMomentForStart = date ? moment(date) : null;
        let tourDayMomentForEnd = date ? moment(date) : null;
        let startTimeMoment = startTime ? moment(startTime, 'HH:mm') : null;
        let endTimeMoment = endTime ? moment(endTime, 'HH:mm') : null;

        if (startTimeMoment && tourDayMomentForStart) {
            toReturn.startTime = tourDayMomentForStart.set({
                hour: startTimeMoment.get('hour'),
                minute: startTimeMoment.get('minute')
            })
        }

        if (endTimeMoment && tourDayMomentForEnd) {
            toReturn.endTime = tourDayMomentForEnd.set({
                hour: endTimeMoment.get('hour'),
                minute: endTimeMoment.get('minute')
            })
        }

        return toReturn;
    }



    render() {

        const { tour } = this.props;
        let { tappe, selectedTappe, orderTappe, collapseNewTour, collapseOpen, rowSelected, guides } = this.state;

        if (!tour) return "";

        const updateTappe = (event) => {

            this.setState({ selectedTappe: event.target.value })
        }


        const handleClose = () => {

            this.setState({
                selectedTappe: [],
                orderTappe: {},
                selectedNewTour: null,
                collapseNewTour: false,
                collapseOpen: false,
                sendMailGuides: false,
                notifyGuides: false
            });

            this.props.closeModal();
        };

        const handleEdit = (e) => {

            const { tour } = this.props;

            let params = {
                data: this.initializeNewTour(this.myFormRef.current),
                sendMailGuides: this.state.sendMailGuides,
                notifyGuides: this.state.notifyGuides
            }

            editTour(tour.id, params)
                .then(() => getTour(tour.id))
                .then((res) => {
                    this.props.updateTour(res.data);
                })
                .catch((error) => {
                    console.log('Error in edit tour with id: ' + tour.id + " - ", error);
                })

            this.setState({
                selectedTappe: [],
                sendMailGuides: false,
                notifyGuides: false,
                orderTappe: {}
            });


            this.props.closeModal();

        };

        const handleEntered = () => {

            if (this.props.showModal) {
                const { tour } = this.props;
                let { tappe, selectedTappe, orderTappe } = this.state;

                if (tour.orderTappe && tour.orderTappe.indexOf(',') !== -1) {
                    let splitted = tour.orderTappe.split(',');
                    for (let i = 0; i <= splitted.length - 1; i++) {
                        orderTappe[i + 1] = splitted[i];
                        let filtered = tappe.filter(t => t.id && t.id.toString() === splitted[i].toString());
                        if (filtered && filtered.length) {
                            let currentTappa = filtered[0];
                            selectedTappe.push(currentTappa.id);
                        }
                    }
                }


                this.setState({
                    selectedTappe: selectedTappe,
                    orderTappe: orderTappe,
                    date: tour.date,
                    startTime: tour.startTime,
                    endTime: tour.endTime
                });
            }
        }

        if (!tour) return "";

        const getSpecificData = () => {
            this.setState({report: null, guides: null});

            const dataFrom = moment().subtract(1, 'months').startOf('day');
    
            getAllTours({ from: dataFrom })
                .then((res) => {
                    if (res && res.data) {
                        let sortedTOurs = res.data.sort((a, b) => (new Date(a.startTime).getTime() - new Date(b.startTime).getTime()));
    
                        this.setState({ allTours: sortedTOurs});
                    } else {
                        this.setState({ allTours: []});
                    }
                })
                .catch((error) => {
                    console.error("[TourModal] - getAllTours: ", error);
                })
            
            getReportByTour(this.props.tour.id)
                .then((res) => {
                    if (res && res.data) this.setState({ report: res.data })
                })
                .catch((error) => {
                    console.error("[TourModal] - getReportByTour: ", error);
                });

            let params = {
                startTime: this.props.tour.startTime,
                endTime: this.props.tour.endTime
            }

            getAllByRole('GUIDE')
                .then((res) => {
                    this.setState({ guides: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAllByRole-GUIDE: ', error);
                }) 

        }

        const getNUmberPartecipant = () => {

            const arrayFiltered = this.props.tour?.partecipants ? this.props.tour.partecipants.filter((p) => !p.annullato) : [];

            if (arrayFiltered.length) {
                return arrayFiltered.length; 
            }

            return "0";
        }


        return <Modal show={this.props.showModal} size="xl" className="w-100" onShow={getSpecificData}
            onHide={handleClose}
            onEntered={handleEntered} >
            <Modal.Header closeButton>
                <div className="d-flex justify-content-between w-100">
                    <Modal.Title>
                        <span>Dettaglio del Tour: {tour.experience.name}</span>
                    </Modal.Title>
                    <Modal.Title>
                        <span>Data: {formatDate(moment(tour.startTime).format(), true)}</span>
                    </Modal.Title>
                    <Modal.Title>
                        <span>Partecipanti: {getNUmberPartecipant()}</span>
                    </Modal.Title>
                </div>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <form ref={this.myFormRef}>
                    <Tabs defaultActiveKey="info" id="uncontrolled-tab-example">
                        <Tab eventKey="info" style={{ "backgroundColor": "#f8f9fa" }} title="Home">
                            <div className="form-row">
                                <div className="form-group col-12">
                                    <label forHtml="meetingPoint">Legenda</label>
                                    <div className="input-group">
                                        <p className="my-2" id="legenda" placeholder="Legenda">{selectColor(tour).legenda}</p>
                                    </div>

                                </div>
                                <div className="form-group col-md-6">
                                    <label forHtml="meetingPoint">Meeting Point</label>
                                    <div className="input-group">
                                        <div className="input-group-prepend">
                                            <span className="input-group-text" id="meetingPointLabel">
                                                <span className="fas fa-file-signature"></span>
                                            </span>
                                        </div>
                                        <input type="text" defaultValue={tour.meetingPoint} className="form-control" id="meetingPoint" placeholder="Meeting Point" />
                                    </div>

                                </div>
                                <div className="form-group col-md-6">
                                    <div className="js-focus-state">
                                        <label className="form-label" forHtml="guide">Guida</label>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="guideLabel">
                                                    <span className="fas fa-user"></span>
                                                </span>
                                            </div>
                                            <select className="custom-select" id="guide" placeholder="Scegliere email pre" aria-describedby="guideLabel"
                                                ref={this.assignedGuide}>
                                                <option selected value="noguide">Scegli la guida d'assegnare!</option>
                                                {guides ? guides.map(guide => <option selected={(tour.guide && tour.guide.id === guide.id)} key={"guida_" + guide.id} value={guide.id}>{guide.fullname}</option>) : <option>caricamento guide</option>}
                                            </select>
                                        </div>
                                        <p className="small text-muted mr-2">{tour.confermato ? 'Confermata' : 'Da confermare'}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="form-group col-md-6">
                                    <div className="js-focus-state">
                                        <label className="form-label" htmlFor="secondGuide">Seconda Guida</label>
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="secondGuideLabel">
                                                    <span className="fas fa-user"></span>
                                                </span>
                                            </div>
                                            <select className="custom-select" id="secondGuide" placeholder="Scegliere seconda guida" aria-describedby="secongGuideLabel"
                                                ref={this.assignedSecondGuide}>
                                                <option selected value="noguide">Hai una seconda guida da assegnare?</option>
                                                {guides ? guides.map(guide => <option selected={(tour.secondGuide && tour.secondGuide.id === guide.id)} key={"guida_" + guide.id} value={guide.id}>{guide.fullname}</option>) : <option>caricamento guide</option>}
                                            </select>
                                        </div>
                                        <p className="small text-muted mr-2">{tour.confermatoSecondaGuida ? 'Confermata' : 'Da confermare'}</p>
                                    </div>
                                </div>
                                <div className="form-group col-md-6">
                                    <div className="js-focus-state ml-4">
                                        <label className="form-label" forHtml="sendMailGuides"><span className="fas fa-envelope"></span> Notifica assegnazione</label>
                                        <div className="input-group">
                                            <div className="d-flex align-items-center">
                                                <input type="checkbox" name="sendMailGuides" onChange={() => this.setState({ sendMailGuides: !this.state.sendMailGuides, notifyGuides: false })}
                                                    id="sendMailGuides" value="Avvisa guide" /><span className="mx-1">Avvisa guide</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                {this.renderTimePickers()}
                            </div>
                            <div className="form-row">
                                {this.renderDayPicker()}
                            </div>
                            <div className="form-row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <label className="form-label">
                                            Note admin
                                        </label>
                                        <div className="input-group">
                                            <textarea className="form-control" rows="6" name="text" id="note" placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                                defaultValue={tour.note}
                                                data-msg="Please enter a property description."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"></textarea>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <label className="form-label">
                                            Note guida
                                        </label>
                                        <div className="input-group">
                                            <textarea className="form-control" rows="6" name="text" id="noteGuida" placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                                data-msg="Please enter a property description."
                                                defaultValue={tour.noteGuida}
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"></textarea>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>

                        </Tab>
                        <Tab eventKey="partecipations" style={{ "backgroundColor": "#f8f9fa" }} title="Partecipanti">
                            {this.renderTable()}
                            <div className="d-flex">
                                <Link
                                    to={"/partecipant/add/" + tour.id}
                                    className="btn btn-primary"
                                >
                                    Aggiungi un nuovo partecipante
                                </Link>
                                <Button
                                    className="mx-2"
                                    onClick={() => this.setState({ collapseNewTour: !collapseNewTour })}
                                    disabled={!rowSelected.length}
                                    aria-controls="collapse-new-tour-text"
                                    aria-expanded={collapseNewTour}
                                >
                                    Sposta in un altro tour
                                </Button>
                            </div>
                            {this.state.allTours?.length && this.renderMoveToAnotherTour()}
                        </Tab>
                        <Tab eventKey="revisions" style={{ "backgroundColor": "#f8f9fa" }} title="Revisione tour">
                            {this.renderTourReport()}
                        </Tab>
                        <Tab eventKey="tappe" style={{ "backgroundColor": "#f8f9fa" }} title="Tappe">
                            <div className="form-row">
                                <div className="form-group col-lg-10 col-md-12">
                                    {tappe.length ? <TappeSelector tappe={tappe}
                                        orderTappe={orderTappe}
                                        selectedTappe={selectedTappe}
                                        updateTappe={updateTappe} /> : ""}

                                </div>
                            </div>
                        </Tab>
                        <Tab eventKey="communications" style={{ "backgroundColor": "#f8f9fa" }} title="Comunicazioni">
                            {this.renderCommunicationsPartecipant()}
                        </Tab>
                    </Tabs>
                </form>
            </Modal.Body>
            <Modal.Footer>
                <div className="d-flex align-items-center mr-8">
                    <input type="checkbox" disabled={this.state.sendMailGuides}
                        onChange={() => this.setState({ notifyGuides: !this.state.notifyGuides })}
                        checked={this.state.notifyGuides}
                        name="notifyGuides"
                        id="notifyGuides"
                        value="Aggiorna guide" />
                    <span className="mx-1">Aggiorna guide</span>
                </div>
                <Button variant="oulined" onClick={handleClose}>
                    Close
                </Button>
                <Button variant="link" onClick={(e) => handleEdit(e)}>
                    Modifica
                </Button>
            </Modal.Footer>
        </Modal >

    }

    renderDayPicker() {
        const { tour } = this.props;

        let { date } = this.state;

        const handleFromDayChange = (day) => {

            this.setState({ date: day });

        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" forHtml="fromTimePicker"><span className="fas fa-clock"></span> Giorno </label>
                        <div className="input-group">
                            <KeyboardDatePicker
                                disableToolbar
                                variant="dialog"
                                format="dd/MM/yyyy"
                                margin="normal"
                                id="from-picker-dialog"
                                label="Date picker"
                                value={date}
                                onChange={handleFromDayChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

    renderTimePickers() {

        let { startTime, endTime } = this.state;

        const handleSTDateChange = (date) => {

            this.setState({ startTime: date });

        };

        const handleETDateChange = (date) => {

            this.setState({ endTime: date });

        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" forHtml="startTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="startTimePicker"
                                ampm={false}
                                value={startTime}
                                onChange={handleSTDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>

                    </Grid>
                </Grid>
                <Grid lg={6}>
                    <Grid lg={10} className="ml-1" item>
                        <label className="form-label" forHtml="endTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="endTimePicker"
                                ampm={false}
                                value={endTime}
                                onChange={handleETDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }

    renderTable() {
        const { tour } = this.props;
        let tableRef = React.createRef();

        const { secondaryInfomation, columnsToEdit, paymentMethods, fonti, rowSelected } = this.state;

        if (!tour || !tour.partecipants || !tour.partecipants.length) return "";

        let pageOptions = { pageCurrent: null, paginationSize: null };

        pageOptions.paginationSize = 15;

        const partecipantsParsed = this.renderRows(tour.partecipants);

        const sizePerPageList = [{
            text: '5', value: 5
        }, {
            text: '15', value: 15
        }, {
            text: '25', value: 25
        }, {
            text: '50', value: 50
        }, {
            text: '100', value: 100
        }, {
            text: '250', value: 250
        }, {
            text: '500', value: 500
        }];

        const sizePerPageOptionRenderer = ({
            text,
            page,
            onSizePerPageChange
        }) => (
            <li
                key={text}
                role="presentation"
                className="dropdown-item"
            >
                <a
                    href="#"
                    tabIndex="-1"
                    key={text}
                    role="menuitem"
                    data-page={page}
                    onMouseDown={(e) => {
                        onSizePerPageChange(page);
                        e.preventDefault();
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    {text}
                </a>
            </li>
        );



        const onPaginationChange = (sizePerPage, page) => {
            console.log('size per page: ', sizePerPage);
            console.log('page: ', page);
        }


        const optionsPagination = {
            sizePerPageOptionRenderer: sizePerPageOptionRenderer,
            sizePerPage: pageOptions.paginationSize,
            sizePerPageList: sizePerPageList,
            onSizePerPageChange: onPaginationChange,
            page: pageOptions.pageCurrent || 1
        };

        const cellEdit = cellEditFactory({
            mode: 'click',
            afterSaveCell: (oldValue, newValue, row, column) => {

                let dataSplitted = column.dataField && column.dataField.split('.');
                let param;
                if (dataSplitted.length > 1) {
                    param = dataSplitted[1];
                } else {
                    param = dataSplitted[0];
                }
                if (param === "imprevisto" || param === "noteAdmin" || param === "note" || param === "price" ||
                    newValue === "true" ||
                    newValue === "false") {
                    if (newValue === "true") newValue = true;
                    if (newValue === "false") newValue = false;
                    handleEditPartecipation(row.id, param, newValue);
                } else {
                    handleEditCustomer(row.customer.id, param, newValue);
                }
            },
        });

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: false,
            onSelect: (row, isSelect, rowIndex, e) => {
                const { rowSelected } = this.state;
                if (isSelect) {
                    rowSelected.push(row.id);
                } else {
                    let idx = rowSelected.indexOf(row.id);
                    if (idx !== -1) {
                        rowSelected.splice(idx, 1);
                    }
                }

                this.setState({ rowSelected: rowSelected });
            },
            onSelectAll: (isSelect, rows, e) => {
                let { rowSelected } = this.state;
                if (isSelect) {
                    rows.map((row) => rowSelected.push(row.id));
                } else {
                    rowSelected = [];
                }

                this.setState({ rowSelected: rowSelected });
            }
        };

        const reactSelectStyles = {
            container: (base, state) => ({
                ...base,
                opacity: state.isDisabled ? ".5" : "1",
                backgroundColor: "transparent",
                zIndex: "999",
                minWidth: "200px"
            })
        };

        const rowStyle = (row, rowIndex) => {

            if (row.imprevisto && row.imprevisto != "") {
                return { backgroundColor: '#ffb822' }
            }
            if (row.annullato) {
                return { backgroundColor: '#F32013' }
            }

            // return { ... };
        };

        const handleSelectColumnToEdit = selectedColumns => {
            console.log('SELECTED Filter: ', selectedColumns);
            this.setState({ columnsToEdit: selectedColumns });
        };

        const handleEditColumns = (event, value) => {

            event.preventDefault();
            const { columnsToEdit, rowSelected } = this.state;

            const { } = this.state;

            let partecipants = [];
            let params = {};

            for (let col of columnsToEdit) {
                params[col.value] = value;
            }

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, params)
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }

        const editColumns = [{
            value: 'paid',
            label: 'Pagato'
        }, {
            value: 'customer.vegetarian',
            label: 'Vegetariano'
        }
            //  {
            //     value: 'customer.vegan',
            //     label: 'Vegano'
            // }
        ]

        const basicStyle = { whiteSpace: "nowrap", overflowX: "hidden", textOverflow: "ellipsis" };

        const columns = [{
            dataField: 'name',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "6rem" };
            },
            text: 'Nome',
            sort: true,
        }, {
            dataField: 'surname',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "6rem" };
            },
            text: 'Cognome',
            sort: true,
        }, {
            dataField: 'customer.email',
            editable: true,
            text: 'Email',
            style: basicStyle,
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "6rem" };
            },
            sort: true,
        }, {
            dataField: 'customer.telephone',
            editable: true,
            style: basicStyle,
            hidden: !secondaryInfomation,
            text: 'Telephone',
            headerStyle: () => {
                return { minWidth: "6rem" };
            },
            sort: true,
        }, {
            dataField: 'bookedFrom.type',
            editable: false,
            text: 'Prenotato da',
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "7.5rem" };
            }
        }, {
            dataField: 'customer.provenienza',
            editable: true,
            text: 'Provenienza',
            style: basicStyle,
            hidden: secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "7rem" };
            },
            sort: false,
        }, {
            dataField: 'imprevisto',
            editable: true,
            text: 'Imprevisto',
            style: basicStyle,
            hidden: secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "7rem" };
            },
            sort: false,
        }, {
            dataField: 'price',
            editable: true,
            text: 'Importo',
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { width: "6rem" };
            }
        }, {
            dataField: 'paymentMethod.type',
            editable: false,
            text: 'Pagamento',
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "6.5rem" };
            }
        }, {
            dataField: 'paid',
            editable: true,
            text: 'Pagato',
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { width: "5.5rem" };
            },
            editor: {
                type: Type.SELECT,
                options: [{
                    value: true,
                    label: 'Si'
                }, {
                    value: false,
                    label: "No"
                }]
            }
        }, {
            dataField: 'vegetarian',
            editable: false,
            hidden: secondaryInfomation,
            text: 'Vegetariano',
            headerStyle: () => {
                return { width: "7.5rem" };
            }
        },
        //  {
        //     dataField: 'vegan',
        //     editable: false,
        //     hidden: secondaryInfomation,
        //     text: 'Vegano',
        //     headerStyle: () => {
        //         return { width: "5.5rem" };
        //     }
        // },
        {
            dataField: 'allergies',
            editable: true,
            hidden: secondaryInfomation,
            text: 'Allergie',
            headerStyle: () => {
                return { width: "10rem" };
            }
        }, {
            dataField: 'note',
            editable: true,
            text: 'Note',
            hidden: secondaryInfomation,
            headerStyle: () => {
                return { width: "13.5rem" };
            }
        }, {
            dataField: 'noteAdmin',
            editable: true,
            text: 'Note admin',
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { minWidth: "7.5rem" };
            }
        }, {
            dataField: 'arrivato',
            editable: false,
            text: <div className="kt-notification__item-icon">
                <i className="flaticon2-check-mark kt-font-success" />
            </div>,
            hidden: !secondaryInfomation,
            headerStyle: () => {
                return { width: "3.5rem" };
            },
        }

        ];

        const parsedPaymentsMethod = this.prepareSetOfEntities(paymentMethods);
        const parsedFonti = this.prepareSetOfEntities(fonti);

        const handleChangePaymentMethod = (selectedPayment) => {

            const { rowSelected } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, { paymentMethod: selectedPayment.value })
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }

        const handleAnnullaPartecipazione = () => {

            const { rowSelected } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, { annullato: true })
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }


        const handleAttivaPartecipazione = () => {

            const { rowSelected } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, { annullato: false })
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }



        const handleEliminaPartecipanti = () => {

            const { rowSelected } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableRef.current.selectionContext.selected = [];
                deleteBulkPartecipations(partecipants)
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }




        const handleChangeFonti = (selectedFonte) => {

            const { rowSelected } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {
                tableRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, { bookedFrom: selectedFonte.value })
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }
        }

        const handleEditCustomer = (id, param, value) => {

            let params = {};
            params[param] = value;

            editCustomer(id, params)
                .then(() => getTour(tour.id))
                .then((res) => {
                    this.props.updateTour(res.data);
                    this.setState({ rowSelected: [] });
                })
                .catch((e) => console.log('error: ', e));
        }

        const handleEditPartecipation = (id, param, value) => {

            let params = {};
            params[param] = value;

            editPartecipation(id, params)
                .then(() => getTour(tour.id))
                .then((res) => {
                    this.props.updateTour(res.data);
                    this.setState({ rowSelected: [] });
                })
                .catch((e) => console.log('error: ', e));
        }

        return <ToolkitProvider
            keyField="partecipantsTable"
            // trClassName={"text-secondary no-min-height"}
            options={{
                sortName: "name",  //default sort column name
                sortOrder: "desc",  //default sort order
            }}
            data={partecipantsParsed}
            columns={columns}
            search={{
                searchFormatted: true
            }}
        >
            {
                props => (
                    <>
                        <div className="row mb-2" style={{ maxWidth: "100%" }}>
                            <div className="col-12 search-inline">
                                <SearchBar className="form-control-sm" {...props.searchProps} />
                                <Button className="btn btn-primary ml-2" onClick={() => this.setState({ secondaryInfomation: !secondaryInfomation })}>
                                    {secondaryInfomation ? "Info Guida" : "Info Admin"}
                                </Button>
                                <Button
                                    className="ml-2"
                                    onClick={handleAnnullaPartecipazione}
                                    variant="danger"
                                    disabled={!rowSelected.length}
                                >
                                    Annulla partecipazione
                                </Button>
                                <Button
                                    className="ml-2"
                                    onClick={handleAttivaPartecipazione}
                                    variant="success"
                                    disabled={!rowSelected.length}
                                >
                                    Attiva partecipazione
                                </Button>
                                <Button
                                    className="ml-2"
                                    onClick={handleEliminaPartecipanti}
                                    variant="warning"
                                    disabled={!rowSelected.length}
                                >
                                    Cancella partecipazione
                                </Button>
                            </div>
                        </div>
                        <div className="row mb-3" style={{ maxWidth: "100%" }}>
                            <div className="col-lg-3 col-md-4 col-sm-12">
                                <Select
                                    className="btn-xs no-min-height d-block basic-multi-select"
                                    key="selectPaymentMethod"
                                    styles={reactSelectStyles}
                                    placeholder="Scegli il pagamento"
                                    onChange={handleChangePaymentMethod}
                                    options={parsedPaymentsMethod}
                                    isDisabled={!rowSelected.length}
                                />
                            </div>
                            <div className="col-lg-3 col-md-4 col-sm-12">
                                <Select
                                    className="btn-xs no-min-height d-block basic-multi-select"
                                    key="selectFonte"
                                    styles={reactSelectStyles}
                                    placeholder="Scegli la piattaforma"
                                    onChange={handleChangeFonti}
                                    options={parsedFonti}
                                    isDisabled={!rowSelected.length}
                                />
                            </div>
                            <div className="col-lg-3 col-md-6 col-sm-12 justify-content-around">
                                <div className="d-flex w-100">
                                    <Select
                                        className="btn-xs no-min-height basic-multi-select"
                                        key="selectFilter"
                                        styles={reactSelectStyles}
                                        isMulti
                                        placeholder="Scegli cosa modificare"
                                        value={columnsToEdit}
                                        onChange={handleSelectColumnToEdit}
                                        options={editColumns}
                                        isDisabled={!rowSelected.length}
                                    />
                                    <button className="btn btn-sm btn-success mx-1 d-block"
                                        disabled={columnsToEdit.length === 0}
                                        onClick={(e) => handleEditColumns(e, true)}>
                                        Si
                                    </button>
                                    <button className="btn btn-sm btn-info mx-1 d-block"
                                        disabled={columnsToEdit.length === 0}
                                        onClick={(e) => handleEditColumns(e, false)}>
                                        No
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="kt-divider" />
                        <BootstrapTable {...props.baseProps} ref={tableRef} striped bordered keyField='id'
                            rowStyle={rowStyle}
                            selectRow={selectRow}
                            wrapperClasses="table-responsive"
                            cellEdit={cellEdit}
                            pagination={paginationFactory(optionsPagination)}
                        />
                    </>
                )
            }
        </ToolkitProvider>

    }

    renderCommunicationsPartecipant() {
        const { tour } = this.props;
        let tableCommRef = React.createRef();

        const { secondaryInfomation, commColToEdit, paymentMethods, fonti, rowSelected } = this.state;

        if (!tour || !tour.partecipants || !tour.partecipants.length) return "";

        let pageOptions = { pageCurrent: null, paginationSize: null };

        pageOptions.paginationSize = 10;

        const partecipantsParsed = this.renderRows(tour.partecipants);

        const sizePerPageList = [{
            text: '5', value: 5
        }, {
            text: '10', value: 10
        }, {
            text: '25', value: 25
        }, {
            text: '50', value: 50
        }, {
            text: '100', value: 100
        }, {
            text: '250', value: 250
        }, {
            text: '500', value: 500
        }];

        const sizePerPageOptionRenderer = ({
            text,
            page,
            onSizePerPageChange
        }) => (
            <li
                key={text}
                role="presentation"
                className="dropdown-item"
            >
                <a
                    href="#"
                    tabIndex="-1"
                    key={text}
                    role="menuitem"
                    data-page={page}
                    onMouseDown={(e) => {
                        onSizePerPageChange(page);
                        e.preventDefault();
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    {text}
                </a>
            </li>
        );



        const onPaginationChange = (sizePerPage, page) => {
            console.log('size per page: ', sizePerPage);
            console.log('page: ', page);
        }


        const optionsPagination = {
            sizePerPageOptionRenderer: sizePerPageOptionRenderer,
            sizePerPage: pageOptions.paginationSize,
            sizePerPageList: sizePerPageList,
            onSizePerPageChange: onPaginationChange,
            page: pageOptions.pageCurrent || 1
        };

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: false,
            onSelect: (row, isSelect, rowIndex, e) => {
                const { rowSelected } = this.state;
                if (isSelect) {
                    rowSelected.push(row.id);
                } else {
                    let idx = rowSelected.indexOf(row.id);
                    if (idx !== -1) {
                        rowSelected.splice(idx, 1);
                    }
                }

                this.setState({ rowSelected: rowSelected });
            },
            onSelectAll: (isSelect, rows, e) => {
                let { rowSelected } = this.state;
                if (isSelect) {
                    rows.map((row) => rowSelected.push(row.id));
                } else {
                    rowSelected = [];
                }

                this.setState({ rowSelected: rowSelected });
            }
        };

        const reactSelectStyles = {
            container: (base, state) => ({
                ...base,
                opacity: state.isDisabled ? ".5" : "1",
                backgroundColor: "transparent",
                zIndex: "999",
                minWidth: "200px"
            })
        };

        const rowStyle = (row, rowIndex) => {

            if (row.imprevisto && row.imprevisto != "") {
                return { backgroundColor: '#ffb822' }
            }
            if (row.annullato) {
                return { backgroundColor: '#F32013' }
            }

            // return { ... };
        };

        const handleSelectColumnToEdit = selectedColumns => {
            console.log('SELECTED Filter: ', selectedColumns);
            this.setState({ commColToEdit: selectedColumns });
        };

        const handleEditColumns = (event, value) => {

            event.preventDefault();
            const { commColToEdit, rowSelected } = this.state;

            const { } = this.state;

            let partecipants = [];
            let params = {};

            for (let col of commColToEdit) {
                params[col.value] = value;
            }

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {

                tableCommRef.current.selectionContext.selected = [];
                editBulkPartecipations(partecipants, params)
                    .then(() => getTour(tour.id))
                    .then((res) => {
                        this.props.updateTour(res.data);
                        this.setState({ rowSelected: [] });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }

        const editColumns = [{
            value: 'emailInfo',
            label: 'Email Info'
        }, {
            value: 'emailBefore',
            label: 'Email Pre-Tour'
        }, {
            value: 'emailPost',
            label: 'Email Post-Tour',
        }, {
            value: 'emailReview',
            label: 'Email Review'
        }]

        const basicStyle = { whiteSpace: "nowrap", overflowX: "hidden", textOverflow: "ellipsis" };

        const columns = [{
            dataField: 'name',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "7rem" };
            },
            text: 'Nome',
            sort: true,
        }, {
            dataField: 'surname',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "7rem" };
            },
            text: 'Cognome',
            sort: true,
        }, {
            dataField: 'customer.email',
            editable: true,
            text: 'Email',
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "12rem" };
            },
            sort: true,
        }, {
            dataField: 'emailInfo',
            editable: false,
            text: 'Email Info',
            headerStyle: () => {
                return { width: "6rem" };
            }
        }, {
            dataField: 'emailBefore',
            editable: false,
            text: 'Email Pre-Tour',
            headerStyle: () => {
                return { width: "6rem" };
            }
        }, {
            dataField: 'emailPost',
            editable: false,
            text: 'Email Post-Tour',
            headerStyle: () => {
                return { width: "7rem" };
            }
        }, {
            dataField: 'emailReview',
            editable: false,
            text: 'Email Review',
            headerStyle: () => {
                return { width: "6rem" };
            }
        }
        ];

        return <ToolkitProvider
            keyField="partecipantsTable"
            // trClassName={"text-secondary no-min-height"}
            options={{
                sortName: "name",  //default sort column name
                sortOrder: "desc",  //default sort order
            }}
            data={partecipantsParsed}
            columns={columns}
            search={{
                searchFormatted: true
            }}
        >
            {
                props => (
                    <div>
                        <div className="d-flex justify-content-end mb-2">
                            <Select
                                className="btn-xs no-min-height basic-multi-select"
                                key="selectFilter"
                                styles={reactSelectStyles}
                                isMulti
                                placeholder="Scegli cosa modificare"
                                value={commColToEdit}
                                onChange={handleSelectColumnToEdit}
                                options={editColumns}
                                isDisabled={!rowSelected.length}
                            />
                            <button className="btn btn-sm btn-success mx-1 d-block"
                                disabled={commColToEdit.length === 0}
                                onClick={(e) => handleEditColumns(e, true)}>
                                Si
                            </button>
                            <button className="btn btn-sm btn-info mx-1 d-block"
                                disabled={commColToEdit.length === 0}
                                onClick={(e) => handleEditColumns(e, false)}>
                                No
                            </button>
                        </div>
                        <BootstrapTable {...props.baseProps} ref={tableCommRef} striped bordered keyField='id'
                            rowStyle={rowStyle}
                            selectRow={selectRow}
                            pagination={paginationFactory(optionsPagination)}
                        />
                    </div>
                )
            }
        </ToolkitProvider>
    }

    renderRows(partecipants) {

        let toReturn = [];

        for (let partecipant of partecipants) {
            let newPartecipant = Object.assign({}, partecipant)
            newPartecipant.name = partecipant.customer ? partecipant.customer.name : "Sconosciuto";
            newPartecipant.surname = partecipant.customer ? partecipant.customer.surname : "Sconosciuto";
            newPartecipant.imprevisto = partecipant.imprevisto ? partecipant.imprevisto : "";
            newPartecipant.paid = partecipant.paid && partecipant.paid === true ? "Si" : "";
            newPartecipant.price = partecipant.price;
            newPartecipant.note = partecipant.note ? partecipant.note : "";
            newPartecipant.noteAdmin = partecipant.noteAdmin ? partecipant.noteAdmin : "";
            newPartecipant.paymentMethod = partecipant.paymentMethod;
            newPartecipant.emailInfo = partecipant.emailInfo && partecipant.emailInfo === true ? "Si" : "";
            newPartecipant.emailBefore = partecipant.emailBefore && partecipant.emailBefore === true ? "Si" : "";
            newPartecipant.emailPost = partecipant.emailPost && partecipant.emailPost === true ? "Si" : "";
            newPartecipant.emailReview = partecipant.emailReview && partecipant.emailReview === true ? "Si" : "";
            // newPartecipant.vegan = partecipant.customer && partecipant.customer.vegan === true ? "Si" : "";
            newPartecipant.vegetarian = partecipant.customer && partecipant.customer.vegetarian === true ? "Si" : "";
            newPartecipant.allergies = partecipant.customer ? partecipant.customer.allergies : "";
            newPartecipant.arrivato = partecipant.arrivato && partecipant.arrivato === true ? <div className="kt-notification__item-icon">
                <i className="flaticon2-check-mark kt-font-success" />
            </div> : "";

            toReturn.push(newPartecipant);
        }

        let sortedPartecipants = toReturn.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))

        return sortedPartecipants;
    }

    renderMoveToAnotherTour() {

        const { selectedNewTour, rowSelected, collapseNewTour } = this.state;

        const { tour } = this.props;

        const handleChangeNewTourSubmit = selectedFilter => {
            console.log(' New tour: ', selectedFilter);
            this.setState({ selectedNewTour: selectedFilter });
        };

        const handleMoveToAnotherTour = (event) => {
            event.preventDefault();

            const { rowSelected, selectedNewTour } = this.state;

            let partecipants = [];

            if (rowSelected) rowSelected.map(partecipantId => partecipants.push(partecipantId));

            if (partecipants.length) {


                editBulkPartecipations(partecipants, { tour: selectedNewTour.value })
                    .then(() => {
                        this.props.updateAllTour();
                        this.setState({ rowSelected: [], selectedNewTour: null });
                    })
                    .catch((e) => console.log('error: ', e));
            }

        }

        const getBetterLabel = (tour, prettyDate) => {
            let tourName, guideName;
            if (tour && tour.experience) {
                tourName = tour.experience.name;
            }
            if (tour && tour.guide && this.state.guides) {
                const guideSelected = this.state.guides.filter(g => g.id === tour.guide.id);
                guideName = guideSelected && guideSelected[0] && guideSelected[0].fullname || '';
            }
            return prettyDate + " " + (tourName ? " - " + tourName : '') + (guideName ? " - " + guideName : '');
        }

        return <div>

            <Collapse in={collapseNewTour}>
                <div id="collapse-new-tour-text"><Grid container className="ml-3 mb-5">
                    <Grid lg={12} className="mb-3 mt-5">
                        <Typography variant="p" className="secondary">
                            Sposta i partecipanti selezionati
                        </Typography>
                    </Grid>
                    <Grid lg={8}>
                        <Grid lg={10} item>
                            <Select
                                className="w-75 btn-xs no-min-height"
                                key="selectFilter"
                                styles={reactSelectStyles}
                                placeholder="Prossimi Tour disponibili"
                                value={selectedNewTour}
                                onChange={handleChangeNewTourSubmit}
                                options={this.state.allTours.map((tour) => { return { value: tour.id, label: getBetterLabel(tour, formatDate(moment(tour.date).format("DD/MM/YYYY HH:mm"))) } })}
                            />
                        </Grid>
                    </Grid>
                    <Grid lg={4}>
                        <Grid lg={10} className="ml-1" item>
                            <Button color="primary" disabled={rowSelected.length === 0 || !selectedNewTour} onClick={handleMoveToAnotherTour}>Sposta</Button>
                        </Grid>
                    </Grid>
                </Grid>

                </div>
            </Collapse>
        </div >
    }

    renderTourReport() {
        const { report } = this.state;

        if (!report) return "";

        return <div>
            {report.tappeReport ? report.tappeReport.map((tappaReport, index) => <div className="my-3">
                <div className="row">
                    <div className="col-lg-12">
                        <h5>Tappa {tappaReport.tappa ? tappaReport.tappa.name : (index + 1).toString()}</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-6">
                        <p>{tappaReport.report}</p>
                    </div>
                    <div className="col-lg-3">
                        <span>Problema: <span>{tappaReport.problema ? "Si" : "No"}</span></span>
                    </div>
                    <div className="col-lg-3">
                        <span>Importo aquisto clienti: <span>{tappaReport.importo}</span></span>
                    </div>
                </div>
            </div>) : ""}
        </div>
    }


}

export default injectIntl(
    connect(
    )(ToursPage)
);
