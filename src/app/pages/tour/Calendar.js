import { Calendar, momentLocalizer, Views } from 'react-big-calendar'
import React, { Component } from "react";
import { connect } from "react-redux";
import Spinner from 'react-bootstrap/Spinner';
import { injectIntl } from "react-intl";
import { getAllTours } from "../../crud/tour.crud";
import { getAllByRole } from "../../crud/user.crud";
import { getAllExperiences } from "../../crud/experience.crud";
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {
    Grid,
} from "@material-ui/core";
import DateFnsUtils from "@date-io/date-fns";
import { Modal, Button } from "react-bootstrap";
import TourModal from "./TourModal";
import * as auth from "../../store/ducks/auth.duck";
import moment from 'moment';
import Select from 'react-select';
import 'moment/locale/it';
import "react-big-calendar/lib/css/react-big-calendar.css";
import { getAllAssenze, getAssenzeByGuide, saveAssenza, editAssenza, deleteAssenza } from '../../crud/assenza.crud';
import { cloneDeep } from 'lodash';
import { selectColor, getLegenda } from '../../utils/calendar';

const reactSelectStyles = {
    container: (base, state) => ({
        ...base,
        opacity: state.isDisabled ? ".5" : "1",
        backgroundColor: "transparent",
        margin: "0 10px 10px 0",
        width: "35%",
        zIndex: "999",
    })
};

class CalendarPage extends Component {

    constructor(props) {
        super(props);

        this.updateTour = this.updateTour.bind(this);
        this.closeModal = this.closeModal.bind(this);

        this.myFormRef = React.createRef();

        this.state = {
            tours: [],
            experiences: [],
            assenze: [],
            filters: {},
            changeRange: false,
            showTourModal: false,
            showAssenzaModal: false,
            assenzaEndTime: null,
            defaultViewByUser: 'week',
            assenzaEndDay: null,
            assenzaPickerEndDay: null,
            assenzaStartTime: null,
            assenzaStartDay: null,
            assenzaMotivo: null,
            assenzaAllDay: null,
            selectedEvent: null,
            allGuides: [],
            selectedDate: null,
            selectedAssenza: null,
            mostraAssenze: true,
            toursLoaded: false,
            assenzeLoaded: false,
            assenzaMultiDays: false
        };
    }

    componentDidMount() {

        const tourSelectedDate = this.props.location && this.props.location.state || null;

        const { user } = this.props;
        const { defaultViewByUser } = this.state;
        let currentView = defaultViewByUser;
        let startOfView;
        let endOfView;

        if (user && user.email === 'paola.cardone5@hotmail.it') {
            currentView = 'month';
            startOfView = moment().startOf('month').toISOString();
            endOfView = moment().endOf('month').toISOString();

        } else {
            const dayToStartQuery = tourSelectedDate ? moment(tourSelectedDate) : moment();
            startOfView = dayToStartQuery.startOf('week').toISOString();
            endOfView = dayToStartQuery.endOf('week').toISOString();
        }

        const params = { from: startOfView, to: endOfView };

        getAllTours(params)
            .then((res) => {
                if (res && res.data) {
                    if (tourSelectedDate) {
                        this.setState({ tours: res.data, selectedDate: tourSelectedDate, defaultViewByUser: currentView, filters: params })
                    } else {
                        this.setState({ tours: res.data })
                    }
                };
            })
            .catch((error) => {
                console.error("[Calendar] - getAllTours: ", error);
            })
            .finally(() => {
                this.setState({ toursLoaded: true });
            })

        getAllExperiences()
            .then((res) => {
                if (res && res.data) {
                    this.setState({
                        experiences: res.data
                    });
                }
            })
            .catch((error) => {
                console.error("[NewTour] - getAllExperiences: ", error);
            })


        if (user && user.role === "ADMIN") {
            getAllByRole('GUIDE')
                .then((res) => {
                    this.setState({ allGuides: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAllByRole-GUIDE: ', error);
                })


            getAllAssenze(params)
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAllAssenze: ', error);
                })
                .finally(() => {
                    this.setState({ assenzeLoaded: true });
                })
        } else if (user && user.role === "GUIDE") {

            getAssenzeByGuide(user.id, { from: params.from, to: params.to })
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAssenzeByGude: ', error);
                })
                .finally(() => {
                    this.setState({ assenzeLoaded: true });
                })
        }



    }

    componentWillUnmount() {


    }

    getToursAfterChangeRangeData(params) {
        let filters = {};
        const { user } = this.props;

        console.log('params', params);

        this.setState({ changeRange: true })

        if (params.start && params.end) {
            filters = {
                from: params.start,
                to: params.end
            }
        }

        if (Array.isArray(params)) {

            if (params.length === 1) {
                const today = moment(params[0]);
                filters = {
                    from: today.startOf('week').toISOString(),
                    to: today.endOf('week').toISOString()
                }
            } else {
                filters = {
                    from: moment(params[0]).startOf('day').toISOString(),
                    to: moment(params[params.length - 1]).endOf('day').toISOString()
                }
            }
        }

        getAllTours(filters)
            .then((res) => {
                if (res && res.data) this.setState({ tours: res.data, filters: filters || {}, changeRange: false });
            })
            .catch((error) => {
                console.error("[Calendar] - getToursAfterChangeRangeData: ", error);
            })

        if (user && user.role === "ADMIN") {

            getAllAssenze(filters)
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data, filters: filters });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAllAssenze: ', error);
                })
                .finally(() => {
                    this.setState({ assenzeLoaded: true });
                })
        } else if (user && user.role === "GUIDE") {

            getAssenzeByGuide(user.id, { from: filters.from, to: filters.to, filters: filters })
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAssenzeByGude: ', error);
                })
                .finally(() => {
                    this.setState({ assenzeLoaded: true });
                })
        }



    }


    getToursByFilter(params) {

        getAllTours(params)
            .then((res) => {
                if (res && res.data) this.setState({ tours: res.data, filters: params || {} });
            })
            .catch((error) => {
                console.error("[Calendar] - getToursByFilter: ", error);
            })

    }

    updateAllTour = () => {

        const { filters } = this.state;
        getAllTours(filters)
            .then((res) => {
                if (res && res.data) {
                    for (let tour of res.data) {
                        if (tour.id === this.state.selectedEvent.id) {
                            this.setState({ tours: res.data, selectedEvent: tour });
                            break;
                        }
                    }
                };
            })
            .catch((error) => {
                console.error("[Calendar] - getAllTours: ", error);
            })
    }

    updateAllAssenze = () => {

        const { user } = this.props;
        const { filters } = this.state;

        if (user && user.role === "ADMIN") {
            getAllAssenze(filters)
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAllAssenze: ', error);
                })
        } else if (user && user.role === "GUIDE") {

            getAssenzeByGuide(user.id, filters)
                .then((res) => {
                    if (res && res.data) this.setState({ assenze: res.data });
                })
                .catch((error) => {
                    console.error('[Calendar] - getAssenzeByGude: ', error);
                })
        }
    }

    updateTour = (data) => {

        const { tours } = this.state;

        for (let i = 0; i <= tours.length - 1; i++) {
            if (tours[i].id === data.id) {
                tours[i] = cloneDeep(data);
                break;
            }
        }

        this.setState({ tours: tours, selectedEvent: data })
    }

    closeModal = () => {
        this.setState({ showTourModal: false, selectedEvent: null })
    }

    prepareGuideFilter(allGuides, selectedFilters) {
        let toReturn = [];
        let toAssignSelected = false;
        for (let guide of allGuides) {
            if (selectedFilters) {
                let arrayOfId = selectedFilters.split(',');
                for (let selected of arrayOfId) {
                    if (parseInt(selected) === guide.id) {
                        toReturn.push({ value: guide.id, label: guide.fullname });
                    }
                    if (selected === "toAssign") toAssignSelected = true;
                }
            } else {
                toReturn.push({ value: guide.id, label: guide.fullname });
            }
        }

        if (selectedFilters) {
            if (toAssignSelected) toReturn.push({ value: "toAssign", label: "Da assegnare" });
            return toReturn;
        }

        toReturn.push({ value: "toAssign", label: "Da assegnare" });
        toReturn.push({ value: null, label: "Tutte" });

        return toReturn;
    }

    prepareExperienceFilter(experiences, selectedFilters) {
        let toReturn = [];
        for (let exp of experiences) {
            if (selectedFilters) {
                let arrayOfId = selectedFilters.split(',');
                for (let selected of arrayOfId) {
                    if (parseInt(selected) === exp.id) {
                        toReturn.push({ value: exp.id, label: exp.name });
                    }
                }
            } else {
                toReturn.push({ value: exp.id, label: exp.name });
            }

        }

        if (selectedFilters) return toReturn;

        toReturn.push({ value: null, label: "Tutte" });

        return toReturn;
    }

    handleChangeExpFilterSubmit = selectedFilters => {
        console.log('SELECTED Filter: ', selectedFilters);
        let { filters } = this.state;
        if (selectedFilters && selectedFilters.length) {
            let arrayExperiences = [];
            selectedFilters.map((epxObj) => arrayExperiences.push(epxObj.value));
            filters.experience = arrayExperiences.join(',');
        } else {
            filters.experience = '';
        }
        this.getToursByFilter(filters);
    };

    handleChangeGuideFilterSubmit = selectedFilters => {
        console.log('SELECTED Filter: ', selectedFilters);
        let { filters } = this.state;
        if (selectedFilters && selectedFilters.length) {
            let arrayGuides = [];
            selectedFilters.map((guideObj) => arrayGuides.push(guideObj.value));
            filters.guide = arrayGuides.join(',');
        } else {
            filters.guide = '';
        }
        this.getToursByFilter(filters);
    };

    render() {
        const { user } = this.props;

        const getEventName = (tour) => {

            let name = '';
            name += tour.experience.name;
            if (tour.privato) {
                name += ' - PVT '
            }
            if (tour.guide) {
                name += " - ";
                name += tour.guide.fullname;
            }
            if (tour.secondGuide) {
                name += " & " + tour.secondGuide.fullname;
            }

            const arrayFiltered = tour.partecipants ? tour.partecipants.filter((p) => !p.annullato) : [];

            if (arrayFiltered.length) {
                name += " -- (" + arrayFiltered.length + ")"; 
            }

            return name;

        }

        const { tours, assenze, allGuides, mostraAssenze, filters, experiences, toursLoaded, assenzeLoaded, defaultViewByUser, changeRange } = this.state;
        const localizer = momentLocalizer(moment)


        for (let tour of tours) {
            tour.title = getEventName(tour);
            tour.startTime = new Date(tour.startTime)
            tour.endTime = new Date(tour.endTime)
        }

        let eventi;
        if (mostraAssenze) {
            for (let assenza of assenze) {
                assenza.title = assenza.guide ? assenza.guide.fullname : "Assenza guida";
                assenza.startTime = new Date(assenza.startTime)
                assenza.endTime = new Date(assenza.endTime)
                assenza.isAssenza = true;
            }
            eventi = tours.concat(assenze);
        } else {
            eventi = tours;
        }

        let selectedGuideFilter = filters.guide ? this.prepareGuideFilter(allGuides, filters.guide) : null;
        let selectedExperienceFilter = filters.experience ? this.prepareExperienceFilter(experiences, filters.experience) : null;

        const GuideOptions = this.prepareGuideFilter(allGuides);
        const ExperienceOptions = this.prepareExperienceFilter(experiences);

        return (
            <div>
                <div className="row">
                    {user.role === "ADMIN" ? <div className="col-8">
                        <div className="d-flex mb-2">
                            <Select
                                className="btn-xs no-min-height basic-multi-select"
                                key="selectFilter"
                                styles={reactSelectStyles}
                                isMulti
                                placeholder="Filtra per guida"
                                value={selectedGuideFilter}
                                onChange={this.handleChangeGuideFilterSubmit}
                                options={GuideOptions}
                            />
                            <Select
                                className="btn-xs no-min-height basic-multi-select"
                                key="selectSort"
                                isMulti
                                styles={reactSelectStyles}
                                placeholder="Filtra per experience"
                                value={selectedExperienceFilter}
                                onChange={this.handleChangeExpFilterSubmit}
                                options={ExperienceOptions}
                            />
                            <Button variant="brand" className="buttonAssenze" size="sm"
                                style={{ height: "38px", backgroundColor: "#606060", borderColor: "#606060" }}
                                onClick={() => this.setState({
                                    mostraAssenze: !mostraAssenze
                                })}>{mostraAssenze ? 'Nascondi assenze' : "Mostra assenze"}</Button>
                            {changeRange && <Spinner className="ml-2" animation="border" variant="danger" />}
                        </div>
                    </div> : ""}
                </div>
                {toursLoaded && assenzeLoaded ? <Calendar
                    localizer={localizer}
                    selectable={true}
                    date={this.state.selectedDate}
                    onNavigate={date => {
                        this.setState({ selectedDate: date });
                    }}
                    defaultView={defaultViewByUser}
                    min={
                        new Date(
                            moment().year(),
                            moment().month(),
                            moment().date(),
                            9
                        )
                    }
                    events={eventi || []}
                    startAccessor="startTime"
                    endAccessor="endTime"
                    onRangeChange={(data) => this.getToursAfterChangeRangeData(data)}
                    onSelectEvent={(event) => {
                        if (event.isAssenza) {
                            this.setState({
                                assenzaStartTime: event.startTime,
                                assenzaStartDay: event.startTime,
                                assenzaEndTime: event.endTime,
                                assenzaEndDay: event.endTime,
                                assenzaMotivo: event.motivo,
                                assenzaMultiDays: event.multiDays,
                                assenzaPickerEndDay: event.endTime,
                                assenzaAllDay: event.allDay,
                                showAssenzaModal: true,
                                selectedAssenza: event
                            })
                        } else if (user.role === "ADMIN") this.setState({ selectedEvent: event, showTourModal: true })
                    }}
                    onSelectSlot={(event) => {
                        if (user.role === "GUIDE") {
                            this.setState({
                                assenzaStartTime: event.start,
                                assenzaStartDay: event.start,
                                assenzaEndTime: event.end,
                                assenzaEndDay: event.end,
                                showAssenzaModal: true,
                                selectedAssenza: null
                            })
                        } else if (user.role === "ADMIN") {
                            this.props.history.push("/tour/new", event.start);
                        }
                    }}
                    style={{ height: 800 }}
                    eventPropGetter={event => {
                        const colorObj = selectColor(event);
                        return { style: colorObj };
                    }}
                /> : <div className="d-flex justify-content-center w-100" style={{ marginTop: 200 }}>
                    <Spinner animation="border" variant="danger" />
                </div>}
                {this.renderTourModal()}
                {this.renderAssenzaModal()}
            </div>
        )


    }

    renderTourModal() {

        const { showTourModal, selectedEvent, tours } = this.state;

        return <TourModal
            tour={selectedEvent}
            showModal={showTourModal}
            closeModal={this.closeModal}
            updateAllTour={this.updateAllTour}
            updateTour={this.updateTour} />

    }

    renderAssenzaModal() {

        const { showAssenzaModal, assenzaMotivo, assenzaAllDay, selectedAssenza, assenzaMultiDays } = this.state;

        const { user } = this.props;

        const handleClose = () => {
            this.setState({
                showAssenzaModal: false, assenzaEndTime: null, assenzaPickerEndDay: null,
                assenzaAllDay: false, assenzaMultiDays: false, assenzaStartTime: null, selectedAssenza: null
            });
        }

        const handleSave = (e) => {

            const { assenzaStartTime,
                assenzaEndTime,
                assenzaStartDay,
                assenzaPickerEndDay,
                assenzaEndDay } = this.state;

            e.preventDefault();

            let data = this.myFormRef.current;

            let newAssenza = {};

            newAssenza.guide = this.props.user.id;
            newAssenza.motivo = data.motivo ? data.motivo.value : "";
            newAssenza.allDay = data.allDayCheck ? data.allDayCheck.checked : false;
            newAssenza.multiDays = data.multiDaysCheck ? data.multiDaysCheck.checked : false;
            let finalEndOfAssenza = null;

            if (newAssenza.multiDays && assenzaPickerEndDay) {
                finalEndOfAssenza = assenzaPickerEndDay;
            } else if (newAssenza.allDay) {
                finalEndOfAssenza = assenzaStartDay;
            } else {
                finalEndOfAssenza = assenzaEndDay;
            }

            let tourDayMomentForStart = assenzaStartDay ? moment(assenzaStartDay) : null;
            let tourDayMomentForEnd = finalEndOfAssenza ? moment(finalEndOfAssenza) : null;
            let assenzaStartTimeMoment = assenzaStartTime ? moment(assenzaStartTime, 'HH:mm') : null;
            let assenzaEndTimeMoment = assenzaEndTime ? moment(assenzaEndTime, 'HH:mm') : null;

            if (assenzaStartTimeMoment && tourDayMomentForStart) {
                newAssenza.assenzaStartTime = tourDayMomentForStart.set({
                    hour: assenzaStartTimeMoment.get('hour'),
                    minute: assenzaStartTimeMoment.get('minute')
                })
            }

            if (assenzaEndTimeMoment && tourDayMomentForEnd) {
                newAssenza.assenzaEndTime = tourDayMomentForEnd.set({
                    hour: assenzaEndTimeMoment.get('hour'),
                    minute: assenzaEndTimeMoment.get('minute')
                })
            }

            if (selectedAssenza) {
                editAssenza(selectedAssenza.id, newAssenza)
                    .then(() => this.updateAllAssenze())
                    .catch((error) => {
                        console.error('[Calendar] - editAssenza: ', error);
                    })
            } else {
                saveAssenza(newAssenza)
                    .then(() => this.updateAllAssenze())
                    .catch((error) => {
                        console.error('[Calendar] - saveAssenza: ', error);
                    })
            }


            this.setState({
                showAssenzaModal: false,
                selectedAssenza: null,
                assenzaEndTime: null,
                assenzaEndDay: null,
                assenzaStartTime: null,
                assenzaStartDay: null,
                assenzaMotivo: null,
                assenzaMultiDays: null,
                assenzaPickerEndDay: null,
                assenzaAllDay: null
            });
        };

        const handleDeleteAssenza = (e) => {
            const { selectedAssenza } = this.state;

            deleteAssenza(selectedAssenza.id)
                .then(() => this.updateAllAssenze())
                .catch((error) => {
                    console.error('[Calendar] - deleteAssenza: ', error);
                })

            this.setState({
                showAssenzaModal: false,
                selectedAssenza: null,
                assenzaEndTime: null,
                assenzaEndDay: null,
                assenzaStartTime: null,
                assenzaStartDay: null,
                assenzaMotivo: null,
                assenzaAllDay: null
            });
        }

        return <Modal key="modal_result" show={showAssenzaModal} size="md" onHide={handleClose}>
            <Modal.Header closeButton>
                <h6 className="test-danger">Imposta orario assenza.</h6>
            </Modal.Header>
            <form id="result_form" ref={this.myFormRef}>
                <Modal.Body className="rounded-lg bg-white">
                    <div className="form-group">
                        {/* <!-- Input --> */}
                        <div className="js-form-message mb-3">
                            <label className="form-label">Motivo ed eventuali note</label>
                            <div className="input-group">
                                <textarea className="form-control" rows="3" name="text" id={"motivo"} placeholder="Aggiungi il motivo e varie ed eventuali" aria-label="Note e varie"
                                    defaultValue={assenzaMotivo}
                                    data-msg="Please enter a property description."
                                    data-error-class="u-has-error"
                                    data-success-class="u-has-success"></textarea>
                            </div>
                        </div>
                    </div>
                    <div className='row ml-2'>
                        <div className="form-group">
                            <div className="js-focus-state">
                                <div className="input-group ml-1">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox"
                                            disabled={assenzaMultiDays}
                                            onChange={() => this.setState({ assenzaAllDay: !assenzaAllDay })}
                                            defaultChecked={assenzaAllDay} value="" id={"allDayCheck"} />
                                        <label className="form-check-label text-muted" htmlFor={"allDayCheck"}>
                                            Tutto il giorno ?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="form-group ml-5">
                            <div className="js-focus-state">
                                <div className="input-group ml-1">
                                    <div className="form-check">
                                        <input className="form-check-input" type="checkbox"
                                            disabled={assenzaAllDay}
                                            onChange={() => this.setState({ assenzaMultiDays: !assenzaMultiDays })}
                                            defaultChecked={assenzaMultiDays} value="" id={"multiDaysCheck"} />
                                        <label className="form-check-label text-muted" htmlFor={"multiDaysCheck"}>
                                            Per più giorni ?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!assenzaAllDay && this.renderTimePickers()}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="dialog" size="sm" onClick={handleClose}>Chiudi</Button>
                    {selectedAssenza && <Button variant="link" size="sm" onClick={handleDeleteAssenza}>Elimina</Button>}
                    {user.role !== "ADMIN" && <Button variant="danger" type="submit" onClick={handleSave} size="sm">Salva assenza</Button>}
                </Modal.Footer>
            </form>

        </Modal>
    }


    renderTimePickers() {

        const { assenzaStartTime, assenzaEndTime, assenzaMultiDays, assenzaAllDay, assenzaPickerEndDay } = this.state;

        const handleSTDateChange = (date) => {
            this.setState({ assenzaStartTime: date });
        };

        const handleETDateChange = (date) => {
            this.setState({ assenzaEndTime: date });
        };

        const handleFromDayChange = (day) => {

            this.setState({ assenzaPickerEndDay: day });

        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            {!assenzaAllDay && !assenzaMultiDays && <Grid container className="ml-3 mb-5">
                <Grid lg={6}>
                    <Grid lg={10} item>
                        <label className="form-label" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="startTimePicker"
                                ampm={false}
                                value={assenzaStartTime}
                                onChange={handleSTDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>

                    </Grid>
                </Grid>
                <Grid lg={6}>
                    <Grid lg={10} className="ml-1" item>
                        <label className="form-label" htmlFor="endTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="endTimePicker"
                                ampm={false}
                                value={assenzaEndTime}
                                onChange={handleETDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>}
            <Grid container className="ml-3 mb-5">
                {assenzaMultiDays && !assenzaAllDay && <Grid lg={6} item>
                    <label className="form-label" forHtml="fromTimePicker"><span className="fas fa-clock"></span> Fino al giorno </label>
                    <div className="input-group">
                        <KeyboardDatePicker
                            disableToolbar
                            variant="dialog"
                            format="dd/MM/yyyy"
                            margin="normal"
                            id="from-picker-dialog"
                            label="Date picker"
                            value={assenzaPickerEndDay}
                            onChange={handleFromDayChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                        />
                    </div>
                </Grid>}
            </Grid>
        </MuiPickersUtilsProvider>
    }


}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});
export default injectIntl(connect(mapStateToProps, auth.actions)(CalendarPage));