import React, { Component } from "react";
import { connect } from "react-redux";
import { FormattedMessage, injectIntl } from "react-intl";
import { Button, Modal } from "react-bootstrap";
import * as auth from "../../store/ducks/auth.duck";
import { getAllTours } from "../../crud/tour.crud";
import { saveReport } from "../../crud/tourReport.crud";
import { formatDate } from "../../utils/date";
import { editPartecipation } from "../../crud/partecipation.crud";
import { getReportByTour } from "../../crud/tourReport.crud";
import moment from 'moment';

class PastTours extends Component {


    constructor(props) {
        super(props);

        this.myFormRef = React.createRef();

        this.state = {
            tours: null,
            selectedTour: null,
            selectedReport: null,
            showEditPartecipantModal: false,
            showWriteResultModal: false
        };

    }

    componentDidMount() {

        this.mainFetch();
    }

    mainFetch() {
        const { user } = this.props;

        const dataTo = moment().toISOString();

        const dataFrom = moment().subtract(6, 'months').startOf('day').toISOString();

        if (user) {
            getAllTours({ guide: user.id, from: dataFrom, to: dataTo, rifiutato: false })
                .then((res) => {
                    if (res && res.data) {
                        let sortedTOurs = res.data.sort((a, b) => (new Date(a.date).getTime() - new Date(b.date).getTime()));
                        this.setState({ tours: sortedTOurs });
                    }
                })
                .catch((error) => {
                    console.error("[Tours] - getAllTours: ", error);
                })
        }

    }

    componentWillUnmount() {

    }

    onSelectTour(event, tour) {
        event.preventDefault();

        getReportByTour(tour.id)
            .then((res) => {
                this.setState({ selectedReport: res?.data || {} })
            }
            )
            .catch((error) => {
                console.error("[TourModal] - getReportByTour: ", error);
            });


        this.setState({ selectedTour: tour, showWriteResultModal: true })
    }


    getTourPartecipantsFiltered = (t) => {
        const arrayFiltered = t.partecipants && t.partecipants.length ? t.partecipants.filter((p) => !p.annullato) : [];
        return arrayFiltered.length;
    }


    render() {

        const { tours } = this.state;



        return (
            <div className="main container px-4">
                {!tours || !tours.length ? <div className="d-flex flex-column">
                    <h4 className="font-weight-bolder kt-font-brand">Nessun tour da revisionare</h4>
                    <p>Sembra che non ci sia nessun tour passato a te assegnato, a cui tu debba aggiungere note e conclusioni.</p>
                </div> :
                    <div className="row">
                        <div className="card w-100 my-3">
                            <div className="card-header d-flex justify-content-between">
                                <h5 className="card-title align-items-start flex-column">
                                    <span className="card-label font-weight-bolder kt-font-brand">
                                        Tour passati
                                    </span>
                                </h5>
                                <div>
                                    <span className="h4 font-weight-bold kt-font-google">{tours.length}</span>
                                    <span className="fas fa-users fa-2x kt-font-google mx-2" />
                                </div>
                            </div>
                            <div className="card-body pt-3 pb-0">
                                <div className="table-responsive">
                                    <table className="table table-borderless table-striped table-vertical-center">
                                        <thead>
                                            <tr>
                                                <td>Data</td>
                                                <td>Experience</td>
                                                <td>Partecipanti</td>
                                                <td>Note</td>
                                                <td>Archiviato</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {tours.map((tour, idx) =>
                                                <tr key={idx}>
                                                    <td>{formatDate(moment(tour.date).format())}</td>
                                                    <td>{tour.experience.name}</td>
                                                    <td><span className="btn btn-icon btn-xs btn-info" onClick={() => this.setState({ showEditPartecipantModal: true, selectedTour: tour })}>{this.getTourPartecipantsFiltered(tour)}</span></td>
                                                    <td>{tour.results && tour.results !== "" ? "Inserite" :
                                                        <button className=" border-0 badge badge-lg badge-primary badge-inline"
                                                            onClick={(e) => this.onSelectTour(e, tour)}>
                                                            Inserisci note
                                                        </button>
                                                    }</td>
                                                    <td>{tour.archived ? "Si" : "No"}</td>
                                                </tr>)}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>}

                {this.renderWriteResultsModal()}
                {this.renderEditPastPartecipantsModal()}
            </div>

        )
    }

    renderEditPastPartecipantsModal() {

        const { showEditPartecipantModal, selectedTour } = this.state;

        const handlePartecipantArrived = (event, partecipant) => {
            if (event && event.target) {

                editPartecipation(partecipant.id, { "arrivato": event.target.checked })
                    .then(() => {
                        let tourCopied = Object.assign({}, selectedTour);
                        for (let part of tourCopied.partecipants) {
                            if (part.id === partecipant.id) {
                                part.arrivato = !partecipant.arrivato;
                                break;
                            }
                        }

                        this.setState({ showEditPartecipantModal: true, selectedTour: tourCopied })
                    })
                    .catch((err) => console.log('Error in arrivato change ', err))

            }
        }

        const handleClosePartecipant = () => {
            this.setState({ showEditPartecipantModal: false, selectedTour: null });
        }

        if (selectedTour && selectedTour.partecipants && selectedTour.partecipants.length) {

            return <Modal key="modal_partecipants" show={showEditPartecipantModal} size="lg" onHide={handleClosePartecipant}>
                <Modal.Header closeButton>
                    <h6 className="test-danger">Aggiungi info partecipanti.</h6>
                </Modal.Header>
                <Modal.Body className="rounded-lg bg-white">

                    {selectedTour.partecipants.length ? <div className="card w-100 my-3">
                        <div className="card-header d-flex justify-content-between">
                            <h4 className="card-title align-items-start flex-column">
                                <span className="card-label font-weight-bolder kt-font-brand">
                                    Partecipanti
                                </span>
                            </h4>
                            <div>
                                <span className="h4 font-weight-bold kt-font-google">{selectedTour.partecipants ? selectedTour.partecipants.length : ""}</span>
                                <span className="fas fa-users fa-2x kt-font-google mx-2" />
                            </div>
                        </div>
                        <div className="card-body pt-3 pb-0">
                            <div className="table-responsive">
                                <table className="table table-borderless table-striped table-vertical-center">
                                    <thead>
                                        <tr>
                                            <td>Nome</td>
                                            <td>Ritiro soldi</td>
                                            {/* <td>Vegano</td> */}
                                            <td>Vegetariano</td>
                                            <td>Note</td>
                                            <td>Provenienza</td>
                                            <td>Arrivato</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {selectedTour.partecipants && selectedTour.partecipants.map(p =>
                                            <tr>
                                                <td>{p.customer && p.customer.name} {p.customer && p.customer.surname}</td>
                                                <td>{p.paymentMethod && p.paymentMethod.type && p.paymentMethod.type.toLowerCase() === "contanti" && !p.paid ? p.price : "No"}</td>
                                                {/* <td>{p.vegan ? "Si" : "No"}</td> */}
                                                <td>{p.vegetarian ? "Si" : "No"}</td>
                                                <td>{p.note}</td>
                                                <td>{p.customer && p.customer.provenienza}</td>
                                                <td className="text-center">
                                                    <input id="checkArrivato" type="checkbox" defaultChecked={false} checked={p.arrivato}
                                                        onChange={(event) => handlePartecipantArrived(event, p)} />
                                                </td>
                                            </tr>)}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> : ""}

                </Modal.Body>
            </Modal>
        }

        return ""

    }

    renderWriteResultsModal() {


        const { showWriteResultModal, selectedTour, selectedReport } = this.state;

        const handleClose = () => {
            this.setState({ showWriteResultModal: false, selectedTour: null, selectedReport: null });
        }

        const handleSave = (e) => {

            e.preventDefault();

            const { selectedTour } = this.state;

            let data = this.myFormRef.current;

            let dataBody = {}

            if (selectedTour && selectedTour.experience && selectedTour.experience.tappe) {

                for (let tappa of selectedTour.experience.tappe) {

                    let noteCurrent = data["note_" + tappa.id];
                    let importoCurrent = data["importoClienti_" + tappa.id];
                    let problemaCurrent = data["problemaCheck_" + tappa.id];


                    if (noteCurrent && importoCurrent && problemaCurrent) {
                        dataBody[tappa.id] = {
                            note: noteCurrent.value ? noteCurrent.value : "",
                            importoClienti: importoCurrent.value ? importoCurrent.value : 0,
                            problema: problemaCurrent.checked,
                            tappaId: tappa.id
                        }

                    }

                }

                saveReport({
                    tourId: selectedTour.id,
                    data: dataBody
                })
                    .then(() => this.setState({ showWriteResultModal: false, selectedTour: null }))
                    .catch((error) => console.log(error));


            } else {
                this.setState({ showWriteResultModal: false, selectedTour: null });
            }

            this.setState({
                selectedTappe: [],
                selectedReport: null,
                selectedTour: null,
                orderTappe: {}
            });



        };

        const getExistentText = (report, tappa) => {
            if (report && report.tappeReport && report.tappeReport.length) {
                const currentTappa = report.tappeReport.find(rep => rep.tappa?.id === tappa.id);

                if (currentTappa) {
                    return currentTappa.report;
                }
                return ''
            }
            return '';
        }

        const getExistentImporto = (report, tappa) => {
            if (report && report.tappeReport && report.tappeReport.length) {
                const currentTappa = report.tappeReport.find(rep => rep.tappa?.id === tappa.id);

                if (currentTappa) {
                    return currentTappa.importo;
                }
                return ''
            }
            return '';
        }


        const getExistentProblema = (report, tappa) => {
            if (report && report.tappeReport && report.tappeReport.length) {
                const currentTappa = report.tappeReport.find(rep => rep.tappa?.id === tappa.id);

                if (currentTappa) {
                    return currentTappa.problema;
                }
                return false
            }
            return false;
        }

        if (!selectedTour) return "";


        return <Modal key="modal_result" show={showWriteResultModal} size="md" onHide={handleClose}>
            <Modal.Header closeButton>
                <h6 className="test-danger">Aggiungi considerazioni finali.</h6>
            </Modal.Header>
            <form id="result_form" ref={this.myFormRef}>
                <Modal.Body className="rounded-lg bg-white">

                    {selectedTour.experience && selectedTour.experience.tappe && selectedReport ?
                        selectedTour.experience.tappe.map((tappa, idx) => {
                            return <>
                                <div className="form-group" key={idx}>
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <h5 className="kt-font-brand">{tappa.name}
                                        </h5>
                                        <label className="form-label">Note ed eventuali</label>
                                        <div className="input-group">
                                            <textarea className="form-control" rows="2" name="text" id={"note_" + tappa.id} placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                                data-msg="Please enter a property description."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success">
                                                {getExistentText(selectedReport, tappa)}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="js-form-message js-focus-state">
                                        <div className="input-group">
                                            <div className="input-group-prepend">
                                                <span className="input-group-text" id="importoLabel">
                                                    <span className="fas fa-file-signature"></span>
                                                </span>
                                            </div>
                                            <input type="text" className="form-control" name={"importoClienti" + tappa.id} id={"importoClienti_" + tappa.id}
                                                placeholder="Importo acquisto clienti"
                                                defaultValue={getExistentImporto(selectedReport, tappa)}
                                                aria-label="Importo acquisto clienti"
                                                aria-describedby="importoLabel" required
                                                data-msg=""
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success" />
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="js-focus-state">
                                        <div className="input-group">
                                            <div className="form-check">
                                                <input className="form-check-input" type="checkbox" defaultChecked={getExistentProblema(selectedReport, tappa)}
                                                    value="" id={"problemaCheck_" + tappa.id} />
                                                <label className="form-check-label text-muted" htmlFor={"problemaCheck_" + tappa.id}>
                                                    C'è stato un problema?
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </>
                        }) : "Stiamo caricando i dati"}

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="link text-dark" size="sm" onClick={handleClose}>Chiudi</Button>
                    <Button variant="danger" type="submit" onClick={handleSave} size="sm">Salva note</Button>
                </Modal.Footer>
            </form>

        </Modal>

    }

}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});
export default injectIntl(connect(mapStateToProps, auth.actions)(PastTours));