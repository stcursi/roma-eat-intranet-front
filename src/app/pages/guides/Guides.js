import React, { Component } from "react";
import { getAllByRole } from "../../crud/user.crud";
import { Card, Button, Modal } from "react-bootstrap";
import { toAbsoluteUrl } from "../../utils/utils";
import { Formik } from "formik";
import { connect } from "react-redux";
import { FormattedMessage, injectIntl } from "react-intl";
import { updateUser, deleteUser } from "../../crud/user.crud";
import { TextField } from "@material-ui/core";
import { register } from "../../crud/auth.crud";

class Guides extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allGuides: [],
            showNewModal: false,
            showEditInfoModal: false,
            selectedGuide: null
        };
    }

    componentDidMount() {

        getAllByRole('GUIDE')
            .then((res) => {
                this.setState({ allGuides: res.data });
            })
            .catch((error) => {
                console.error('[getAllByRole-GUIDE] - ', error);
            })

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps() {

        if (this.props && this.props.history && this.props.history.location && this.props.history.location.state) {
            this.setState({ showNewModal: this.props.history.location.state.showNewModal || false })
        }
    }

    updateGuideRender(guideUpdated) {

        const { allGuides } = this.state;


        const updatedGuides = allGuides.map(guide => {
            if (guide.id === guideUpdated.id) {
                return guideUpdated;
            }
            return guide;
        });

        this.setState({ allGuides: updatedGuides });
    }

    deleteGuide(id) {

        deleteUser(id)
            .then(() => {
                getAllByRole('GUIDE')
                    .then((res) => {
                        this.setState({ allGuides: res.data });
                    })
                    .catch((error) => {
                        console.error('[getAllByRole-GUIDE] - ', error);
                    })
            })
    }

    render() {


        return (
            <div className="container-fluid">
                <div className="row">
                    {this.renderPage()}
                </div>
                {this.renderNewModal()}
                {this.renderEditModal()}
            </div>
        )
    }

    renderPage() {
        const { allGuides } = this.state;

        if (allGuides && allGuides.length) {
            return allGuides.map((guide, index) => this.renderGuide(guide, index))
        } else {
            return (<div className="col-sm-12 col-md-12 col-lg-6">
                "NON CI SONO ANCORA GUIDE TODO PAGINA VUOTA"
            </div>)
        }

    }

    renderGuide(guide, idx) {

        if (guide) {
            return (<div className="col-sm-12 col-md-6 col-lg-3 mb-3">
                <Card key={"g_" + idx} className="text-center" style={{ width: '18rem' }}>
                    <Card.Header className="text-center">
                        <Card.Img style={{ width: '80px', height: '80px' }} variant="top" className="rounded-circle" src={toAbsoluteUrl(guide.profilePicture)} />
                        <button type="button" className="btn btn-pill btn-icon btn-light transition-3d-hover"
                            style={{
                                position: "absolute",
                                backgroundColor: "transparent",
                                borderColor: "transparent",
                                top: 0,
                                right: 0
                            }}
                            onClick={() => this.deleteGuide(guide.id)}>
                            <span className="p-0 fa fa-trash kt-font-info btn-icon__inner"></span>
                        </button>
                    </Card.Header>
                    <Card.Body>
                        <Card.Title>{guide.fullname}</Card.Title>
                        <Card.Text>
                            <i className="flaticon2-calendar-3 kt-font-success" /> {guide.username}
                        </Card.Text>
                        <Card.Text>
                            <i className="flaticon2-mail kt-font-warning" /> {guide.email}
                        </Card.Text>
                        <Card.Text>
                            <i className="flaticon2-rocket-1 kt-font-brand" /> {guide.role ? guide.role.toLowerCase() : "no role"}
                        </Card.Text>
                        <Button variant="brand" onClick={() => {
                            this.setState({
                                showEditInfoModal: true,
                                selectedGuide: guide
                            })
                        }}>Edit info</Button>
                    </Card.Body>
                </Card>
            </div>
            );
        }
        return "";
    }

    renderEditModal() {
        const handleClose = () => {
            this.setState({ showEditInfoModal: false });
        };

        const { intl } = this.props;

        const { selectedGuide } = this.state;

        if (!selectedGuide) return null;

        return (

            <Modal show={this.state.showEditInfoModal} onHide={handleClose} >
                <Modal.Header closeButton>
                    <Modal.Title className="h6">Edit {selectedGuide.fullname}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="bg-light">
                    <Formik
                        initialValues={{
                            email: selectedGuide.email,
                            fullname: selectedGuide.fullname,
                            username: selectedGuide.username,
                        }}
                        validate={values => {
                            const errors = {};

                            if (!values.email) {
                                errors.email = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            } else if (
                                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                            ) {
                                errors.email = intl.formatMessage({
                                    id: "AUTH.VALIDATION.INVALID_FIELD"
                                });
                            }

                            if (!values.fullname) {
                                errors.fullname = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (!values.username) {
                                errors.username = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            return errors;
                        }}
                        onSubmit={(values, { setStatus, setSubmitting }) => {
                            updateUser(
                                selectedGuide.id,
                                values.email,
                                values.fullname,
                                values.username,
                                values.role
                            )
                                .then((res) => {
                                    const guideUpdated = res.data;
                                    this.updateGuideRender(guideUpdated);
                                    this.setState({ showEditInfoModal: false, selectedGuide: null });
                                })
                                .catch(() => {
                                    setSubmitting(false);
                                    setStatus(
                                        intl.formatMessage({
                                            id: "AUTH.VALIDATION.INVALID_LOGIN"
                                        })
                                    );
                                });
                        }}
                    >
                        {({
                            values,
                            status,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <form onSubmit={handleSubmit} noValidate autoComplete="off">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group mb-0">
                                    <TextField
                                        margin="normal"
                                        label="Fullname"
                                        className="kt-width-full"
                                        name="fullname"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.fullname}
                                        helperText={touched.fullname && errors.fullname}
                                        error={Boolean(touched.fullname && errors.fullname)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        label="Email"
                                        margin="normal"
                                        className="kt-width-full"
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        helperText={touched.email && errors.email}
                                        error={Boolean(touched.email && errors.email)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        margin="normal"
                                        label="Username"
                                        className="kt-width-full"
                                        name="username"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.username}
                                        helperText={touched.username && errors.username}
                                        error={Boolean(touched.username && errors.username)}
                                    />
                                </div>

                                <div className="kt-login__actions mt-5">

                                    <button
                                        disabled={isSubmitting}
                                        className="btn btn-primary btn-elevate kt-login__btn-brand"
                                    >
                                        Submit
                                         </button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="link" onClick={handleClose}>
                        Close
                        </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    renderNewModal() {
        const handleClose = () => {
            this.setState({ showNewModal: false });
        };

        const { intl } = this.props;


        return (
            <Modal show={this.state.showNewModal} onHide={handleClose} >
                <Modal.Header closeButton>
                    <Modal.Title className="h6">New Guide</Modal.Title>
                </Modal.Header>
                <Modal.Body className="bg-light">
                    <Formik
                        initialValues={{
                            email: "",
                            fullname: "",
                            username: "",
                            password: "",
                            confirmPassword: ""
                        }}
                        validate={values => {
                            const errors = {};

                            if (!values.email) {
                                errors.email = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            } else if (
                                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                            ) {
                                errors.email = intl.formatMessage({
                                    id: "AUTH.VALIDATION.INVALID_FIELD"
                                });
                            }

                            if (!values.fullname) {
                                errors.fullname = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (!values.username) {
                                errors.username = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (!values.password) {
                                errors.password = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (!values.confirmPassword) {
                                errors.confirmPassword = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            } else if (values.password !== values.confirmPassword) {
                                errors.confirmPassword =
                                    "Password and Confirm Password didn't match.";
                            }

                            return errors;
                        }}
                        onSubmit={(values, { setStatus, setSubmitting }) => {
                            register(
                                values.email,
                                values.fullname,
                                values.username,
                                values.password,
                                "GUIDE"
                            )
                                .then((res) => {
                                    const { allGuides } = this.state;
                                    if (res && res.data) {
                                        const newGuide = res.data;
                                        allGuides.push(newGuide);
                                        this.setState({ allGuides: allGuides, showNewModal: false });
                                    } else {
                                        console.error('[Signup] - Res is empty');
                                    }
                                })
                                .catch(() => {
                                    setSubmitting(false);
                                    setStatus(
                                        intl.formatMessage({
                                            id: "AUTH.VALIDATION.INVALID_LOGIN"
                                        })
                                    );
                                });
                        }}
                    >
                        {({
                            values,
                            status,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <form onSubmit={handleSubmit} noValidate autoComplete="off">
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="form-group mb-0">
                                    <TextField
                                        margin="normal"
                                        label="Fullname"
                                        className="kt-width-full"
                                        name="fullname"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.fullname}
                                        helperText={touched.fullname && errors.fullname}
                                        error={Boolean(touched.fullname && errors.fullname)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        label="Email"
                                        margin="normal"
                                        className="kt-width-full"
                                        name="email"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.email}
                                        helperText={touched.email && errors.email}
                                        error={Boolean(touched.email && errors.email)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        margin="normal"
                                        label="Username"
                                        className="kt-width-full"
                                        name="username"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.username}
                                        helperText={touched.username && errors.username}
                                        error={Boolean(touched.username && errors.username)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        type="password"
                                        margin="normal"
                                        label="Password (4 caratteri)"
                                        className="kt-width-full"
                                        name="password"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.password}
                                        helperText={touched.password && errors.password}
                                        error={Boolean(touched.password && errors.password)}
                                    />
                                </div>

                                <div className="form-group mb-0">
                                    <TextField
                                        type="password"
                                        margin="normal"
                                        label="Confirm Password"
                                        className="kt-width-full"
                                        name="confirmPassword"
                                        onBlur={handleBlur}
                                        onChange={handleChange}
                                        value={values.confirmPassword}
                                        helperText={touched.confirmPassword && errors.confirmPassword}
                                        error={Boolean(
                                            touched.confirmPassword && errors.confirmPassword
                                        )}
                                    />
                                </div>


                                <div className="kt-login__actions mt-5">

                                    <button
                                        disabled={isSubmitting}
                                        className="btn btn-primary btn-elevate kt-login__btn-brand"
                                    >
                                        Submit
                                         </button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="link" onClick={handleClose}>
                        Close
                        </Button>
                </Modal.Footer>
            </Modal>
        );
    }

}

export default injectIntl(
    connect(
    )(Guides)
);