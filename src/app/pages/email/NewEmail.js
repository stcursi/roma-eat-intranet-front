import React, { Component } from "react";
import { toAbsoluteUrl } from "../../utils/utils";
import { getAllEmail, deleteEmail, editEmail } from "../../crud/email.crud";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { saveEmail } from "../../crud/email.crud";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import BootstrapTable from 'react-bootstrap-table-next';
import cellEditFactory from 'react-bootstrap-table2-editor';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

class NewEmail extends Component {

  constructor(props) {
    super(props);

    this.myFormRef = React.createRef();

    this.state = {
      emails: [],
      showSuccessAlert: false,
      showFailAlert: false
    };
  }

  componentDidMount() {
    this.getEmails()
  }

  async getEmails() {
    await getAllEmail()
      .then((res) => {
        if (res && res.data) this.setState({ emails: res.data });
      })
      .catch((error) => {
        console.error("[NewEmail] - getAllEmail: ", error);
      });
  }


  componentWillUnmount() {

  }

  initializeNewEmail(data) {
    let toReturn = {};

    toReturn.type = data.emailName ? data.emailName.value : null;
    toReturn.text = data.text ? data.text.value : null;

    return toReturn;
  }

  render() {

    const saveNewEmail = (event) => {
      event.preventDefault();

      if (event.target) {
        const newEmail = this.initializeNewEmail(event.target);

        saveEmail(newEmail)
          .then(() => {
            this.setState({ showSuccessAlert: true })
          })
          .catch(() => {
            this.setState({ showFailAlert: true })
          })
          .finally(() => {
            this.myFormRef.reset();
          })
      }

    }

    return (
      <div className="container-fluid">
        <main id="content" role="main">
          {/* <!-- Upload Form Section --> */}
          <div className="container space-2">
            <div className="w-lg-75 mx-lg-auto">
              {/* <!-- Title --> */}
              <div className="text-center mb-9">
                <h1 className="h2 kt-font-brand font-weight-medium">Crea una nuova Email</h1>
                <p>Inserisci i valori nei campi che seguono</p>
              </div>
              {/* <!-- End Title --> */}

              <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewEmail} ref={(el) => this.myFormRef = el}>
                {/* <!-- Listing Agent Information --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="emailName">Nome Email</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="emailNameLabel">
                                <span className="fas fa-file-signature"></span>
                              </span>
                            </div>
                            <input type="text" className="form-control" id="emailName" placeholder="Nome nuova email (Email info vaticano)" aria-label="Nome email" aria-describedby="emailNameLabel" required
                              data-msg="Please enter a listing agent name."
                              data-error-class="u-has-error"
                              data-success-class="u-has-success" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                  </div>
                  {/* <!-- End Listing Agent Information --> */}


                  {/* <!-- Upload Images --> */}
                  <div className="mb-7">
                    {/* <!-- Title --> */}
                    <div className="border-bottom pb-3 mb-5">
                      <h2 className="h6 kt-font-brand mb-0">Testo</h2>
                    </div>
                    {/* <!-- End Title --> */}

                    <div className="row">

                      <div className="col-lg-6 mb-3">
                        {/* <!-- Input --> */}
                        <div className="js-form-message mb-3">
                          <label className="form-label">
                            Testo
                    <span className="text-danger">*</span>
                          </label>

                          <div className="input-group">
                            <textarea className="form-control" rows="6" name="text" id="note" placeholder="Aggiungi il testo della mail" aria-label="Note e varie"
                              data-msg="Please enter a property description."
                              data-error-class="u-has-error"
                              data-success-class="u-has-success"></textarea>
                          </div>
                        </div>
                        {/* <!-- End Input --> */}
                      </div>
                    </div>
                  </div>
                </div>
                {/* <!-- End Upload Images --> */}

                <div className="row">
                  <div className="col-lg-3 col-md-2 col-sm-1">
                    <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Submit</button>
                  </div>
                </div>
              </form>
            </div>
            {this.renderTable()}
          </div>
          {/* <!-- End Upload Form Section --> */}
        </main>
        {this.renderSuccessModal()}
        {this.renderFailModal()}
      </div>
    )
  }

  renderTable() {

    const { emails } = this.state;

    const columns = [{
      dataField: 'type',
      editable: true,
      text: 'Nome',
      sort: true,
    }, {
      dataField: 'note',
      editable: true,
      text: 'Note',
      sort: true,
    }, {
      dataField: 'delete',
      editable: false,
      text: 'Emilina',
    }
    ];

    const handleDeleteEmail = (id) => {

      deleteEmail(id)
        .then(() => {
          this.getFonti()
            .then(() => this.setState({ showSuccessAlert: true }));
        })
        .catch((error) => {
          console.log('Error in delete email: ', error);
          this.setState({ showFailAlert: true });
        });
    }

    const handleEditEmail = (id, param, value) => {

      let params = {};
      params[param] = value;

      editEmail(id, params)
        .then(() => {
          this.getFonti()
            .then(() => this.setState({ showSuccessAlert: true }));
        });

    }

    const cellEdit = cellEditFactory({
      mode: 'click',
      afterSaveCell: (oldValue, newValue, row, column) => {

        handleEditEmail(row.id, column.dataField, newValue)

      },
    });

    const renderRows = (emails) => {

      let toReturn = [];

      for (let email of emails) {
        let newemail = Object.assign({}, email);
        newemail.type = email.type ? email.type : "Sconosciuto";
        newemail.note = email.note ? email.note : "Sconosciuto";
        newemail.delete = <div className="text-center">
          <button className="btn btn-xs btn-danger" onClick={() => handleDeleteEmail(email.id)}>Elimina</button>
        </div>

        toReturn.push(newemail);
      }

      return toReturn;
    }


    return (
      <div className="row my-5">
        <div className="col-lg-12">
          <h5>Emails automatiche </h5>
          <BootstrapTable striped
            keyField="id"
            data={renderRows(emails)}
            columns={columns}
            cellEdit={cellEdit}
          />
        </div>
      </div>
    )
  }



  renderSuccessModal() {
    const { showSuccessAlert } = this.state;

    const onCloseSuccessModal = () => {
      this.setState({ showSuccessAlert: false });
    };

    return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
  }

  renderFailModal() {
    const { showFailAlert } = this.state;

    const onCloseFailModal = () => {
      this.setState({ showFailAlert: false })
    };

    return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
  }




}

export default injectIntl(
  connect(
  )(NewEmail)
);