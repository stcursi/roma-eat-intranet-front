import React, { Suspense, lazy } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Summary from "./Summary/Summary";
import SummaryAdmin from "./Summary/SummaryAdmin";
import NewExperience from "../experience/NewExperience";
import Experiences from "../experience/Experiences";
import NewTour from "../tour/NewTour";
import NewTappa from "../tappa/NewTappa";
import Tappe from "../tappa/Tappe";
import Tours from "../tour/Tours";
import Calendar from "../tour/Calendar";
import { LayoutSplashScreen } from "../../../_metronic";
import ProfilePage from "../../pages/user/UserProfile";
import GuidesPage from "../guides/Guides";
import AssenzePage from "../assenze/Assenze";
import NewEmail from "../email/NewEmail";
import NewFonte from "../fonti/NewFonte";
import NewPaymentMethod from "../paymentMethod/NewPaymentMethod";
import PastTours from "../tour/PastTours";
import NewPartecipant from "../partecipant/NewPartecipant";


export default function HomePage() {
  // useEffect(() => {
  //   console.log('Home page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/summary" />
        }

        <Route path="/summary" component={Summary} />
        <Route path="/summaryAdmin" component={SummaryAdmin} />
        <Route path="/user" component={ProfilePage} />
        <Route path="/guides" component={GuidesPage} />
        <Route path="/assenze" component={AssenzePage} />
        <Route path="/experiences" component={Experiences} />
        {/* <Route path="/experience/:id" component={Experience} /> */}
        <Route path="/experience/new" component={NewExperience} />
        <Route path="/tappa/new" component={NewTappa} />
        <Route path="/tappe" component={Tappe} />
        <Route path="/tours/calendar" component={Calendar} />
        <Route path="/tours/all" component={Tours} />
        <Route path="/tours/past" component={PastTours} />
        <Route path="/tour/new" component={NewTour} />
        <Route path="/email/new" component={NewEmail} />
        <Route path="/fonti/new" component={NewFonte} />
        <Route path="/payment-method/new" component={NewPaymentMethod} />
        <Route path="/partecipant/add/:tourId" component={NewPartecipant} />
        <Redirect to="/error/error-v1" />
      </Switch>
    </Suspense>
  );
}
