import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { Button, Modal } from "react-bootstrap";
import * as auth from "../../../store/ducks/auth.duck";
import TourReminderTable from "../../../widgets/TourReminderTable";
import TourIter from "../../../widgets/TourIter";
import { getAllTours, editTour } from "../../../crud/tour.crud";
import { formatDate } from "../../../utils/date";
import moment from 'moment';
import clsx from "clsx";
import { editPartecipation } from "../../../crud/partecipation.crud";
import { TextField } from "@material-ui/core";

class Summary extends Component {

    constructor(props) {
        super(props);

        this.onSelectTour = this.onSelectTour.bind(this);
        this.onConfirmTour = this.onConfirmTour.bind(this);
        this.onRefuseTour = this.onRefuseTour.bind(this);
        this.onSelectedPartecipant = this.onSelectedPartecipant.bind(this);
        this.onSelectIter = this.onSelectIter.bind(this);
        this.onSelectToConfirm = this.onSelectToConfirm.bind(this);

        this.state = {
            tours: null,
            nextTour: null,
            selectedTour: null,
            showIterModal: null,
            showRefuseModal: null,
            tourToRefuse: null,
            tourForModal: null,
            amountToReceive: 0,
            confirmLoading: false,
            paddingRight: "0.5rem",
            onlyToConfirm: false,
            showImprevistoModal: false,
            imprevistoText: null,
            selectedPartecipant: null
        };

    }

    componentDidMount() {

        this.mainFetch();

    }

    mainFetch() {
        const { user } = this.props;

        const dataFrom = moment().startOf('day').toISOString();
        const dataTo = moment().add(8, 'months').startOf('day').toISOString();

        if (user && user.role !== 'ADMIN') {

            const params = { from: dataFrom, to: dataTo };

            getAllTours(params)
                .then((res) => {
                    if (res && res.data) {
                        let sortedTOurs = res.data.sort((a, b) => (new Date(a.startTime).getTime() - new Date(b.startTime).getTime()));
                        let nextTour = sortedTOurs.shift();
                        const dateTimeNow = Date.now();
                        if (nextTour.endTime && new Date(nextTour.endTime).getTime() < dateTimeNow) {
                            nextTour = sortedTOurs.shift();
                        }
                        nextTour.partecipants = nextTour.partecipants && nextTour.partecipants.length ? nextTour.partecipants.filter((p) => !p.annullato) : [];
                        let amount = this.getAmountToReceive(nextTour);

                        this.setState({ tours: sortedTOurs, nextTour: nextTour, amountToReceive: amount });
                    } else {
                        this.setState({ tours: [], nextTour: null, amountToReceive: 0 });
                    }
                })
                .catch((error) => {
                    console.error("[Tours] - getAllTours: ", error);
                })
        } else {
            // getExperienceCounter()
            //     .then((res) => {
            //         if (res && res.data) {
            //             console.log('DATAAAA', res.data);
            //         }
            //     });

            this.props.history.push({
                pathname: '/summaryAdmin',
            })
        }

    }

    componentWillUnmount() {

    }

    getTourConfirmed = (tour, userId) => {
        const tourGuideId = tour.guide.id;
        if (tourGuideId !== userId) {
            return tour.confermatoSecondaGuida;
        } else {
            return tour.confermato;
        }
    }

    onSelectTour(tour) {
        const { selectedTour } = this.state;
        if (selectedTour && selectedTour.id === tour.id) {
            this.setState({ selectedTour: null })
        } else {
            this.setState({ selectedTour: tour })
        }
    }

    onSelectToConfirm(toConfirmParam) {
        this.setState({ onlyToConfirm: toConfirmParam });
    }

    onSelectedPartecipant(event, tour) {
        event.stopPropagation();

        this.setState({ tourForModal: tour, showPartecipantModal: true })
    }

    onSelectIter(event, tour) {
        event.stopPropagation();

        this.setState({ tourForModal: tour, showIterModal: true })
    }

    onConfirmTour(event, tour, bool, isNext) {
        event.stopPropagation();
        const { tours } = this.state;
        const { user } = this.props;

        if (isNext) {
            this.setState({ confirmLoading: true, paddingRight: "2rem" });
        }

        let params = {};

        if (user.id !== tour.guide.id) {
            params.confermatoSecondaGuida = bool;
        } else {
            params.confermato = bool;
        }

        editTour(tour.id, { data: params })
            .then(() => {
                if (isNext) {
                    if (user.id !== tour.guide.id) {
                        tour.confermatoSecondaGuida = bool;
                    } else {
                        tour.confermato = bool;
                    }
                } else {
                    for (let t of tours) {
                        if (t.id === tour.id) {
                            if (user.id !== tour.guide.id) {
                                t.confermatoSecondaGuida = bool;
                            } else {
                                t.confermato = bool;
                            }
                            break;
                        }
                    }
                }

                this.setState({ tours: [...tours], nextTour: isNext ? tour : this.state.nextTour, confirmLoading: false, paddingRight: "0.5rem" });
            })
    }

    onRefuseTour(event, tour) {
        event.stopPropagation();
        this.setState({ tourToRefuse: tour, showRefuseModal: true });
    }

    getAmountToReceive(nextTour) {

        if (!nextTour) return 0;

        let amount = 0;

        for (let partecipant of nextTour.partecipants) {

            if (partecipant.paymentMethod && partecipant.paymentMethod.type && partecipant.paymentMethod.type.toLowerCase() === "contanti") {
                amount += Number(partecipant.price);
            }
        }

        return amount;
    }

    render() {

        const { tours, onlyToConfirm, nextTour, amountToReceive, confirmLoading, paddingRight } = this.state;
        const { user } = this.props;

        if ((!tours || !tours.length) && !nextTour) return <>
        </>;

        const handlePartecipantArrived = (event, partecipant) => {
            if (event && event.target) {
                editPartecipation(partecipant.id, { "arrivato": event.target.checked })
                    .then(() => this.mainFetch())
                    .catch((err) => console.log('Error in arrivato change ', err))
            }
        };


        const nextToursForTable = onlyToConfirm && onlyToConfirm === true ? tours.filter((tour) => {
            if (tour.guide?.id === user.id && tour.confermato === false) {
                return tour;
            }
            if (tour.secondGuide?.id === user.id && tour.confermatoSecondaGuida === false) {
                return tour;
            }
        }) : tours;

        return (
            <>
                <div className="row">
                    <div className="col-lg-8">
                        <div className="card w-100">
                            <div className="card-header d-flex justify-content-between">
                                <h4 className="card-title align-items-start flex-column">
                                    <span className="card-label font-weight-bolder kt-font-brand">
                                        Il tuo prossimo Tour
                                    </span>
                                    <p className="small text-muted mt-3 font-weight-bold font-size-sm">
                                        {nextTour.experience && nextTour.experience.name}
                                        {nextTour.privato && <span className="card-label ml-2 font-weight-medium">
                                            PVT
                                        </span>}
                                    </p>
                                </h4>
                                <span className="h-25 border-0 badge badge-lg badge-warning badge-inline mt-1 font-weight-bold font-size-lg">
                                    {formatDate(moment(nextTour.startTime).calendar())}
                                </span>
                                {nextTour.guide.id !== user.id ? <div className="row mt-2 mr-1">
                                    <span>
                                        Seconda guida
                                    </span>
                                    <div className="kt-notification__item-icon ml-2">
                                        <i className="flaticon-users-1 kt-font-warning fa-lg" />
                                    </div>
                                </div> : <></>}
                            </div>
                            <div className="card-body">
                                <div className="row align-items-start">
                                    <div className="col-lg-5 p-2">
                                        <h6>Meeting Point</h6>
                                        <p>{nextTour.meetingPoint}</p>
                                    </div>
                                    <div className="col-lg-5 p-2">
                                        <h6>Orario</h6>
                                        <p>{moment(nextTour.startTime).format('HH:mm')} - {moment(nextTour.endTime).format('HH:mm')}</p>
                                    </div>
                                    <div className="col-lg-2 text-center">
                                        {this.getTourConfirmed(nextTour, user.id) ? <div>
                                            <button className={`border-0 badge badge-lg badge-success badge-inline ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": confirmLoading
                                                }
                                            )}`}
                                                style={{ paddingRight }}
                                                onClick={(e) => this.onConfirmTour(e, nextTour, false, true)}>
                                                Confermato
                                            </button>
                                            <p className="small text-muted">Click per annullare</p></div> : <div>
                                            <button className={`border-0 badge badge-lg badge-info badge-inline ${clsx(
                                                {
                                                    "kt-spinner kt-spinner--right kt-spinner--md kt-spinner--light": confirmLoading
                                                }
                                            )}`}
                                                style={{ paddingRight }}
                                                onClick={(e) => this.onConfirmTour(e, nextTour, true, true)}>
                                                In attesa
                                            </button>
                                            <p className="small text-muted">Click per confermare</p>
                                        </div>}
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-lg-5 p-2">
                                        <h6>Totale da incassare</h6>
                                        <p>{amountToReceive ? amountToReceive + " €" : "Nessun pagamento in contanti"}</p>
                                    </div>
                                    <div className="col-lg-5 p-2">
                                        <h6>Note ed eventuali</h6>
                                        <p>{nextTour.noteGuida || "Nessuna informazione aggiuntiva"}</p>
                                    </div>
                                    <div className="col-lg-2 p-2 text-center">
                                        <button className=" border-0 badge badge-lg badge-danger mt-1 badge-inline"
                                            onClick={(e) => this.onRefuseTour(e, nextTour, false, true)}>
                                            Rifiuta
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {nextTour.partecipants.length ? <div className="card w-100 my-3">
                            <div className="card-header d-flex justify-content-between">
                                <h4 className="card-title align-items-start flex-column">
                                    <span className="card-label font-weight-bolder kt-font-brand">
                                        Partecipanti
                                    </span>
                                </h4>
                                <div>
                                    <span className="h4 font-weight-bold kt-font-google">{nextTour.partecipants ? nextTour.partecipants.length : ""}</span>
                                    <span className="fas fa-users fa-2x kt-font-google mx-2" />
                                </div>
                            </div>
                            <div className="card-body pt-3 pb-0">
                                <div className="table-responsive">
                                    <table className="table table-borderless table-striped table-vertical-center">
                                        <thead>
                                            <tr>
                                                <td><div className="kt-notification__item-icon">
                                                    <i className="flaticon2-check-mark kt-font-success" />
                                                </div></td>
                                                <td>Nome</td>
                                                <td>Ritiro soldi</td>
                                                {/* <td>Vegano</td> */}
                                                <td>Vegetariano</td>
                                                <td>Note</td>
                                                <td>Prenotato da</td>
                                                <td>Provenienza</td>
                                                <td>Allergie</td>
                                                <td>Imprevisto</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {nextTour.partecipants && nextTour.partecipants.map(p =>
                                                <tr>
                                                    <td className="text-center">
                                                        <input id="checkArrivato" type="checkbox" defaultChecked={false} checked={p.arrivato}
                                                            onChange={(event) => handlePartecipantArrived(event, p)} />
                                                    </td>
                                                    <td>{p.customer && p.customer.name} {p.customer && p.customer.surname}</td>
                                                    <td>{p.paymentMethod && p.paymentMethod.type && p.paymentMethod.type.toLowerCase() === "contanti" && !p.paid ? p.price : "No"}</td>
                                                    {/* <td>{p.vegan ? "Si" : "No"}</td> */}
                                                    <td>{p.customer && p.customer.vegetarian ? "Si" : "No"}</td>
                                                    <td>{p.note}</td>
                                                    <td>{p.bookedFrom && p.bookedFrom.type ? p.bookedFrom.type : ""}</td>
                                                    <td>{p.customer && p.customer.provenienza}</td>
                                                    <td>{p.customer && p.customer.allergies}</td>
                                                    <td className="text-center">
                                                        <button className={`border-0 badge badge-lg ${p.imprevisto ? 'badge-danger' : 'badge-warning'} badge-inline`}
                                                            onClick={() => this.setState({ showImprevistoModal: true, selectedPartecipant: p, imprevistoText: p.imprevisto || '' })}>
                                                            <i className={`flaticon-alert ${p.imprevisto ? 'kt-font-light' : 'kt-font-dark'}`} />
                                                        </button>
                                                        {/* {p.imprevisto ? <OverlayTrigger
                                                            placement="left"
                                                            overlay={<Tooltip id="layout-tooltip">{p.imprevisto}</Tooltip>}
                                                        >
                                                            <div className="kt-notification__item-icon">
                                                                <i className="flaticon-alert kt-font-danger" />
                                                            </div>
                                                        </OverlayTrigger> : <></>} */}
                                                    </td>

                                                </tr>)}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div> : ""}
                    </div>
                    {nextTour.experience && nextTour.experience.tappe && nextTour.experience.orderTappe && <div className="col-lg-4">
                        <TourIter tappe={nextTour.experience.tappe} orderTappe={nextTour.experience.orderTappe} tour={nextTour} />
                    </div>}
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        {nextToursForTable && nextToursForTable.length ? <TourReminderTable tours={nextToursForTable}
                            user={user}
                            onSelectTour={this.onSelectTour}
                            onSelectToConfirm={this.onSelectToConfirm}
                            onConfirmTour={this.onConfirmTour}
                            onRefuseTour={this.onRefuseTour}
                            onSelectedPartecipant={this.onSelectedPartecipant}
                            onSelectIter={this.onSelectIter}
                            paddingRight={paddingRight}
                            className="card-stretch gutter-b" /> : this.renderEmptyState()}
                    </div>
                </div>
                {this.renderPartecipantsModal()}
                {this.renderIterModal()}
                {this.renderRefuseModal()}
                {this.renderImprevistoModal()}
            </>
        )
    }

    renderEmptyState() {
        return <div className="row">
            <div className="col-lg-8">
                <div className="card w-100">
                    <div className="card-header">
                        <h4 className="card-title align-items-start d-flex justify-content-between">
                            <span className="card-label font-weight-bolder kt-font-brand">
                                Non hai alcun tuor da confermare!
                            </span>
                            <button className="btn btn-primary" onClick={() => this.onSelectToConfirm(null)}>Mostra tutti i tour</button>
                        </h4>
                    </div>
                    <div className="card-body">
                        <div className="row align-items-start">
                            <p>Hai già confermato tutti i tuor dei prossimi 8 mesi</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }


    renderPartecipantsModal() {
        const { showPartecipantModal, tourForModal } = this.state;

        const handleClose = () => {
            this.setState({ showPartecipantModal: false, tourForModal: null });
        }

        if (!tourForModal) return "";

        return <Modal key="modal_part" show={showPartecipantModal} size="lg" onHide={handleClose}>
            <Modal.Body className="rounded-lg bg-white">
                <div className="card-body pt-3 pb-0">
                    <div className="table-responsive">
                        <table className="table table-borderless table-striped table-vertical-center">
                            <thead>
                                <tr>
                                    <td>Nome</td>
                                    <td>Ritiro soldi</td>
                                    {/* <td>Vegano</td> */}
                                    <td>Vegetariano</td>
                                    <td>Provenienza</td>
                                    <td>Prenotato da</td>
                                    <td>Note</td>
                                    <td>Allergie</td>
                                </tr>
                            </thead>
                            <tbody>
                                {tourForModal.partecipants && tourForModal.partecipants.map(p =>
                                    <tr>
                                        <td>{p.customer && p.customer.name} {p.customer && p.customer.surname}</td>
                                        <td>{p.paymentMethod && p.paymentMethod.type && p.paymentMethod.type.toLowerCase() === "contanti" && !p.paid ? "Si" : "No"}</td>
                                        {/* <td>{p.vegan ? "Si" : "No"}</td> */}
                                        <td>{p.customer && p.customer.vegetarian ? "Si" : "No"}</td>
                                        <td>{p.customer && p.customer.provenienza}</td>
                                        <td>{p.bookedFrom && p.bookedFrom.type ? p.bookedFrom.type : ""}</td>
                                        <td>{p.note}</td>
                                        <td>{p.customer && p.customer.allergies}</td>
                                    </tr>)}
                            </tbody>
                        </table>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    }

    renderIterModal() {
        const { showIterModal, tourForModal } = this.state;

        const handleClose = () => {
            this.setState({ showIterModal: false, tourForModal: false });
        }

        if (!tourForModal) return "";

        return <Modal key="modal_iter" show={showIterModal} size="md" onHide={handleClose}>
            <Modal.Body className="rounded-lg bg-white">
                <TourIter
                    tappe={tourForModal.experience.tappe}
                    orderTappe={tourForModal.experience.orderTappe}
                    tour={tourForModal}
                />
            </Modal.Body>
        </Modal>
    }


    renderImprevistoModal() {
        const { showImprevistoModal, imprevistoText, selectedPartecipant } = this.state;

        const handleClose = () => {
            this.setState({ showImprevistoModal: false, imprevistoText: null, selectedPartecipant: null });
        }

        const handleChange = (e) => {
            this.setState({ imprevistoText: e.target.value })
        }

        const handleSalvaImprevisto = () => {
            editPartecipation(selectedPartecipant.id, { "imprevisto": imprevistoText })
                .then(() => this.mainFetch())
                .catch((err) => console.log('Error in arrivato change ', err))
                .finally(() => this.setState({ showImprevistoModal: false, imprevistoText: null, selectedPartecipant: null }))
        }

        return <Modal key="modal_iter" show={showImprevistoModal} size="sm" onHide={handleClose}>
            <Modal.Header closeButton>
                <h6 className="test-danger">Imprevisto.</h6>
            </Modal.Header>
            <Modal.Body className="rounded-lg bg-white">
                <TextField
                    fullWidth
                    multiline
                    label="Inserisci qui cosa è successo"
                    InputProps={{
                        rows: 3
                    }}
                    value={imprevistoText}
                    onChange={handleChange}
                />
            </Modal.Body>
            <Modal.Footer>
                <Button variant="link text-dark" size="sm" onClick={handleClose}>Chiudi</Button>
                <Button variant="danger" size="sm" onClick={handleSalvaImprevisto}>Salva imprevisto</Button>
            </Modal.Footer>
        </Modal>
    }

    renderRefuseModal() {


        const refuseModal = (tour) => {

            const { user } = this.props;
            let params = {};

            if (user.id !== tour.guide.id) {
                params.rifiutatoSecondaGuida = true;
            } else {
                params.rifiuato = true;
            }

            editTour(tour.id, { data: params })
                .then(() => {
                    this.mainFetch();
                    this.setState({ tourToRefuse: null, showRefuseModal: false });
                })
        }

        const { showRefuseModal, tourToRefuse } = this.state;

        const handleClose = () => {
            this.setState({ showRefuseModal: false, tourToRefuse: null });
        }

        if (!tourToRefuse) return "";

        return <Modal key="modal_refuse" show={showRefuseModal} size="md" onHide={handleClose}>
            <Modal.Header closeButton>
                <h6 className="test-danger">Stai per rifiutare questo tour.</h6>
            </Modal.Header>
            <Modal.Body className="rounded-lg bg-white">
                Vuoi veramente rifiutare il tour ? Una volta rifiutato sparirà dalla tua Dashboard e solo un admin potrà riassegnartelo.
            </Modal.Body>
            <Modal.Footer>
                <Button variant="link text-dark" size="sm" onClick={handleClose}>Chiudi</Button>
                <Button variant="danger" size="sm" onClick={() => refuseModal(tourToRefuse)}>Rifiuta</Button>
            </Modal.Footer>
        </Modal>

    }

}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});
export default injectIntl(connect(mapStateToProps, auth.actions)(Summary));