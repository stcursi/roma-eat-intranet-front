import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import * as auth from "../../../store/ducks/auth.duck";
import { Button, Modal } from "react-bootstrap";
import { getAllTours } from "../../../crud/tour.crud";
import { formatDate } from "../../../utils/date";
import moment from 'moment';
import { selectColor, checkIfEveryPartecipantsRefused } from "../../../utils/calendar";
import { editPartecipation } from "../../../crud/partecipation.crud";
import './summaryAdmin.css';

class SummaryAdmin extends Component {

    constructor(props) {
        super(props);

        this.onSelectTour = this.onSelectTour.bind(this);
        this.onSelectedPartecipant = this.onSelectedPartecipant.bind(this);

        this.state = {
            tours: null,
            nextTour: null,
            selectedTour: null,
            tourForModal: null,
            showNoteAdminModal: false,
            noteAdminPartecipantText: null,
            selectedPartecipant: null
        };

    }

    componentDidMount() {

        this.mainFetch();

    }

    mainFetch() {
        const { user } = this.props;

        const dataFrom = moment().startOf('day').toISOString();
        const dataTo = moment().add(1, 'months').startOf('day').toISOString();

        if (user && user.role === 'ADMIN') {

            const params = { from: dataFrom, to: dataTo };

            getAllTours(params)
                .then((res) => {
                    if (res && res.data) {
                        let sortedTOurs = res.data.sort((a, b) => (new Date(a.startTime).getTime() - new Date(b.startTime).getTime()));
                        this.setState({ tours: sortedTOurs });
                    } else {
                        this.setState({ tours: [] });
                    }
                })
                .catch((error) => {
                    console.error("[Tours] - getAllTours: ", error);
                })
        } else {

            this.props.history.push({
                pathname: '/summary',
            })
        }

    }

    componentWillUnmount() {

    }

    onSelectTour(tour) {
        const { selectedTour } = this.state;
        if (selectedTour && selectedTour.id === tour.id) {
            this.setState({ selectedTour: null })
        } else {
            this.setState({ selectedTour: tour })
        }
    }

    onSelectedPartecipant(event, tour) {
        event.stopPropagation();

        this.setState({ tourForModal: tour, showPartecipantModal: true })
    }

    getAmountToReceive(nextTour) {

        if (!nextTour) return 0;

        let amount = 0;

        for (let partecipant of nextTour.partecipants) {

            if (partecipant.paymentMethod && partecipant.paymentMethod.type && partecipant.paymentMethod.type.toLowerCase() === "contanti") {
                amount += partecipant.price ? Number(partecipant.price.replace(',', '.')) : 0;
            }
        }

        return amount ? amount + " €" : "Nessun pagamento in contanti";

    }

    getAllAllergie(nextTour) {
        if (!nextTour) return '';
        let totAllergie = [];

        for (let partecipant of nextTour.partecipants) {

            if (partecipant.customer && partecipant.customer.allergies) {
                totAllergie.push(partecipant.customer.allergies);
            }
        }

        return totAllergie.length ? totAllergie.join(',') : 'Nessuna allergia';
    }

    getAllVeggyGuys(nextTour) {
        if (!nextTour) return null;
        let toReturn = 0;

        for (let partecipant of nextTour.partecipants) {

            if (partecipant.customer && partecipant.customer.vegetarian) {
                toReturn++;
            }
        }

        return toReturn;
    }

    render() {

        const { tours } = this.state;

        if (!tours || !tours.length) return <>
        </>;

        const handlePartecipantArrived = (event, partecipant) => {
            if (event && event.target) {
                editPartecipation(partecipant.id, { "arrivato": event.target.checked })
                    .then(() => this.mainFetch())
                    .catch((err) => console.log('Error in arrivato change ', err))
            }
        };

        return (
            <div>
                {tours.map((nextTour, index) => {

                    const tuttiPartAnnullati = checkIfEveryPartecipantsRefused(nextTour.partecipants);

                    if (!tuttiPartAnnullati) {
                        nextTour.partecipants = nextTour.partecipants && nextTour.partecipants.length ? nextTour.partecipants.filter((p) => !p.annullato) : [];

                        const colorObject = selectColor(nextTour);
                        return <>
                            <div key={index} className="d-flex">
                                <div className="col-lg-12">
                                    <div className="card w-100">
                                        <div className="card-header d-flex justify-content-between">
                                            <div className="d-flex flex-column w-25">
                                                <div className="d-flex">
                                                    <span style={{ backgroundColor: colorObject.backgroundColor }} className={`status-badge border border-light rounded-circle`} />
                                                    <h4 className="card-title">
                                                        <span className="card-label font-weight-bolder kt-font-brand">
                                                            {nextTour.experience && nextTour.experience.name}
                                                        </span>
                                                        {nextTour.privato && <span className="card-label ml-2 font-weight-medium">
                                                            PVT
                                                        </span>}
                                                    </h4>
                                                </div>
                                                <span className="text-muted mt-3 font-size-sm">{colorObject.legenda}</span>
                                            </div>
                                            <span className="h-25 border-0 badge badge-lg badge-warning badge-inline mt-1 font-weight-bold font-size-lg">
                                                {formatDate(moment(nextTour.startTime).calendar())}
                                            </span>
                                            <div className="row mt-2 mr-1 flex-column">
                                                <h6>
                                                    Guide
                                                </h6>
                                                <div className="d-flex flex-column">
                                                    {nextTour.guide ? <span>Prima: {nextTour.guide.fullname}</span> : ''}
                                                    {nextTour.secondGuide ? <span>Seconda: {nextTour.secondGuide.fullname}</span> : ''}
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <div className="row align-items-start">
                                                <div className="col-lg-5 p-2">
                                                    <h6>Meeting Point</h6>
                                                    <p>{nextTour.meetingPoint}</p>
                                                </div>
                                                <div className="col-lg-5 p-2">
                                                    <h6>Orario</h6>
                                                    <p>{moment(nextTour.startTime).format('HH:mm')} - {moment(nextTour.endTime).format('HH:mm')}</p>
                                                </div>
                                                <div className="col-lg-2 text-center">
                                                    <h6>Allergie</h6>
                                                    <p>{this.getAllAllergie(nextTour)}</p>
                                                </div>
                                            </div>
                                            <div className="row">
                                                <div className="col-lg-5 p-2">
                                                    <h6>Totale da incassare</h6>
                                                    <p>{this.getAmountToReceive(nextTour)}</p>
                                                </div>
                                                <div className="col-lg-5 p-2">
                                                    <h6>Note</h6>
                                                    <p>{nextTour.note || "Nessuna informazione aggiuntiva"}</p>
                                                </div>
                                                <div className="col-lg-2 p-2 text-center">
                                                    <h6>Vegetariani</h6>
                                                    <p>{this.getAllVeggyGuys(nextTour)}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {nextTour.partecipants.length ? <div className="card w-100 my-3">
                                        <div className="card-header d-flex justify-content-between">
                                            <h4 className="card-title align-items-start flex-column">
                                                <span className="card-label font-weight-bolder kt-font-brand">
                                                    Partecipanti
                                                </span>
                                            </h4>
                                            <div>
                                                <span className="h4 font-weight-bold kt-font-google">{nextTour.partecipants ? nextTour.partecipants.length : ""}</span>
                                                <span className="fas fa-users fa-2x kt-font-google mx-2" />
                                            </div>
                                        </div>
                                        <div className="card-body pt-3 pb-0">
                                            <div className="table-responsive">
                                                <table className="table table-borderless table-striped table-vertical-center">
                                                    <thead>
                                                        <tr>
                                                            <td><div className="kt-notification__item-icon">
                                                                <i className="flaticon2-check-mark kt-font-success" />
                                                            </div></td>
                                                            <td>Nome</td>
                                                            <td>Ritiro soldi</td>
                                                            {/* <td>Vegano</td> */}
                                                            <td style={{ width: '150px' }}>Telefono</td>
                                                            <td>Email</td>
                                                            <td>Note</td>
                                                            <td>Prenotato da</td>
                                                            <td>Provenienza</td>
                                                            <td>Allergie</td>
                                                            <td>Imprevisto</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {nextTour.partecipants && nextTour.partecipants.map(p =>
                                                            <tr>
                                                                <td className="text-center">
                                                                    <input id="checkArrivato" type="checkbox" defaultChecked={false} checked={p.arrivato}
                                                                        onChange={(event) => handlePartecipantArrived(event, p)} />
                                                                </td>
                                                                <td>{p.customer && p.customer.name} {p.customer && p.customer.surname}</td>
                                                                <td>{p.paymentMethod && p.paymentMethod.type && p.paymentMethod.type.toLowerCase() === "contanti" && !p.paid ? p.price : "No"}</td>
                                                                {/* <td>{p.vegan ? "Si" : "No"}</td> */}
                                                                <td>{p.customer && p.customer.telephone}</td>
                                                                <td>{p.customer && p.customer.email}</td>
                                                                <td>{p.note}</td>
                                                                <td>{p.bookedFrom && p.bookedFrom.type ? p.bookedFrom.type : ""}</td>
                                                                <td>{p.customer && p.customer.provenienza}</td>
                                                                <td>{p.customer && p.customer.allergies}</td>
                                                                <td className="text-center">
                                                                    <button className={`border-0 badge badge-lg ${p.noteAdmin ? 'badge-success' : 'badge-warning'} badge-inline`}
                                                                        onClick={() => this.setState({ showNoteAdminModal: true, selectedPartecipant: p, noteAdminPartecipantText: p.noteAdmin || '' })}>
                                                                        <i className={`flaticon-alert ${p.imprevisto ? 'kt-font-light' : 'kt-font-dark'}`} />
                                                                    </button>
                                                                    {/* {p.imprevisto ? <OverlayTrigger
                                                    placement="left"
                                                    overlay={<Tooltip id="layout-tooltip">{p.imprevisto}</Tooltip>}
                                                >
                                                    <div className="kt-notification__item-icon">
                                                        <i className="flaticon-alert kt-font-danger" />
                                                    </div>
                                                </OverlayTrigger> : <></>} */}
                                                                </td>

                                                            </tr>)}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> : ""}
                                </div>
                            </div>
                            <hr style={{ marginTop: "40px", marginBottom: "40px" }} />
                        </>
                    } else {
                        return <></>
                    }
                })}

                {this.renderNoteAdminModal()}
            </div>
        )
    }

    renderNoteAdminModal() {
        const { showNoteAdminModal, noteAdminPartecipantText, selectedPartecipant } = this.state;

        const handleClose = () => {
            this.setState({ showNoteAdminModal: false, noteAdminPartecipantText: null, selectedPartecipant: null });
        }

        // const handleChange = (e) => {
        //     this.setState({noteAdminPartecipantText: e.target.value})
        // }

        // const handleSalvaImprevisto = () => {
        //     editPartecipation(selectedPartecipant.id, { "imprevisto" : imprevistoText })
        //     .then(() => this.mainFetch())
        //     .catch((err) => console.log('Error in arrivato change ', err))
        //     .finally(() => this.setState({showImprevistoModal: false, imprevistoText: null, selectedPartecipant: null}))
        // }

        return <Modal key="modal_iter" show={showNoteAdminModal} size="sm" onHide={handleClose}>
            <Modal.Header closeButton>
                <h6 className="test-danger">Note admin</h6>
            </Modal.Header>
            <Modal.Body className="rounded-lg bg-white">
                <p>{noteAdminPartecipantText}</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="link text-dark" size="sm" onClick={handleClose}>Chiudi</Button>
                {/* <Button variant="danger" size="sm" onClick={handleSalvaImprevisto}>Salva imprevisto</Button> */}
            </Modal.Footer>
        </Modal>
    }

}

const mapStateToProps = ({ auth: { user } }) => ({
    user
});
export default injectIntl(connect(mapStateToProps, auth.actions)(SummaryAdmin));