import React, { Component } from "react";
import { toAbsoluteUrl } from "../../utils/utils";
import { getAllEmail } from "../../crud/email.crud";
import { getAllTappe } from "../../crud/tappa.crud";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { saveExperience } from "../../crud/experience.crud";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";

import Grid from '@material-ui/core/Grid';
import TappeSelector from "../tappa/TappeSelector";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";

var moment = require('moment');

class NewExperience extends Component {

  constructor(props) {
    super(props);

    this.myFormRef = React.createRef();

    this.state = {
      email: [],
      tappe: [],
      orderTappe: {},
      selectedTappe: [],
      startTime: moment().hour(9).minute(0),
      endTime: moment().hour(12).minute(0),
      showSuccessAlert: false,
      showFailAlert: false
    };
  }

  componentDidMount() {

    getAllEmail()
      .then((res) => {
        if (res && res.data) this.setState({ email: res.data });
      })
      .catch((error) => {
        console.error("[NewExperience] - getAllEmail: ", error);
      })

    getAllTappe()
      .then((res) => {
        if (res && res.data) this.setState({ tappe: res.data });
      })
      .catch((error) => {
        console.error("[NewExperience] - getAllTappe: ", error);
      })
  }

  componentWillUnmount() {


  }


  initializeNewExperience(data) {
    let toReturn = {};

    const { startTime, endTime, selectedTappe } = this.state;

    toReturn.name = data.experienceName ? data.experienceName.value : null;
    toReturn.partecipantNumber = data.partecipantNumber ? data.partecipantNumber.value : null;
    toReturn.startTime = startTime ? moment(startTime).format('HH:mm') : null;
    toReturn.endTime = endTime ? moment(endTime).format('HH:mm') : null;
    toReturn.tappe = [];
    let tappeToJoin = [];

    let orderTappeToSave = {}
    if (selectedTappe) {
      for (let selTappa of selectedTappe) {
        if (data["tappa_" + selTappa] && data["tappa_" + selTappa].value) {
          orderTappeToSave[data["tappa_" + selTappa].value] = selTappa;
          toReturn.tappe.push(selTappa);
        }
      }

      let i = 1;

      while (i <= selectedTappe.length) {
        tappeToJoin.push(orderTappeToSave[i])
        i++;
      }
    }


    toReturn.orderTappe = tappeToJoin.join(',');
    toReturn.emailInfo = data.emailInfo ? data.emailInfo.value : null;
    toReturn.emailPre = data.emailPre ? data.emailPre.value : null;
    toReturn.emailPost = data.emailPost ? data.emailPost.value : null;
    toReturn.emailRecensione = data.emailReview ? data.emailReview.value : null;
    toReturn.picture = data.picture ? data.picture.value : null;
    toReturn.note = data.note ? data.note.value : null;
    toReturn.price = data.price ? data.price.value : null;

    return toReturn;
  }

  render() {

    const { tappe, email, selectedTappe, orderTappe } = this.state;

    const saveNewExperience = (event) => {
      event.preventDefault();

      if (event.target) {
        const newExperience = this.initializeNewExperience(event.target);

        saveExperience(newExperience)
          .then(() => {
            this.setState({ showSuccessAlert: true, selectedTappe: [], orderTappe: {} })
          })
          .catch(() => {
            this.setState({ showFailAlert: true })
          })
          .finally(() => {
            this.myFormRef.reset();
          })
      }

    }

    const updateTappe = (event) => {
      this.setState({ selectedTappe: event.target.value })
    }

    return (
      <div className="container-fluid">
        <main id="content" role="main">
          {/* <!-- Upload Form Section --> */}
          <div className="container space-2">
            <div className="w-lg-75 mx-lg-auto">
              {/* <!-- Title --> */}
              <div className="text-center mb-9">
                <h1 className="h2 kt-font-brand font-weight-medium">Crea una nuova esperienza</h1>
                <p>Inserisci i valori nei campi che seguono</p>
              </div>
              {/* <!-- End Title --> */}

              <form id="uploadForm" className="js-validate svg-preloader" onSubmit={saveNewExperience} ref={(el) => this.myFormRef = el}>
                {/* <!-- Listing Agent Information --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="experienceName">Nome</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="experienceNameLabel">
                                <span className="fas fa-file-signature"></span>
                              </span>
                            </div>
                            <input type="text" className="form-control" name="experienceName" id="experienceName" placeholder="Nome esperienza" aria-label="Nome esperienza" aria-describedby="experienceNameLabel" required
                              data-msg="Please enter a listing agent name."
                              data-error-class="u-has-error"
                              data-success-class="u-has-success" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                    <div className="col-md-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-form-message js-focus-state">
                          <label className="form-label" htmlFor="partecipantNumber">Numero partecipanti</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="partecipantNumberLabel">
                                <span className="fas fa-users"></span>
                              </span>
                            </div>
                            <input type="number" className="form-control" name="phoneNumber" id="partecipantNumber" placeholder="Per esempio 10" aria-label="Per esempio 10" aria-describedby="partecipantNumberLabel" required
                              data-msg="Please enter a phone number."
                              data-error-class="u-has-error"
                              data-success-class="u-has-success" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>

                  <div className="row">
                    {this.renderTimePickers()}
                  </div>
                </div>
                {/* <!-- End Listing Agent Information --> */}


                {/* <!-- Listing Information --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Struttura</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  {tappe.length ? <TappeSelector tappe={tappe}
                    orderTappe={orderTappe}
                    selectedTappe={selectedTappe}
                    updateTappe={updateTappe} /> : ""}

                </div>
                {/* <!-- End Listing Information --> */}

                {/* <!-- Email setting --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Email Automatiche</h2>
                  </div>
                  {/* <!-- End Title --> */}
                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="emailInfo">Email di Info</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="emailInfoLabel">
                                <span className="fas fa-envelope"></span>
                              </span>
                            </div>
                            <select className="custom-select" id="emailInfo" placeholder="Scegliere email info" aria-describedby="emailInfoLabel">
                              <option hidden>Scegliere email info</option>
                              {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="emailPre">Email pre</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="emailPreLabel">
                                <span className="fas fa-envelope"></span>
                              </span>
                            </div>
                            <select className="custom-select" id="emailPre" placeholder="Scegliere email pre" aria-describedby="emailPreLabel">
                              <option hidden>Scegliere email pre</option>
                              {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="emailPost">Email post experience</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="emailPostLabel">
                                <span className="fas fa-envelope"></span>
                              </span>
                            </div>
                            <select className="custom-select" id="emailPost" placeholder="Scegliere email post" aria-describedby="emailPostLabel">
                              <option hidden>Scegliere email post</option>
                              {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="emailReview">Email per la review</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="emailReviewLabel">
                                <span className="fas fa-envelope"></span>
                              </span>
                            </div>
                            <select className="custom-select" id="emailReview" placeholder="Scegliere email review" aria-describedby="emailReviewLabel">
                              <option hidden>Scegliere email review</option>
                              {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                  </div>

                </div>
                {/* <!-- End Emails --> */}

                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Icona experience</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="picture">
                            <span className="fas fa-map-marker-alt mr-2"></span> Scegliere l'icona </label>
                          <div className="input-group">

                            <select className="custom-select" searchable="Search here.." id="picture" aria-describedby="pictureLabel">
                              <option value="colosseum.png">Icona Colosseo</option>
                              <option value="campo.png">Icona Campo</option>
                              <option value="vatican.png">Icona Vaticano</option>
                              <option value="monti.png">Icona Monti</option>
                              <option value="italiaMarker.png">Icona Italiani</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>

                  </div>
                </div>
                {/* <!-- End Upload Images --> */}

                {/* <!-- Upload Images --> */}
                <div className="mb-7">
                  {/* <!-- Title --> */}
                  <div className="border-bottom pb-3 mb-5">
                    <h2 className="h6 kt-font-brand mb-0">Note</h2>
                  </div>
                  {/* <!-- End Title --> */}

                  <div className="row">
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="js-form-message mb-3">
                        <label className="form-label">
                          Note ed eventuali  <span className="text-danger">*</span>
                        </label>

                        <div className="input-group">
                          <textarea className="form-control" rows="6" name="text" id="note" placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                            data-msg="Please enter a property description."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success"></textarea>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                    <div className="col-lg-6 mb-3">
                      {/* <!-- Input --> */}
                      <div className="form-group mb-5">
                        <div className="js-focus-state">
                          <label className="form-label" htmlFor="listingPrice">Price</label>
                          <div className="input-group">
                            <div className="input-group-prepend">
                              <span className="input-group-text" id="listingPriceLabel">
                                <span className="fas fa-dollar-sign"></span>
                              </span>
                            </div>
                            <input type="text" className="form-control" name="price" id="listingPrice" placeholder="Price" aria-label="Price" aria-describedby="listingPriceLabel" />
                          </div>
                        </div>
                      </div>
                      {/* <!-- End Input --> */}
                    </div>
                  </div>
                </div>
                {/* <!-- End Upload Images --> */}

                <div className="row">
                  <div className="col-lg-3 col-md-2 col-sm-1">
                    <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Salva</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          {/* <!-- End Upload Form Section --> */}
        </main>
        {this.renderSuccessModal()}
        {this.renderFailModal()}
      </div>
    )
  }


  renderSuccessModal() {
    const { showSuccessAlert } = this.state;

    const onCloseSuccessModal = () => {
      this.setState({ showSuccessAlert: false });
    };

    return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
  }

  renderFailModal() {
    const { showFailAlert } = this.state;

    const onCloseFailModal = () => {
      this.setState({ showFailAlert: false })
    };

    return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
  }


  renderTimePickers() {

    const { startTime, endTime } = this.state;

    const handleSTDateChange = (date) => {
      this.setState({ startTime: date });
    };

    const handleETDateChange = (date) => {
      this.setState({ endTime: date });
    };

    return <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container className="ml-3 mb-5">
        <Grid lg="6">
          <Grid lg="10" item>
            <label className="form-label" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
            <div className="input-group">
              <KeyboardTimePicker
                margin="normal"
                id="startTimePicker"
                ampm={false}
                value={startTime}
                onChange={handleSTDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </div>

          </Grid>
        </Grid>
        <Grid lg="6">
          <Grid lg="10" className="ml-1" item>
            <label className="form-label" htmlFor="endTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
            <div className="input-group">
              <KeyboardTimePicker
                margin="normal"
                id="endTimePicker"
                ampm={false}
                value={endTime}
                onChange={handleETDateChange}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </div>
          </Grid>
        </Grid>
      </Grid>
    </MuiPickersUtilsProvider>
  }



}

export default injectIntl(
  connect(
  )(NewExperience)
);