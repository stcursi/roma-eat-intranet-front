import React, { Component } from "react";
import {getAllExperiences, editExperience, deleteExperience} from "../../crud/experience.crud";
import { toAbsoluteUrl } from "../../utils/utils";
import { formatPickerDate } from "../../utils/date";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import { getAllEmail } from "../../crud/email.crud";
import { getAllTappe } from "../../crud/tappa.crud";
import { Modal } from "react-bootstrap";
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';

import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';

import TappeSelector from "../tappa/TappeSelector";

import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import { dateFnsLocalizer } from "react-big-calendar";

var moment = require('moment');

class Experiences extends Component {

    constructor(props) {
        super(props);

        this.state = {
            allExps: null,
            email: [],
            tappe: [],
            orderTappe: {},
            selectedTappe: [],
            startTime: null,
            endTime: null,
            showSuccessAlert: false,
            showFailAlert: false
        };
    }

    componentDidMount() {
        getAllExperiences()
            .then((res) => {
                if (res && res.data) {
                    this.setState({ allExps: res.data });
                }
            });

        getAllEmail()
            .then((res) => {
                if (res && res.data) this.setState({ email: res.data });
            })
            .catch((error) => {
                console.error("[NewExperience] - getAllEmail: ", error);
            })

        getAllTappe()
            .then((res) => {
                if (res && res.data) this.setState({ tappe: res.data });
            })
            .catch((error) => {
                console.error("[NewExperience] - getAllTappe: ", error);
            })
    }

    componentWillUnmount() {

    }

    retrieveExpInfo(experience) {

        this.setState({ selectedTappe: [], orderTappe: {}, startTime: null, endTime: null });


        if (experience) {
            let preparedTappe = [];
            let preparedOrderTappe = {};
            if (experience.tappe) {
                for (let t of experience.tappe) {
                    preparedTappe.push(t.id)
                }
            }

            if (experience.orderTappe && experience.orderTappe.indexOf(',') !== -1) {
                let splitted = experience.orderTappe.split(',');
                for (let i = 0; i <= splitted.length - 1; i++) {
                    preparedOrderTappe[i + 1] = splitted[i];
                }
            }

            this.setState({
                selectedExperience: experience,
                selectedTappe: preparedTappe,
                orderTappe: preparedOrderTappe,
                showEditModal: true,
                startTime: formatPickerDate(experience.startTime),
                endTime: formatPickerDate(experience.endTime)
            });
        } else {
            console.error('error no data experience')
        }

    }

    deleteExperience(experience) {

        if (experience && experience.id) {
            deleteExperience(experience.id)
                .then(() => {
                    getAllExperiences()
                        .then((res) => {
                            if (res && res.data) {
                                this.setState({ allExps: res.data });
                            }
                        });
                });
        }
    }

    initializeNewExperience(data) {
        let toReturn = {};

        const { startTime, endTime, orderTappe, selectedTappe } = this.state;

        toReturn.name = data.experienceName ? data.experienceName.value : null;
        toReturn.partecipantNumber = data.partecipantNumber ? Number(data.partecipantNumber.value) : null;
        toReturn.startTime = startTime ? moment(startTime).format('HH:mm') : null;
        toReturn.endTime = endTime ? moment(endTime).format('HH:mm') : null;
        toReturn.tappe = [];
        let tappeToJoin = [];

        let orderTappeToSave = {};

        if (selectedTappe) {
            for (let selTappa of selectedTappe) {
                if (data["tappa_" + selTappa] && data["tappa_" + selTappa].value) {
                    orderTappeToSave[data["tappa_" + selTappa].value] = selTappa;
                    toReturn.tappe.push(selTappa);
                }
            }

            let i = 1;

            while (i <= selectedTappe.length) {
                tappeToJoin.push(orderTappeToSave[i])
                i++;
            }
        }
        toReturn.orderTappe = tappeToJoin.join(',');

        toReturn.emailInfo = data.emailInfo ? data.emailInfo.value : null;
        toReturn.emailPre = data.emailPre ? data.emailPre.value : null;
        toReturn.emailPost = data.emailPost ? data.emailPost.value : null;
        toReturn.emailRecensione = data.emailReview ? data.emailReview.value : null;
        toReturn.picture = data.picture ? data.picture.value : null;
        toReturn.note = data.note ? data.note.value : null;
        toReturn.price = data.price ? data.price.value : null;

        return toReturn;
    }


    render() {

        const { allExps } = this.state;

        if (!allExps) return "";

        return <div className="container-fluid">
            <main id="content" role="main">
                {/* <!-- Upload Form Section --> */}
                <div className="row">
                    <div className="col-lg-12">
                        {/* <!-- Title --> */}
                        <div className="row align-items-center mb-5">
                            <div className="col-lg-12">
                                <h3>Tutte le experience: {allExps.length}</h3>
                            </div>

                        </div>
                        {/* <!-- End title --> */}

                        {/* <!-- Tappe --> */}
                        <div className="row mx-n2 mb-5">
                            {allExps.map(experience => <div key={"experience_" + experience.id} className="col-sm-8 col-md-6 col-lg-4 px-2 px-sm-3 mb-3 mb-sm-5">
                                <div className="card border shadow-none text-center">

                                    <div className="mt-3">
                                        {/* <img className="card-img-top" src="../../assets/img/300x180/img3.jpg" alt="Image Description" /> */}
                                        <div className="d-flex justify-content-between align-items-center">
                                            <h5 className="mx-auto pl-5 mt-2">{experience.name}</h5>
                                            <button type="button" className="btn btn-pill btn-icon btn-light transition-3d-hover"
                                                onClick={() => this.retrieveExpInfo(experience)}>
                                                <span className="p-0 far fa-edit kt-font-brand btn-icon__inner"></span>
                                            </button>
                                            <button type="button" className="btn btn-pill btn-icon btn-light transition-3d-hover"
                                                    onClick={() => this.deleteExperience(experience)}>
                                                <span className="p-0 fa fa-trash kt-font-info btn-icon__inner"></span>
                                            </button>
                                        </div>
                                        <div className="text-center my-2">
                                            <img style={{ width: "150px", height: "150px" }} className="rounded"
                                                src={toAbsoluteUrl("/media/icons/png/" + experience.picture)}
                                            ></img>
                                        </div>
                                    </div>

                                    <div className="card-body">

                                        <div className="mb-2">
                                            <span className="d-inline-block font-weight-bold mb-2"><span className="fa fa-user-friends mr-2" /> {experience.partecipantNumber}</span>
                                            <p className="">Durata: {experience.startTime} - {experience.endTime}</p>
                                            <p className="">Tappe: <span className="text-dark ">{experience.orderTappe ? experience.orderTappe.split(',').length : 0}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>)}
                        </div>
                        {/* <!-- End Products --> */}

                        {/* <!-- Divider --> */}
                        < div className="d-lg-none" >
                            <hr className="my-7 my-sm-11" />
                        </div>
                        {/* <!-- End Divider --> */}
                    </div>

                </div>
            </main>
            {this.renderEditModal()}
            {this.renderSuccessModal()}
            {this.renderFailModal()}
        </div>
    }

    renderEditModal() {

        const { selectedExperience, email, tappe, orderTappe, selectedTappe, allExps } = this.state;

        if (!selectedExperience) return "";

        const handleSubmit = (event) => {
            event.preventDefault();

            if (event.target) {
                const params = this.initializeNewExperience(event.target);


                editExperience(selectedExperience.id, params)
                    .then(() => getAllExperiences())
                    .then((res) => {
                        if (res && res.data) {
                            this.setState({ showSuccessAlert: true, showEditModal: false, selectedExperience: null, allExps: res.data })
                        } else {
                            this.setState({ showFailAlert: true, showEditModal: false, selectedExperience: null })
                        }
                    })
                    .catch(() => {
                        this.setState({ showFailAlert: true, showEditModal: false, selectedExperience: null })
                    })
            }

        }

        const handleClose = () => {
            this.setState({ showEditModal: false, selectedExperience: null })

        }


        const updateTappe = (event) => {
            this.setState({ selectedTappe: event.target.value })
        }


        return <Modal show={this.state.showEditModal} size="lg" onHide={handleClose} >
            <Modal.Header closeButton>
                <Modal.Title className="h6">Modifica Experience</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <div className="container space-2">
                    <form id="uploadForm" className="js-validate svg-preloader" onSubmit={handleSubmit}>
                        {/* <!-- Listing Agent Information --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Informazioni base</h2>
                            </div>
                            {/* <!-- End Title --> */}

                            <div className="row">
                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="experienceName">Nome</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="experienceNameLabel">
                                                        <span className="fas fa-file-signature"></span>
                                                    </span>
                                                </div>
                                                <input type="text" className="form-control" name="experienceName" id="experienceName"
                                                    placeholder="Nome esperienza" aria-label="Nome esperienza" aria-describedby="experienceNameLabel" required
                                                    defaultValue={selectedExperience.name}
                                                    data-msg="Please enter a listing agent name."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success" />
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>

                                <div className="col-md-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-form-message js-focus-state">
                                            <label className="form-label" htmlFor="partecipantNumber">Numero partecipanti</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="partecipantNumberLabel">
                                                        <span className="fas fa-users"></span>
                                                    </span>
                                                </div>
                                                <input type="number" className="form-control" name="phoneNumber" id="partecipantNumber" placeholder="Per esempio 10"
                                                    aria-label="Per esempio 10" aria-describedby="partecipantNumberLabel" required
                                                    defaultValue={selectedExperience.partecipantNumber}
                                                    data-msg="Please enter a phone number."
                                                    data-error-class="u-has-error"
                                                    data-success-class="u-has-success" />
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>

                            <div className="row">
                                {this.renderTimePickers()}
                            </div>
                        </div>
                        {/* <!-- End Listing Agent Information --> */}


                        {/* <!-- Listing Information --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Struttura</h2>
                            </div>
                            {/* <!-- End Title --> */}

                            {tappe.length ? <TappeSelector tappe={tappe}
                                orderTappe={orderTappe}
                                selectedTappe={selectedTappe}
                                updateTappe={updateTappe} /> : ""}

                        </div>
                        {/* <!-- End Listing Information --> */}

                        {/* <!-- Email setting --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Email Automatiche</h2>
                            </div>
                            {/* <!-- End Title --> */}
                            <div className="row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="emailInfo">Email di Info</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="emailInfoLabel">
                                                        <span className="fas fa-envelope"></span>
                                                    </span>
                                                </div>
                                                <select className="custom-select" id="emailInfo" placeholder="Scegliere email info"
                                                    defaultValue={selectedExperience.emailInfo.id}
                                                    aria-describedby="emailInfoLabel">
                                                    <option hidden>Scegliere email info</option>
                                                    {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>

                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="emailPre">Email pre</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="emailPreLabel">
                                                        <span className="fas fa-envelope"></span>
                                                    </span>
                                                </div>
                                                <select className="custom-select" id="emailPre" placeholder="Scegliere email pre"
                                                    defaultValue={selectedExperience.emailPre.id} aria-describedby="emailPreLabel">
                                                    <option hidden>Scegliere email pre</option>
                                                    {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="emailPost">Email post experience</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="emailPostLabel">
                                                        <span className="fas fa-envelope"></span>
                                                    </span>
                                                </div>
                                                <select className="custom-select" id="emailPost" placeholder="Scegliere email post"
                                                    defaultValue={selectedExperience.emailPost.id} aria-describedby="emailPostLabel">
                                                    <option hidden>Scegliere email post</option>
                                                    {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="emailReview">Email per la review</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="emailReviewLabel">
                                                        <span className="fas fa-envelope"></span>
                                                    </span>
                                                </div>
                                                <select className="custom-select" id="emailReview" placeholder="Scegliere email review"
                                                    defaultValue={selectedExperience.emailRecensione.id} aria-describedby="emailReviewLabel">
                                                    <option hidden>Scegliere email review</option>
                                                    {email ? email.map(email => <option value={email.id}>{email.type}</option>) : <option>caricamento email</option>}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>

                            </div>

                        </div>
                        {/* <!-- End Emails --> */}

                        {/* <!-- Upload Images --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Icona experience</h2>
                            </div>
                            {/* <!-- End Title --> */}

                            <div className="row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="picture">
                                                <span className="fas fa-map-marker-alt mr-2"></span> Scegliere l'icona </label>
                                            <div className="input-group">

                                                <select className="custom-select" searchable="Search here.." id="picture"
                                                    defaultValue={selectedExperience.picture} aria-describedby="pictureLabel">
                                                    <option value="colosseum.png">Icona Colosseo</option>
                                                    <option value="campo.png">Icona Campo</option>
                                                    <option value="vatican.png">Icona Vaticano</option>
                                                    <option value="monti.png">Icona Monti</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>

                            </div>
                        </div>
                        {/* <!-- End Upload Images --> */}

                        {/* <!-- Upload Images --> */}
                        <div className="mb-7">
                            {/* <!-- Title --> */}
                            <div className="border-bottom pb-3 mb-5">
                                <h2 className="h6 kt-font-brand mb-0">Note</h2>
                            </div>
                            {/* <!-- End Title --> */}

                            <div className="row">
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="js-form-message mb-3">
                                        <label className="form-label">
                                            Note ed eventuali  <span className="text-danger">*</span>
                                        </label>

                                        <div className="input-group">
                                            <textarea className="form-control" rows="6" name="text" id="note"
                                                defaultValue={selectedExperience.note} placeholder="Aggiungi note e varie ed eventuali" aria-label="Note e varie"
                                                data-msg="Please enter a property description."
                                                data-error-class="u-has-error"
                                                data-success-class="u-has-success"></textarea>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                                <div className="col-lg-6 mb-3">
                                    {/* <!-- Input --> */}
                                    <div className="form-group mb-5">
                                        <div className="js-focus-state">
                                            <label className="form-label" htmlFor="listingPrice">Price</label>
                                            <div className="input-group">
                                                <div className="input-group-prepend">
                                                    <span className="input-group-text" id="listingPriceLabel">
                                                        <span className="fas fa-dollar-sign"></span>
                                                    </span>
                                                </div>
                                                <input type="text" className="form-control" name="price" id="listingPrice"
                                                    defaultValue={selectedExperience.price} placeholder="Price" aria-label="Price" aria-describedby="listingPriceLabel" />
                                            </div>
                                        </div>
                                    </div>
                                    {/* <!-- End Input --> */}
                                </div>
                            </div>
                        </div>
                        {/* <!-- End Upload Images --> */}

                        <div className="row">
                            <div className="col-lg-3 col-md-2 col-sm-1">
                                <button type="submit" className="btn btn-primary btn-block transition-3d-hover">Salva</button>
                            </div>
                        </div>
                    </form>
                </div>
            </Modal.Body>
        </Modal>
    }

    renderTimePickers() {

        const { startTime, endTime } = this.state;

        const handleSTDateChange = (date) => {
            this.setState({ startTime: date });
        };

        const handleETDateChange = (date) => {
            this.setState({ endTime: date });
        };

        return <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container className="ml-3 mb-5">
                <Grid lg="6">
                    <Grid lg="10" item>
                        <label className="form-label" htmlFor="startTimePicker"><span className="fas fa-clock"></span> Orario d'Inizio</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="startTimePicker"
                                ampm={false}
                                value={startTime}
                                onChange={handleSTDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>

                    </Grid>
                </Grid>
                <Grid lg="6">
                    <Grid lg="10" className="ml-1" item>
                        <label className="form-label" htmlFor="endTimePicker"><span className="fas fa-clock"></span> Orario di fine</label>
                        <div className="input-group">
                            <KeyboardTimePicker
                                margin="normal"
                                id="endTimePicker"
                                ampm={false}
                                value={endTime}
                                onChange={handleETDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change time',
                                }}
                            />
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </MuiPickersUtilsProvider>
    }



    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState({ showSuccessAlert: false });
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }


}

export default injectIntl(
    connect(
    )(Experiences)
);