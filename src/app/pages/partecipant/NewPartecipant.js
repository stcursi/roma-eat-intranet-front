import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { Formik } from "formik";
import {
    TextField,
    FormControl,
    Select,
    Checkbox,
    MenuItem,
    InputLabel,
    FormControlLabel
} from "@material-ui/core";
import 'react-widgets/dist/css/react-widgets.css';
import { getTour } from "../../crud/tour.crud";
import { getAllFonti } from "../../crud/fonte.crud";
import BootstrapTable from 'react-bootstrap-table-next';
import TableHeaderColumn from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import cellEditFactory from 'react-bootstrap-table2-editor';
import { Type } from 'react-bootstrap-table2-editor';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { savePartecipation } from "../../crud/partecipation.crud";
import SuccessModal from "../../partials/content/SuccessModal";
import FailModal from "../../partials/content/FailModal";
import { getAllPaymentMethods } from "../../crud/paymentMethod.crud";

class NewPartecipant extends Component {

    constructor(props) {
        super(props);

        this.myFormRef = React.createRef();

        this.state = {
            tour: null,
            fonti: [],
            selectedPartecipants: [],
            tourId: null,
            showSuccessAlert: false,
            showFailAlert: false,
            rowSelected: []
        };
    }

    componentDidMount() {

        const tourId = this.props.match && this.props.match.params && this.props.match.params.tourId || null;

        if (tourId) {
            getTour(tourId)
                .then(res => this.setState({
                    tour: res.data, tourId: tourId, initialValues: {
                        surname: "",
                        name: "",
                        telephone: "",
                        email: "",
                        provenienza: "",
                        vegan: false,
                        vegetarian: false,
                        group: false,
                        groupNumber: null,
                        allergies: "",
                        bookedFrom: "",
                        paymentMethod: "",
                        note: "",
                        noteAdmin: "",
                        price: res.data.experience && res.data.experience.price || ""
                    }
                }))

            getAllFonti()
                .then((res) => {
                    if (res && res.data) this.setState({ fonti: res.data });
                })
                .catch((error) => {
                    console.error("[NewPartecipant] - getAllFonti: ", error);
                })

            getAllPaymentMethods()
                .then((res) => {
                    if (res && res.data) this.setState({ paymentMethods: res.data });
                })
                .catch((error) => {
                    console.error("[NewPartecipant] - getAllPayments: ", error);
                });

        }


    }

    componentWillUnmount() {

    }

    render() {

        const { tour } = this.state;

        if (!tour) return "";

        return (
            <div className="container-fluid">
                <main id="content" role="main">
                    {/* <!-- Upload Form Section --> */}
                    {this.renderInserForm()}
                    {this.renderSelectedPartecipantTable()}
                    {this.renderSuccessModal()}
                    {this.renderFailModal()}
                    {/* <!-- End Upload Form Section --> */}
                </main>
            </div>
        )
    }

    renderSuccessModal() {
        const { showSuccessAlert } = this.state;

        const onCloseSuccessModal = () => {
            this.setState({
                tour: null,
                fonti: [],
                selectedPartecipants: [],
                tourId: null,
                showSuccessAlert: false,
                showFailAlert: false
            });
        };

        return <SuccessModal showModal={showSuccessAlert} onCloseModal={onCloseSuccessModal} />
    }

    renderFailModal() {
        const { showFailAlert } = this.state;

        const onCloseFailModal = () => {
            this.setState({ showFailAlert: false })
        };

        return <FailModal showModal={showFailAlert} onCloseModal={onCloseFailModal} />
    }

    renderInserForm() {

        const { tour, fonti, paymentMethods } = this.state;
        const { intl } = this.props;


        return (
            <div className="container space-2">
                <div className="w-lg-75 mx-lg-auto">
                    {/* <!-- Title --> */}
                    <div className="text-center mb-9">
                        <h1 className="h2 kt-font-brand font-weight-medium">Aggiungi partecipanti al tour</h1>
                        <p>Inserisci le informazioni della partecipazione </p>
                    </div>
                    {/* <!-- End Title --> */}

                    <Formik
                        initialValues={this.state.initialValues}
                        validate={values => {
                            const errors = {};

                            if (!values.surname) {
                                errors.surname = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (values.email != "" &&
                                !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                            ) {
                                errors.email = intl.formatMessage({
                                    id: "AUTH.VALIDATION.INVALID_FIELD"
                                });
                            }

                            if (!values.name) {
                                errors.name = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            if (!values.bookedFrom) {
                                errors.bookedFrom = intl.formatMessage({
                                    id: "AUTH.VALIDATION.REQUIRED_FIELD"
                                });
                            }

                            return errors;
                        }}
                        onSubmit={(values, { setStatus, setSubmitting, resetForm }) => {

                            try {
                                const { selectedPartecipants } = this.state;

                                let partecipants = [];

                                let group = values.groupNumber && values.groupNumber !== 0 ? true : false;
                                let groupNumber = values.groupNumber;

                                let partecipant = {
                                    surname: values.surname,
                                    name: values.name,
                                    telephone: values.telephone,
                                    email: values.email,
                                    vegan: values.vegan,
                                    group: values.groupNumber && values.groupNumber !== 0 ? true : false,
                                    groupNumber: values.groupNumber,
                                    provenienza: values.provenienza,
                                    vegetarian: values.vegetarian,
                                    bookedFrom: values.bookedFrom,
                                    paymentMethod: values.paymentMethod,
                                    price: values.price,
                                    allergies: values.allergies,
                                    note: values.note,
                                    noteAdmin: values.noteAdmin,
                                    id: 0
                                }

                                partecipants.push(partecipant);

                                if (group) {
                                    let i = 0;
                                    while (i < groupNumber) {
                                        let newPartecipant = {
                                            name: partecipant.name + "_" + i,
                                            surname: partecipant.surname + "_" + i,
                                            email: "",
                                            telephone: partecipant.telephone,
                                            vegan: false,
                                            provenienza: partecipant.provenienza,
                                            vegetarian: false,
                                            bookedFrom: partecipant.bookedFrom,
                                            paymentMethod: partecipant.paymentMethod,
                                            price: partecipant.price,
                                            allergies: "",
                                            note: "",
                                            noteAdmin: ""
                                        }
                                        i++;
                                        newPartecipant.id = i;
                                        partecipants.push(newPartecipant);
                                    }
                                }

                                partecipants = partecipants.concat(selectedPartecipants);

                                this.setState({
                                    selectedPartecipants: partecipants,
                                });

                                resetForm(this.state.initialValues);

                            } catch (e) {
                                setSubmitting(false);
                                setStatus(
                                    intl.formatMessage({
                                        id: "AUTH.VALIDATION.INVALID_LOGIN"
                                    })
                                );
                            }
                        }}
                    >
                        {({
                            values,
                            status,
                            errors,
                            touched,
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            isSubmitting
                        }) => (
                            <form onSubmit={handleSubmit} noValidate autoComplete="off" ref={(el) => this.myFormRef = el}>
                                {status && (
                                    <div role="alert" className="alert alert-danger">
                                        <div className="alert-text">{status}</div>
                                    </div>
                                )}

                                <div className="row">
                                    <h6 className="kt-font-brand">Informazioni personali</h6>
                                </div>

                                <div className="row">
                                    <div className="col-lg-4">
                                        <div className="form-group mb-0">
                                            <TextField
                                                label="Nome"
                                                margin="normal"
                                                className="kt-width-full"
                                                name="name"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.name}
                                                helperText={touched.name && errors.name}
                                                error={Boolean(touched.name && errors.name)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-0">
                                            <TextField
                                                margin="normal"
                                                label="Cognome"
                                                className="kt-width-full"
                                                name="surname"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.surname}
                                                helperText={touched.surname && errors.surname}
                                                error={Boolean(touched.surname && errors.surname)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-0">
                                            <TextField
                                                margin="normal"
                                                label="Email"
                                                className="kt-width-full"
                                                name="email"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.email}
                                                helperText={errors.email}
                                                error={Boolean(errors.email)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-4">
                                            <TextField
                                                margin="normal"
                                                label="Recapito Tel."
                                                className="kt-width-full"
                                                name="telephone"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.telephone}
                                                helperText={touched.telephone && errors.telephone}
                                                error={Boolean(touched.telephone && errors.telephone)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-2">
                                            <TextField
                                                margin="normal"
                                                label="Provenienza"
                                                className="kt-width-full"
                                                name="provenienza"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.provenienza}
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <h6 className="kt-font-brand">Allergie e scelte alimentari</h6>
                                </div>
                                <div className="row">
                                    <div className="col-lg-4">
                                        <div className="form-group mb-5">
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={values.allergic}
                                                        onChange={handleChange}
                                                        name="allergic"
                                                        color="primary"
                                                    />
                                                }
                                                label="Restrizioni Alimentari"
                                            />
                                            {values.allergic ?
                                                <TextField
                                                    margin="normal"
                                                    label="Tipi di allergie"
                                                    multiline
                                                    className="kt-width-full"
                                                    name="allergies"
                                                    onBlur={handleBlur}
                                                    onChange={handleChange}
                                                    value={values.allergies}
                                                    helperText={touched.allergies && errors.allergies}
                                                    error={Boolean(touched.allergies && errors.allergies)}
                                                />
                                                : ""}
                                        </div>
                                    </div>
                                    {/* <div className="col-lg-4">
                                        <div className="form-group mb-2">
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={values.vegan}
                                                        onChange={handleChange}
                                                        name="vegan"
                                                        color="primary"
                                                    />
                                                }
                                                label="Vegano"
                                            />
                                        </div>
                                    </div> */}
                                    <div className="col-lg-4">
                                        <div className="form-group mb-2">
                                            <FormControlLabel
                                                control={
                                                    <Checkbox
                                                        checked={values.vegetarian}
                                                        onChange={handleChange}
                                                        name="vegetarian"
                                                        color="primary"
                                                    />
                                                }
                                                label="Vegetariano"
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <h6 className="kt-font-brand">Prezzo e prenotazioni</h6>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-lg-4 ">
                                        <div className="form-group mb-2" >
                                            <TextField
                                                margin="normal"
                                                label="Prezzo"
                                                className="kt-width-full"
                                                name="price"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.price}
                                                helperText={touched.price && errors.price}
                                                error={Boolean(touched.price && errors.price)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormControl style={{
                                            minWidth: 320,
                                        }}>
                                            <InputLabel id="BookedFromLabel">Prenotato da</InputLabel>
                                            <Select
                                                labelId="BookedFromLabel"
                                                className="d-block"
                                                id="bookedFromSelect"
                                                name="bookedFrom"
                                                value={values.bookedFrom}
                                                error={Boolean(touched.bookedFrom && errors.bookedFrom)}
                                                onChange={handleChange}
                                            >
                                                {fonti ? fonti.map((fonte) => <MenuItem key={"fonte_" + fonte.id} value={fonte.id}>{fonte.type}</MenuItem>) : <MenuItem value="null">Caricamento piattaforme</MenuItem>}
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div className="col-lg-4">
                                        <FormControl style={{
                                            minWidth: 320,
                                        }}>
                                            <InputLabel id="PaymentMethodLabel">Metodo di pagamento</InputLabel>
                                            <Select
                                                labelId="PaymentMethodLabel"
                                                className="d-block"
                                                id="paymentMethodSelect"
                                                name="paymentMethod"
                                                value={values.paymentMethod}
                                                onChange={handleChange}
                                            >
                                                {paymentMethods ? paymentMethods.map((paymentMethod) => <MenuItem key={"paymentMethod_" + paymentMethod.id} value={paymentMethod.id}>{paymentMethod.type}</MenuItem>) : <MenuItem value="null">Caricamento piattaforme</MenuItem>}
                                            </Select>
                                        </FormControl>
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-lg-4">
                                        <FormControl style={{
                                            minWidth: 320,
                                            marginTop: "5px"
                                        }}>
                                            {!values.groupNumber ? <InputLabel id="GroupNumberLabel">Partecipanti aggiuntivi?</InputLabel> : ""}
                                            <TextField
                                                margin="normal"
                                                labelId="GroupNumberLabel"
                                                name="groupNumber"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.groupNumber}
                                                type="number"
                                                helperText={touched.groupNumber && errors.groupNumber}
                                                error={Boolean(touched.groupNumber && errors.groupNumber)}
                                            />
                                        </FormControl>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-2">
                                            <TextField
                                                margin="normal"
                                                label="Note"
                                                multiline
                                                className="kt-width-full"
                                                name="note"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.note}
                                                helperText={touched.note && errors.note}
                                                error={Boolean(touched.note && errors.note)}
                                            />
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="form-group mb-2">
                                            <TextField
                                                margin="normal"
                                                label="Note per admin"
                                                multiline
                                                className="kt-width-full"
                                                name="noteAdmin"
                                                onBlur={handleBlur}
                                                onChange={handleChange}
                                                value={values.noteAdmin}
                                                helperText={touched.noteAdmin && errors.noteAdmin}
                                                error={Boolean(touched.noteAdmin && errors.noteAdmin)}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div className="kt-login__actions mt-5">

                                    <button
                                        disabled={isSubmitting}
                                        className="btn btn-info btn-elevate kt-login__btn-brand"
                                    >
                                        Aggiungi
                                         </button>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </div>)
    }

    renderSelectedPartecipantTable() {
        const { selectedPartecipants } = this.state;
        let tableRef = React.createRef();

        if (!selectedPartecipants || !selectedPartecipants.length) return "";

        let pageOptions = { pageCurrent: null, paginationSize: null };

        pageOptions.paginationSize = 25;

        const partecipantsParsed = this.renderRows(selectedPartecipants);

        const sizePerPageList = [{
            text: '5', value: 5
        }, {
            text: '10', value: 10
        }, {
            text: '25', value: 25
        }, {
            text: '50', value: 50
        }, {
            text: '100', value: 100
        }, {
            text: '250', value: 250
        }, {
            text: '500', value: 500
        }];

        const sizePerPageOptionRenderer = ({
            text,
            page,
            onSizePerPageChange
        }) => (
            <li
                key={text}
                role="presentation"
                className="dropdown-item"
            >
                <a
                    href="#"
                    tabIndex="-1"
                    key={text}
                    role="menuitem"
                    data-page={page}
                    onMouseDown={(e) => {
                        onSizePerPageChange(page);
                        e.preventDefault();
                    }}
                    onClick={(e) => {
                        e.preventDefault();
                    }}
                >
                    {text}
                </a>
            </li>
        );



        const onPaginationChange = (sizePerPage, page) => {
            console.log('size per page: ', sizePerPage);
            console.log('page: ', page);
        }


        const optionsPagination = {
            sizePerPageOptionRenderer: sizePerPageOptionRenderer,
            sizePerPage: pageOptions.paginationSize,
            sizePerPageList: sizePerPageList,
            onSizePerPageChange: onPaginationChange,
            page: pageOptions.pageCurrent || 1
        };

        const cellEdit = cellEditFactory({
            mode: 'click',
            afterSaveCell: (oldValue, newValue, row, column) => {

                let partecipantsCurrent = [...this.state.selectedPartecipants];
                let newRow = { ...row };

                let index = 0;
                partecipantsCurrent.map((part, i) => {
                    if (part.id === row.id) {
                        if (newValue === "true") newValue = true;
                        if (newValue === "false") newValue = false;
                        newRow[column.dataField] = newValue;
                        index = i;
                    }
                });

                partecipantsCurrent.splice(index, 1);
                partecipantsCurrent.push(newRow);

                this.setState({ selectedPartecipants: partecipantsCurrent });


                if (tableRef.current && tableRef.current.props && tableRef.current.props.data) {
                    tableRef.current.props.data.splice(0, tableRef.current.props.data.length)
                }

            },
        });

        const deletePartecipant = (idx) => {
            let partecipantsCurrent = [...this.state.selectedPartecipants];

            let index = 0;
            partecipantsCurrent.map((part, i) => {
                if (part.id === idx) {
                    index = i;
                }
            });

            partecipantsCurrent.splice(index, 1);

            this.setState({ selectedPartecipants: partecipantsCurrent });


            if (tableRef.current && tableRef.current.props && tableRef.current.props.data) {
                tableRef.current.props.data.splice(0, tableRef.current.props.data.length)
            }

        }

        const selectRow = {
            mode: 'checkbox',
            clickToSelect: false,
            onSelect: (row, isSelect, rowIndex, e) => {
                const { rowSelected } = this.state;
                if (isSelect) {
                    rowSelected.push(row.id);
                } else {
                    let idx = rowSelected.indexOf(row.id);
                    if (idx !== -1) {
                        rowSelected.splice(idx, 1);
                    }
                }

                this.setState({ rowSelected: rowSelected });
            },
            onSelectAll: (isSelect, rows, e) => {
                let { rowSelected } = this.state;
                if (isSelect) {
                    rows.map((row) => rowSelected.push(row.id));
                } else {
                    rowSelected = [];
                }

                this.setState({ rowSelected: rowSelected });
            }
        };

        const rowStyle = (row, rowIndex) => {

            if (row.imprevisto && row.imprevisto != "") {
                return { backgroundColor: '#ffb822' }
            }
            if (row.annullato) {
                return { backgroundColor: '#F32013' }
            }

            // return { ... };
        };

        const basicStyle = { whiteSpace: "nowrap", overflowX: "hidden", textOverflow: "ellipsis" };

        const columns = [{
            dataField: 'name',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "5rem" };
            },
            text: 'Nome',
            sort: true,
        }, {
            dataField: 'surname',
            editable: true,
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "5rem" };
            },
            text: 'Cognome',
            sort: true,
        }, {
            dataField: 'email',
            editable: true,
            text: 'Email',
            style: basicStyle,
            headerStyle: () => {
                return { minWidth: "10rem" };
            },
            sort: true,
        }, {
            dataField: 'allergies',
            editable: true,
            text: 'Allergie',
            style: basicStyle,
            headerStyle: () => {
                return { width: "7rem" };
            }
        }, {
            dataField: 'provenienza',
            editable: true,
            text: 'Provenienza',
            style: basicStyle,
            headerStyle: () => {
                return { width: "7rem" };
            }
        }, {
            dataField: 'price',
            editable: true,
            text: 'Prezzo',
            style: basicStyle,
            headerStyle: () => {
                return { width: "5rem" };
            }
        }, {
            dataField: 'vegetarian',
            text: 'Vegetariano',
            style: basicStyle,
            headerStyle: () => {
                return { width: "7.5rem" };
            },
            editor: {
                type: Type.SELECT,
                options: [{
                    value: true,
                    label: 'Si'
                }, {
                    value: false,
                    label: "No"
                }]
            }
        }, {
            dataField: 'vegan',
            text: 'Vegano',
            style: basicStyle,
            headerStyle: () => {
                return { width: "7.5rem" };
            },
            editor: {
                type: Type.SELECT,
                options: [{
                    value: true,
                    label: 'Si'
                }, {
                    value: false,
                    label: "No"
                }]
            }
        }, {
            dataField: 'note',
            editable: true,
            style: basicStyle,
            text: 'Note',
            headerStyle: () => {
                return { width: "9rem" };
            }
        }, {
            dataField: 'noteAdmin',
            editable: true,
            style: basicStyle,
            text: 'Note per admin',
            headerStyle: () => {
                return { minWidth: "8rem" };
            }
        }, {
            dataField: "deleteSlot",
            text: "",
            editable: false,
            sort: false,
            formatter: (cell, row, rowIndex, formatExtraData) => {
                return <span id={row.id} type="button" className="p-0 fa fa-trash kt-font-info" onClick={() => deletePartecipant(rowIndex)} />
            },
            headerAttrs: { width: 50 },
            attrs: { width: 50, class: "" } 
        }

        ];

        return <ToolkitProvider
            keyField="partecipantsTable"
            // trClassName={"text-secondary no-min-height"}
            options={{
                sortName: "name",  //default sort column name
                sortOrder: "desc",  //default sort order
            }}
            data={partecipantsParsed}
            columns={columns}
            search={{
                searchFormatted: true
            }}
        >
            {
                props => (
                    <div>
                        <BootstrapTable {...props.baseProps} ref={tableRef} striped bordered keyField='id'
                            rowStyle={rowStyle}
                            selectRow={selectRow}
                            cellEdit={cellEdit}
                            pagination={paginationFactory(optionsPagination)}
                        />
                        <div className="row justify-content-start">
                            <button className="btn btn-primary" onClick={() => this.handleSavePartecipation()}>Salva partecipanti</button>
                            <button className="btn btn-warning ml-2" disabled={this.state.rowSelected.length === 0} onClick={() => this.handleCancelPartecipation()}>Elimina partecipanti</button>
                        </div>
                    </div>
                )
            }
        </ToolkitProvider>

    }

    handleCancelPartecipation() {
        const { rowSelected, selectedPartecipants } = this.state;

        let idToDelete = [];

        for (var row of rowSelected) {
            for (var i = 0; i <= selectedPartecipants.length - 1; i++) {
                if (selectedPartecipants[i].id === row) {
                    idToDelete.push(i);
                    break;
                }
            }
        }

        for (var idx of idToDelete) {
            selectedPartecipants.splice(idx, 1);
        }

        this.setState({ selectedPartecipants: selectedPartecipants, rowSelected: [] });

    }

    handleSavePartecipation() {

        const { selectedPartecipants, tourId, tour } = this.state;

        savePartecipation(tourId, selectedPartecipants)
            .then(() => {
                this.setState({ showSuccessAlert: true, selectedPartecipants: [] })

                this.props.history.push("/tours/calendar", tour.date);

            })
            .catch(e => {
                console.error('Error save partecipants', e);
                this.setState({ showFailAlert: true, messageError: e.message })
            });
    }

    renderRows(partecipants) {

        let toReturn = [];

        for (let partecipant of partecipants) {
            let newPartecipant = Object.assign({}, partecipant)
            newPartecipant.name = partecipant.name;
            newPartecipant.surname = partecipant.surname;
            newPartecipant.email = partecipant.email;
            newPartecipant.provenienza = partecipant.provenienza;
            newPartecipant.price = partecipant.price;
            newPartecipant.allergies = partecipant.allergies;
            newPartecipant.note = partecipant.note ? partecipant.note : "";
            newPartecipant.noteAdmin = partecipant.noteAdmin ? partecipant.noteAdmin : "";
            newPartecipant.vegan = partecipant.vegan && partecipant.vegan === true ? "Si" : "";
            newPartecipant.vegetarian = partecipant.vegetarian && partecipant.vegetarian === true ? "Si" : "";
            newPartecipant.id = partecipant.id //fittizio
            newPartecipant.deleteSlot = 'slot'

            toReturn.push(newPartecipant);
        }

        let sortedPartecipants = toReturn.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))

        return sortedPartecipants;
    }

}





export default injectIntl(
    connect(
    )(NewPartecipant)
);