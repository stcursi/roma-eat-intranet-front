import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { Modal } from "react-bootstrap";

class FailModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {

        const handleClose = () => {
            this.props.onCloseModal();
        };

        return <Modal show={this.props.showModal} size="sm" onHide={handleClose} >
            <Modal.Header closeButton>
                <Modal.Title className="h6">Ops!</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <div className="container space-2 text-center align-items-center">
                    <span className="fas fa-times-circle fa-5x text-danger my-2"></span>
                    <p>Qualcosa è andato storto</p>
                </div>
            </Modal.Body>
        </Modal>

    }


}

export default injectIntl(
    connect(
    )(FailModal)
);