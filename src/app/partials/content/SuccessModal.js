import React, { Component } from "react";
import { connect } from "react-redux";
import { injectIntl } from "react-intl";
import { Modal } from "react-bootstrap";

class SuccessModal extends Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    render() {

        const handleClose = () => {
            this.props.onCloseModal();
        };


        return <Modal show={this.props.showModal} size="sm" onHide={handleClose} >
            <Modal.Header closeButton>
                <Modal.Title className="h6">Ok!</Modal.Title>
            </Modal.Header>
            <Modal.Body className="bg-light">
                <div className="container space-2 text-center align-items-center">
                    <i class="fas fa-check fa-5x text-success my-2"></i>
                    <p>Operazione avvenuta con successo</p>
                </div>
            </Modal.Body>
        </Modal>

    }


}

export default injectIntl(
    connect(
    )(SuccessModal)
);