import axios from "axios";

export const PARTECIPATION_URL = "/partecipation"

export function getAllPartecipations() {
    // Authorization head should be fulfilled in interceptor.
    return axios.get(PARTECIPATION_URL + "/" + "all");
}

export function getPartecipation(partecipationId) {
    return axios.get(PARTECIPATION_URL + "/" + partecipationId);
}

export function getTourPartecipations(tourId) {
    return axios.get(PARTECIPATION_URL + "/" + tourId);
}

export function editPartecipation(partecipationId, params) {
    return axios.patch(PARTECIPATION_URL + "/" + partecipationId, params);
}

export function editBulkPartecipations(partecipations, params) {
    return axios.patch(PARTECIPATION_URL + "/bulk", {
        partecipations: partecipations,
        params: params
    });
}

export function savePartecipation(tourId, partecipationData) {
    return axios.post(PARTECIPATION_URL, {
        partecipations: partecipationData,
        tourId: tourId
    });
}

export function deletePartecipation(partecipationId) {
    return axios.delete(PARTECIPATION_URL + "/" + partecipationId);
}

export function deleteBulkPartecipations(partecipations) {
    return axios.post(PARTECIPATION_URL + "/bulkDelete", {
        partecipations: partecipations
    });
}
