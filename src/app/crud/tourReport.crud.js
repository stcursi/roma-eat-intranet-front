import axios from "axios";

export const TOUR_REPORT_URL = "/tourReport"

export function getAllReport() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(TOUR_REPORT_URL + "/" + "all");
}

export function getReport(reportId) {
  return axios.get(TOUR_REPORT_URL + "/" + reportId);
}

export function getReportByTour(tourId) {
  return axios.get(TOUR_REPORT_URL + "/tour/" + tourId);
}

export function saveReport(reportData) {
    return axios.post(TOUR_REPORT_URL, reportData);
  }

export function editReport(reportId, params) {
  return axios.patch(TOUR_REPORT_URL + "/" + reportId, params);
}

export function deleteReport(reportId) {
  return axios.delete(TOUR_REPORT_URL + "/" + reportId);
}
