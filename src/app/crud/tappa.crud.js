import axios from "axios";

export const TAPPA_URL = "/tappa"

export function getAllTappe() {
  // Authorization head should be fulfilled in interceptor.
  return axios.get(TAPPA_URL + "/" + "all");
}

export function getTappa(tappaId) {
  return axios.get(TAPPA_URL + "/" + tappaId);
}

export function getTappeByTour(tourId) {
  return axios.get(TAPPA_URL + "/tour/" + tourId);
}

export function editTappa(tappaId, params) {
  return axios.patch(TAPPA_URL + "/" + tappaId, params);
}


export function saveTappa(tappaData) {
  return axios.post(TAPPA_URL, tappaData);
}

export function deleteTappa(tappaId) {
  return axios.delete(TAPPA_URL + "/" + tappaId);
}
