import axios from "axios";

export const PAYMENT_METHOD_URL = "/paymentMethod"

export function getAllPaymentMethods() {
    return axios.get(PAYMENT_METHOD_URL + "/" + "all");
  }

export function getPaymentMethod(paymentMethodId) {
  return axios.get(PAYMENT_METHOD_URL + "/" + paymentMethodId);
}

export function editPaymentMethod(paymentMethodId, params) {
    return axios.patch(PAYMENT_METHOD_URL + "/" + paymentMethodId, params);
}

export function savePaymentMethod(paymentMethodData) {
    return axios.post(PAYMENT_METHOD_URL, paymentMethodData);
}

export function deletePaymentMethod(paymentMethodId) {
    return axios.delete(PAYMENT_METHOD_URL + "/" + paymentMethodId);
}
