import axios from "axios";

export const ASSENZA_URL = "/assenza"

export function getAllAssenze(params) {
  // Authorization head should be fulfilled in interceptor.
  let options = {
    params: params
  }
  return axios.get(ASSENZA_URL + "/" + "all", options);
}

export function getAssenza(assenzaId) {
  return axios.get(ASSENZA_URL + "/" + assenzaId);
}

export function getAssenzeByGuide(userId, params) {
  let options = {
    params: params
  }
  return axios.get(ASSENZA_URL + "/user/" + userId, options);
}

export function saveAssenza(assenzaData) {
    return axios.post(ASSENZA_URL, assenzaData);
  }

export function editAssenza(assenzaId, params) {
  return axios.patch(ASSENZA_URL + "/" + assenzaId, params);
}

export function deleteAssenza(assenzaId) {
  return axios.delete(ASSENZA_URL + "/" + assenzaId);
}

export function deleteBulkAssenze(assenze) {
  return axios.post(ASSENZA_URL + "/bulk", {
    assenze: assenze
  });
}
