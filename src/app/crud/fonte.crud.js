import axios from "axios";

export const FONTE_URL = "/fonte"

export function getAllFonti() {
    return axios.get(FONTE_URL + "/" + "all");
  }

export function getFonte(fonteId) {
  return axios.get(FONTE_URL + "/" + fonteId);
}

export function editFonte(fonteId, params) {
    return axios.patch(FONTE_URL + "/" + fonteId, params);
}

export function saveFonte(fonteData) {
    return axios.post(FONTE_URL, fonteData);
}

export function deleteFonte(fonteId) {
    return axios.delete(FONTE_URL + "/" + fonteId);
}
