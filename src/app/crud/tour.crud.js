import axios from "axios";

export const TOUR_URL = "/tour"

export function getAllTours(params) {
  // Authorization head should be fulfilled in interceptor.
  let options = {
    params: params
  }
  return axios.get(TOUR_URL + "/" + "all", options);
}

export function getToursForActions(params) {
  // Authorization head should be fulfilled in interceptor.
  let options = {
    params: params
  }
  return axios.get(TOUR_URL + "/" + "allForTours", options);
}

export function getTour(tourId) {
  return axios.get(TOUR_URL + "/" + tourId);
}

export function editTour(tourId, params) {
  return axios.patch(TOUR_URL + "/" + tourId, params);
}

export function editBulkTours(tours, params) {
  return axios.patch(TOUR_URL + "/bulk", {
    tours: tours,
    params: params
  });
}

export function editBulkTimeTours(tours, params) {
  return axios.patch(TOUR_URL + "/time/bulk", {
    tours: tours,
    params: params
  });
}

export function assignTours(tours, guideId) {
  return axios.post(TOUR_URL + "/assign", {
    guideId: guideId,
    tours: tours
  })
}

export function saveTour(tourData) {
  return axios.post(TOUR_URL, tourData);
}

export function deleteTour(tourId) {
  return axios.delete(TOUR_URL + "/" + tourId);
}

export function deleteBulkTours(tours) {
  return axios.post(TOUR_URL + "/bulk", {
    tours: tours
  });
}

export function getExperienceCounter() {
  return axios.get(TOUR_URL + "/counterExperience")
}
