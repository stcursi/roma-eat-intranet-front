import axios from "axios";

const EXPERIENCE_URL = "/experience"


export function getAllExperiences() {
    return axios.get(EXPERIENCE_URL + "/all");
  }

export function getExperience(experienceId) {
  return axios.get(EXPERIENCE_URL + "/" + experienceId);
}

export function editExperience(experienceId, params) {
    return axios.patch(EXPERIENCE_URL + "/" + experienceId, params);
}

export function saveExperience(experienceData) {
    return axios.post(EXPERIENCE_URL, experienceData);
}

export function deleteExperience(experienceId) {
    return axios.delete(EXPERIENCE_URL + "/" + experienceId);
}
