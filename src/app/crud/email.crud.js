import axios from "axios";

export const EMAIL_URL = "/email"

export function getAllEmail() {
    return axios.get(EMAIL_URL + "/" + "all");
  }

export function getEmail(emailId) {
  return axios.get(EMAIL_URL + "/" + emailId);
}

export function editEmail(emailId, params) {
    return axios.patch(EMAIL_URL + "/" + emailId, params);
}

export function saveEmail(emailData) {
    return axios.post(EMAIL_URL, emailData);
}

export function deleteEmail(emailId) {
    return axios.delete(EMAIL_URL + "/" + emailId);
}
