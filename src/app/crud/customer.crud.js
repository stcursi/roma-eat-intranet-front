import axios from "axios";

export const CUSTOMER_URL = "/customer";

export function newCustomer(email, fullname, Customername, password, role) {
    return axios.post(CUSTOMER_URL, { email, fullname, Customername, password, role });
}

export function deleteCustomer(email) {
    return axios.delete(CUSTOMER_URL, { email });
}

export function editCustomer(customerId, params) {
    return axios.patch(CUSTOMER_URL + "/" + customerId, params);
  }