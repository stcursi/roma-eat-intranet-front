import axios from "axios";

export const ME_URL = "/user/me";
export const ROUTE_URL = "/user";

export function getUserByToken() {
    // Authorization head should be fulfilled in interceptor.
    return axios.get(ME_URL);
}

export function newUser(email, fullname, username, password, role) {
    return axios.post(ROUTE_URL, { email, fullname, username, password, role });
}

export function deleteUser(id) {
    return axios.delete(ROUTE_URL + "/" + id);
}

export function updateUser(id, email, fullname, username, password, role) {
    return axios.patch(ROUTE_URL + "/" + id, { email, fullname, username, password, role });
}

export function getAllByRole(role) {
    return axios.get(ROUTE_URL + "/users/" + role);
}

export function getGuideByAssenze(params) {
    // Authorization head should be fulfilled in interceptor.
    let options = {
        params: params
    }
    return axios.get(ROUTE_URL + "/guides/available", options);
}