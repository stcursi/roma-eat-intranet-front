import axios from "axios";

export const LOGIN_URL = "/auth/login";
export const REGISTER_URL = "/auth/signup";
export const REQUEST_PASSWORD_URL = "/auth/forgot-password";
export const CHANGE_PASSWORD_URL = "/auth/change-password";

export const ME_URL = "/user/me";

export function login(email, password) {
  return axios.post(LOGIN_URL, { email, password });
}

export function register(email, fullname, username, password, role) {
  return axios.post(REGISTER_URL, { email, fullname, username, password, role });
}

export function requestPassword(email) {
  return axios.post(REQUEST_PASSWORD_URL, { email });
}

export function changePassword(oldPassword, newPassword) {
  return axios.post(CHANGE_PASSWORD_URL, { oldPassword, newPassword });
}